from sqlalchemy.dialects.postgresql import UUID as PGUUID, JSONB
from crewapi.ext import db, is_not_same_uuid
from uuid import UUID
from sqlalchemy import func
from flask import request, current_app
from crewapi.exceptions import SessionRequiredError, RequiredPermissionMissingError, UnauthorizeError, OrgContextMissingError
from jwt import decode as jwt_decode


class CruSession(object):
    def __init__(self, id, user_id, user_name, is_first_login, app_role=None,
                 organisation=None, organisation_role=None,
                 opunit=None, opunit_role=None,
                 accessible_org_count=0, accessible_opunit_count=0,
                 permissions={}):
        self.id = id
        self.user_id = user_id
        self.user_name = user_name
        self.is_first_login = is_first_login
        self.app_role = app_role
        self.organisation = organisation
        self.organisation_role = organisation_role
        self.accessible_org_count = accessible_org_count
        self.accessible_opunit_count = accessible_opunit_count
        self.opunit = opunit
        self.opunit_role = opunit_role
        self.permissions = permissions

    def to_dict(self) -> dict:
        return {
            'id': self.id,
            'userId': self.user_id,
            'userName': self.user_name,
            'isFirstLogin': self.is_first_login,
            'appRole': self.app_role,
            'organisation': self.organisation,
            'organisationRole': self.organisation_role,
            'accessibleOrgCount': self.accessible_org_count,
            'accessibleOpunitCount': self.accessible_opunit_count,
            'opunit': self.opunit,
            'opunitRole': self.opunit_role,
            'permissions': self.permissions
        }


class SessionModel(db.Model):
    __tablename__ = 'session'

    id = db.Column(PGUUID(as_uuid=True), nullable=False, primary_key=True)
    user_id = db.Column(PGUUID(as_uuid=True), nullable=False)
    user_name = db.Column(db.String("256"), nullable=False)
    is_first_login = db.Column(db.Boolean(), nullable=False)
    app_role = db.Column(JSONB(), nullable=True)
    organisation = db.Column(JSONB(), nullable=True)
    organisation_role = db.Column(JSONB(), nullable=True)
    accessible_org_count = db.Column(db.Integer, nullable=False, default=0)
    opunit = db.Column(JSONB(), nullable=True)
    opunit_role = db.Column(JSONB(), nullable=True)
    accessible_opunit_count = db.Column(db.Integer, nullable=False, default=0)
    permissions = db.Column(JSONB(), nullable=True)
    is_active = db.Column(db.Boolean(), nullable=False, default=False)
    deactivate_reason = db.Column(db.String("4096"), nullable=True)
    created_on = db.Column(db.DateTime(timezone=True), nullable=False)
    deactivated_on = db.Column(db.DateTime(timezone=True))

    def to_cru_session(self) -> CruSession:
        return CruSession(
            id=self.id,
            user_name=self.user_name,
            is_first_login=self.is_first_login,
            user_id=self.user_id,
            app_role=self.app_role,
            organisation=self.organisation,
            organisation_role=self.organisation_role,
            accessible_org_count=self.accessible_org_count,
            opunit=self.opunit,
            opunit_role=self.opunit_role,
            accessible_opunit_count=self.accessible_opunit_count,
            permissions=self.permissions)


def register_session(session_resource: CruSession):
    session_record = SessionModel(
        id=session_resource.id,
        user_id=session_resource.user_id,
        user_name=session_resource.user_name,
        is_first_login=session_resource.is_first_login,
        app_role=session_resource.app_role,
        organisation=session_resource.organisation,
        organisation_role=session_resource.organisation_role,
        accessible_org_count=session_resource.accessible_org_count,
        opunit=session_resource.opunit,
        opunit_role=session_resource.opunit_role,
        accessible_opunit_count=session_resource.accessible_opunit_count,
        permissions=session_resource.permissions,
        is_active=True,
        deactivate_reason=None,
        created_on=func.now(),
        deactivated_on=None)
    db.session.add(session_record)


def get_session(session_id: UUID) -> CruSession:
    session_record = SessionModel.query.get(session_id)
    if session_record is not None:
        return session_record.to_cru_session()

    return None


def remove_session(session_id: UUID):
    session_record = SessionModel.query.get(session_id)
    if session_record is not None:
        db.session.delete(session_record)
        db.session.flush()


def ensure_selected_org(org_id: UUID):
    cru_session = request.cru_session
    if cru_session is None or cru_session.organisation is None or is_not_same_uuid(cru_session.organisation.get('id'), org_id):
        raise UnauthorizeError()


def SessionContext(required: bool = True):
    def decorator(func):
        def decorated_function(*args, **kwargs):
            session_required = required
            cru_session = None

            token_name = current_app.config.get('AUTH_TOKEN_NAME')
            jwt_secrety_key = current_app.config.get('JWT_SECRET_KEY')
            jwt_algo = current_app.config.get('JWT_ALGO')

            jwt_token = request.cookies.get(token_name)

            if jwt_token is not None:
                payload = jwt_decode(
                    jwt_token,
                    key=jwt_secrety_key,
                    verify=True,
                    algorithms=[jwt_algo])

                cru_session = get_session(payload.get('id'))

            if cru_session is None and session_required:
                raise SessionRequiredError()

            request.cru_session = cru_session
            return func(*args, **kwargs)

        return decorated_function

    return decorator


def PermissionRequired(any_of=(), all_of=()):
    def decorator(func):
        def decorated_function(*args, **kwargs):
            if request.cru_session is None:
                raise SessionRequiredError()

            for permission in all_of:
                pathBreakup = permission.split('.')
                has_permission = request.cru_session.permissions.get(pathBreakup[0], {}).get(pathBreakup[1], {}).get(pathBreakup[2], False)
                if not has_permission:
                    raise RequiredPermissionMissingError()

            has_any_of_permission = False
            for permission in any_of:
                pathBreakup = permission.split('.')
                has_permission = request.cru_session.permissions.get(pathBreakup[0], {}).get(pathBreakup[1], {}).get(pathBreakup[2], False)
                if has_permission:
                    has_any_of_permission = True
                    break

            if not has_any_of_permission:
                raise RequiredPermissionMissingError()

            return func(*args, **kwargs)

        return decorated_function

    return decorator


def OrgContextRequired(func):
    def ensure_org_context(*args, **kwargs):
        if request.cru_session is None:
            raise SessionRequiredError()

        if request.cru_session.organisation is None:
            raise OrgContextMissingError()

        return func(*args, **kwargs)

    return ensure_org_context
