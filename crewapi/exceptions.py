class ValidationError(Exception):
    def __init__(self, error=None, message='Invalid input received.'):
        self.error = error
        self.message = message


class SessionRequiredError(Exception):
    pass


class OrgContextMissingError(Exception):
    pass


class RequiredPermissionMissingError(Exception):
    pass


class UnauthorizeError(Exception):
    pass
