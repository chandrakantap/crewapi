from crewapi.ext import Transactional, ValidateRequest, db, API_SUCCESS
from crewapi.session_helper import SessionContext, OrgContextRequired, PermissionRequired
from .helper import UPDATE_CATALOG_SCHEMA
from crewapi.catalog.models import CatalogModel
from crewapi.exceptions import ValidationError
from crewapi.catalog.helper import map_catalog_model_to_dict
from flask import request
from sqlalchemy import func

@Transactional
@SessionContext(required = True)
@OrgContextRequired
@PermissionRequired(any_of = ('org.catalog.manage_catalog', 'org.catalog.review_catalog'))
@ValidateRequest(UPDATE_CATALOG_SCHEMA)
def update_catalog(catalog_id):
    catalog_data = request.validated_json
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    catalog = _ensure_catalog_exist(catalog_id, org_id)
    _ensure_unique_catalog_name(catalog_id, org_id, catalog_data.get("name"))

    catalog.name = catalog_data.get("name")
    catalog.description = catalog_data.get("description")
    catalog.last_modified_by = cru_session.user_id
    catalog.last_modified_on = func.now()

    db.session.flush()

    return API_SUCCESS(msg="Successfully updated catalog.", payload=map_catalog_model_to_dict(catalog))



def _ensure_catalog_exist(catalog_id, org_id):
    catalog = CatalogModel.query.filter_by(id = catalog_id, org_id = org_id).first()

    if catalog is None:
        raise ValidationError(message = "No data found with provided organisation and catalog.")

    return catalog


def _ensure_unique_catalog_name(catalog_id, org_id, catalog_name):
    CATALOG_COUNT_BY_NAME_SQL_UPDATE = """SELECT COUNT(*) as catalog_count FROM catalog
                                        WHERE LOWER(catalog.name)=LOWER(:NAME)
                                        AND catalog.org_id=:ORG_ID
                                        AND catalog.id != :CATALOG_ID"""

    existing_catalog = db.session.execute(CATALOG_COUNT_BY_NAME_SQL_UPDATE, {
        'NAME': catalog_name,
        'ORG_ID': org_id,
        'CATALOG_ID': catalog_id
    }).scalar()

    if existing_catalog != 0:
        raise ValidationError(
            error={"name": "catalog with same name already exist."}
        )
