from flask import Blueprint, request, abort
from .create_catalog_service import handle_create_request
from .update_catalog_service import update_catalog

catalog_bp = Blueprint('catalogs', __name__)


@catalog_bp.route('/catalogs', methods=('POST', 'GET'))
def organisations():
    if request.method == 'POST':
        return handle_create_request()

    abort(404)


@catalog_bp.route('/catalogs/<uuid:catalog_id>', methods=('PUT',))
def catalog(catalog_id):
    if request.method == 'PUT':
        return update_catalog(catalog_id)

    abort(404)
