
from crewapi.ext import ValidateRequest, db, API_SUCCESS, Transactional, STATUS_CONST
from crewapi.session_helper import SessionContext, PermissionRequired, OrgContextRequired
from .helper import CATALOG_SCHEMA, COPY_CATALOG_SCHEMA
from crewapi.catalog.helper import map_catalog_model_to_dict
from crewapi.catalog.models import CatalogModel
from crewapi.exceptions import ValidationError
from sqlalchemy import func
from flask import request
from uuid import uuid4


@Transactional
@SessionContext(required=True)
@OrgContextRequired
@PermissionRequired(any_of=('org.catalog.manage_catalog',))
def handle_create_request():
    request_json = request.json
    if request_json.get('copyFrom') is not None:
        return _copy_catalog()
    else:
        return _create_catalog()


@ValidateRequest(CATALOG_SCHEMA)
def _create_catalog():
    catalog_data = request.validated_json
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    _ensure_unique_catalog_name(catalog_data.get('name'), org_id)

    catalog = CatalogModel(
        id=uuid4(),
        name=catalog_data.get('name'),
        description=catalog_data.get('description'),
        org_id=org_id,
        status=STATUS_CONST.DRAFT,
        created_by=cru_session.user_id,
        created_on=func.now(),
        last_modified_by=cru_session.user_id,
        last_modified_on=func.now(),
    )
    db.session.add(catalog)
    db.session.flush()
    return API_SUCCESS(msg="Successfully created catalog.", payload=map_catalog_model_to_dict(catalog))


@ValidateRequest(COPY_CATALOG_SCHEMA)
def _copy_catalog():
    pass


def _ensure_unique_catalog_name(catalog_name, org_id):
    CATALOG_COUNT_BY_NAME_SQL = """SELECT count(*) as catalog_count FROM catalog
                              WHERE LOWER(catalog.name)=LOWER(:NAME)
                              AND catalog.org_id=:ORG_ID"""

    existing_catalog_count = db.session.execute(CATALOG_COUNT_BY_NAME_SQL, {
        'NAME': catalog_name,
        'ORG_ID': org_id
    }).scalar()

    if existing_catalog_count != 0:
        raise ValidationError(
            error={"name": "A catalog with same name already exist."}
        )
