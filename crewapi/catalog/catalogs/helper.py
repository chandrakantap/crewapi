import kanpai as Kanpai

CATALOG_SCHEMA = Kanpai.Object({
    "name": Kanpai.String().trim().required("Please provide catalog name").max(256),
    "description": Kanpai.String().trim().max(4096)
})

COPY_CATALOG_SCHEMA = Kanpai.Object({
    "name": Kanpai.String().trim().required("Please provide catalog name").max(256),
    "description": Kanpai.String().trim().max(4096),
    "copyFrom": Kanpai.UUID("Please provide valid catalog to copy.").required("Please provide catalog to copy")
})

UPDATE_CATALOG_SCHEMA = Kanpai.Object({
    "name": Kanpai.String().trim().required("Please provide catalog name").max(256),
    "description": Kanpai.String().trim().max(4096)
})
