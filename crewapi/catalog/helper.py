def map_catalog_model_to_dict(catalog_model):
    return {
        'id': catalog_model.id,
        'name': catalog_model.name,
        'description': catalog_model.description,
        'status': catalog_model.status,
        'createdBy': catalog_model.created_by,
        'createdOn': catalog_model.created_on.isoformat(),
        'lastModifiedBy': catalog_model.last_modified_by,
        'lastModifiedOn': catalog_model.last_modified_on.isoformat()
    }
