def catalog_init(app):
    from .catalogs import catalog_bp
    app.register_blueprint(catalog_bp, url_prefix=app.config['APPLICATION_ROOT'])

    from .categories import category_bp
    app.register_blueprint(category_bp, url_prefix=app.config['APPLICATION_ROOT'])

    from .product_types import product_type_bp
    app.register_blueprint(product_type_bp, url_prefix=app.config['APPLICATION_ROOT'])
