from flask import Blueprint, request, abort
from .create_category_service import create_category
from .update_category_service import update_category

category_bp = Blueprint('categories', __name__)


@category_bp.route('/catalogs/<uuid:catalog_id>/categories', methods=('POST', 'GET'))
def organisations(catalog_id):
    if request.method == 'POST':
        return create_category(catalog_id)

    abort(404)


@category_bp.route('/catalogs/<uuid:catalog_id>/categories/<uuid:category_id>', methods=('PUT',))
def catalog(catalog_id, category_id):
    if request.method == 'PUT':
        return update_category(catalog_id, category_id)

    abort(404)