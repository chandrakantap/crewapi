from crewapi.ext import ValidateRequest, db, API_SUCCESS, Transactional, STATUS_CONST
from crewapi.session_helper import SessionContext, PermissionRequired, OrgContextRequired
from .helper import CATEGORY_SCHEMA, map_category_model_to_dict
from crewapi.catalog.helper import map_catalog_model_to_dict
from crewapi.catalog.models import CategoryModel, CatalogModel
from crewapi.exceptions import ValidationError
from sqlalchemy import func
from flask import request
from uuid import uuid4


@Transactional
@SessionContext(required=True)
@OrgContextRequired
@PermissionRequired(any_of=('org.catalog.manage_catalog',))
@ValidateRequest(CATEGORY_SCHEMA)
def create_category(catalog_id):
    category_data = request.validated_json
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    catalog = _ensure_catalog_exist(catalog_id, org_id)
    _ensure_no_category_exist_with_name(catalog_id)
    _ensure_no_draft_category_exist_with_name(catalog_id)
    _ensure_parent_category_valid(catalog_id, category_data.get('parentCategoryId'))

    category = CategoryModel(
        id=uuid4(),
        name=category_data.get('name'),
        description=category_data.get('description'),
        parent_category_id=category_data.get('parentCategoryId'),
        catalog_id=catalog_id,
        status=STATUS_CONST.DRAFT,
        created_by=cru_session.user_id,
        created_on=func.now(),
        last_modified_by=cru_session.user_id,
        last_modified_on=func.now()
    )
    db.session.add(category)

    if catalog.status == STATUS_CONST.PUBLISHED:
        catalog.status = STATUS_CONST.PULISHED_WITH_DRAFT

    catalog.last_modified_by = cru_session.user_id
    catalog.last_modified_on = func.now()

    db.session.flush()
    payload = {
        'category': map_category_model_to_dict(category),
        'catalog': map_catalog_model_to_dict(catalog)
    }
    return API_SUCCESS(msg="Successfully created category.", payload=payload)


def _ensure_catalog_exist(catalog_id, org_id):
    catalog = CatalogModel.query.filter_by(id=catalog_id, org_id=org_id).first()
    if catalog is None:
        raise ValidationError(message="No catalog found.")

    return catalog


def _ensure_parent_category_valid(catalog_id, parent_category_id=None):
    if parent_category_id is not None:
        count = db.session.execute("""SELECT count(*) as count FROM category WHERE
        id=:PARENT_CATEGORY_ID AND catalog_id=:CATALOG_ID""", {
            'PARENT_CATEGORY_ID': parent_category_id,
            'CATALOG_ID': catalog_id
        }).scalar()

        if count == 0:
            raise ValidationError(
                error={"parentCategoryId": "Invalid parent category."}
            )


def _ensure_no_category_exist_with_name(catalog_id, table_name='category'):
    category_data = request.validated_json
    count = 0
    if category_data.get('parentCategoryId') is None:
        count = db.session.execute("""
        SELECT count(*) as count FROM {0}
        WHERE LOWER(name)=LOWER(:NAME)
        AND catalog_id=:CATALOG_ID
        AND parent_category_id IS NULL
        """.format(table_name), {
            'NAME': category_data.get('name'),
            'CATALOG_ID': catalog_id
        }).scalar()

    else:
        count = db.session.execute("""
        SELECT count(*) as count FROM {0}
        WHERE LOWER(name)=LOWER(:NAME)
        AND catalog_id=:CATALOG_ID
        AND parent_category_id = :PARENT_CATEGORY_ID
        """.format(table_name), {
            'NAME': category_data.get('name'),
            'CATALOG_ID': catalog_id,
            'PARENT_CATEGORY_ID': category_data.get('parentCategoryId')
        }).scalar()

    if count != 0:
        raise ValidationError(
            error={"name": "A category with same name already exist."}
        )


def _ensure_no_draft_category_exist_with_name(catalog_id):
    _ensure_no_category_exist_with_name(catalog_id, table_name='category_draft')
