import kanpai as Kanpai

CATEGORY_SCHEMA = Kanpai.Object({
    'name': Kanpai.String().trim().required('Please enter category name.').max(256),
    'description': Kanpai.String().trim().max(4096),
    'parentCategoryId': Kanpai.UUID()
})


def map_category_model_to_dict(category_model):
    return {
        'id': category_model.id,
        'name': category_model.name,
        'description': category_model.description,
        'parentCategoryId': category_model.parent_category_id,
        'status': category_model.status,
        'createdBy': category_model.created_by,
        'createdOn': category_model.created_on.isoformat(),
        'lastModifiedBy': category_model.last_modified_by,
        'lastModifiedOn': category_model.last_modified_on.isoformat()
    }

UPDATE_CATEGORY_SCHEMA = Kanpai.Object({
    'name': Kanpai.String().trim().required('Please enter category name.').max(256),
    'description': Kanpai.String().trim().max(4096),
    'parentCategoryId': Kanpai.UUID()
})


def map_draft_category_model_to_dict(draft_category_model, status):
    return {
        'id': draft_category_model.id,
        'name': draft_category_model.name,
        'description': draft_category_model.description,
        'parentCategoryId': draft_category_model.parent_category_id,
        'status': status,
        'createdBy': draft_category_model.created_by,
        'createdOn': draft_category_model.created_on.isoformat(),
        'lastModifiedBy': draft_category_model.last_modified_by,
        'lastModifiedOn': draft_category_model.last_modified_on.isoformat()
    }
