from crewapi.ext import Transactional, ValidateRequest, STATUS_CONST, db, API_SUCCESS
from crewapi.session_helper import SessionContext, OrgContextRequired, PermissionRequired
from .helper import UPDATE_CATEGORY_SCHEMA, map_category_model_to_dict, map_draft_category_model_to_dict
from crewapi.catalog.helper import map_catalog_model_to_dict
from flask import request
from crewapi.catalog.models import CatalogModel, CategoryModel, CategoryDraftModel
from crewapi.exceptions import ValidationError
from sqlalchemy import func

@Transactional
@SessionContext(required = True)
@OrgContextRequired
@PermissionRequired(any_of=('org.catalog.manage_catalog', 'org.catalog.review_catalog'))
@ValidateRequest(UPDATE_CATEGORY_SCHEMA)
def update_category(catalog_id, category_id):
    category_data = request.validated_json
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    catalog = _ensure_catalog_exist(catalog_id, org_id)
    category = _ensure_category_exist(category_id, catalog_id)

    _ensure_no_other_category_exist_with_same_name(category)
    _ensure_no_other_draft_category_exist_with_same_name(category)

    draft_category = None

    if category.status == STATUS_CONST.DRAFT:
        category = _update_category_for_draft_status(category, category_data, cru_session)
    
    elif category.status == STATUS_CONST.PUBLISHED:
        category = _update_category_for_published_status(category, cru_session)
        draft_category = _create_draft_category(category, category_data, cru_session)

    elif category.status == STATUS_CONST.PULISHED_WITH_DRAFT:
        draft_category = _update_draft_category(category, category_data, cru_session)

    if catalog.status == STATUS_CONST.PUBLISHED:
        catalog.status = STATUS_CONST.PULISHED_WITH_DRAFT
        catalog.last_modified_by = cru_session.user_id
        catalog.last_modified_on = func.now()

    db.session.flush()

    if category.status == STATUS_CONST.DRAFT:
        payload = {
            'category': map_category_model_to_dict(category),
            'catalog': map_catalog_model_to_dict(catalog)
        }
    else:
        payload = {
            'category': map_draft_category_model_to_dict(draft_category, category.status),
            'catalog': map_catalog_model_to_dict(catalog)
        }
    return API_SUCCESS(msg="Successfully updated category.", payload=payload)


def _ensure_catalog_exist(catalog_id, org_id):
    catalog = CatalogModel.query.filter_by(id=catalog_id, org_id=org_id).first()
    if catalog is None:
        raise ValidationError(message='No data found with provided organisation and catalog.')

    return catalog


def _ensure_category_exist(category_id, catalog_id):
    category = CategoryModel.query.filter_by(id = category_id, catalog_id = catalog_id).first()
    if category is None:
        raise ValidationError(message='No data found with provided catalog and category.')

    return category


def _ensure_no_other_category_exist_with_same_name(category, table_name='category'):
    name = request.validated_json.get('name')
    count = 0
    if category.parent_category_id is None:
        count = db.session.execute("""
        SELECT count(*) as count FROM {0}
        WHERE LOWER(name)=LOWER(:NAME)
        AND catalog_id=:CATALOG_ID
        AND id != :CATEGORY_ID
        AND parent_category_id IS NULL
        """.format(table_name), {
            'NAME': name,
            'CATALOG_ID': category.catalog_id,
            'CATEGORY_ID': category.id
        }).scalar()

    else:
        count = db.session.execute("""
        SELECT count(*) as count FROM {0}
        WHERE LOWER(name)=LOWER(:NAME)
        AND catalog_id=:CATALOG_ID
        AND id != :CATEGORY_ID
        AND parent_category_id = :PARENT_CATEGORY_ID
        """.format(table_name), {
            'NAME': name,
            'CATALOG_ID': category.catalog_id,
            'CATEGORY_ID': category.id,
            'PARENT_CATEGORY_ID': category.parent_category_id
        }).scalar()

    if count != 0:
        raise ValidationError(
            error={'name': 'A category with same name already exist.'}
        )


def _ensure_no_other_draft_category_exist_with_same_name(category):
    _ensure_no_other_category_exist_with_same_name(category, table_name='category_draft')


def _update_category_for_draft_status(category, category_data, cru_session):
    category.name = category_data.get('name')
    category.description = category_data.get('description')
    category.last_modified_by = cru_session.user_id
    category.last_modified_on = func.now()
    return category


def _update_category_for_published_status(category, cru_session):
    category.status = STATUS_CONST.PULISHED_WITH_DRAFT
    category.last_modified_by = cru_session.user_id
    category.last_modified_on = func.now()
    return category


def _create_draft_category(category, category_data, cru_session):
    draft_category = CategoryDraftModel(
        id = category.id,
        name = category_data.get('name'),
        description = category_data.get('description'),
        parent_category_id = category.parent_category_id,
        catalog_id = category.catalog_id,
        created_by = cru_session.user_id,
        created_on = func.now(),
        last_modified_by = cru_session.user_id,
        last_modified_on = func.now()
    )

    db.session.add(draft_category)

    return draft_category


def _update_draft_category(category, category_data, cru_session):
    draft_category = _ensure_draft_category_exist(category.id, category.catalog_id)

    draft_category.name = category_data.get('name')
    draft_category.description = category_data.get('description')
    draft_category.last_modified_by = cru_session.user_id
    draft_category.last_modified_on = func.now()

    return draft_category



def _ensure_draft_category_exist(category_id, catalog_id):
    draft_category = CategoryDraftModel.query.filter_by(id = category_id, catalog_id = catalog_id).first()
    if draft_category is None:
        raise ValidationError(message='No data found with provided catalog and draft category.')

    return draft_category
