from crewapi.ext import db
from sqlalchemy.dialects.postgresql import UUID as PGUUID, JSONB


class CatalogModel(db.Model):
    __tablename__ = 'catalog'

    id = db.Column(PGUUID(as_uuid=True), primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    description = db.Column(db.String(4096), nullable=True)
    org_id = db.Column(PGUUID(as_uuid=True), nullable=False)
    status = db.Column(db.String(4), nullable=False)
    created_by = db.Column(PGUUID(as_uuid=True))
    created_on = db.Column(db.DateTime(timezone=True))
    last_modified_by = db.Column(PGUUID(as_uuid=True))
    last_modified_on = db.Column(db.DateTime(timezone=True))


class CategoryModel(db.Model):
    __tablename__ = 'category'

    id = db.Column(PGUUID(as_uuid=True), primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    description = db.Column(db.String(4096), nullable=True)
    parent_category_id = db.Column(PGUUID(as_uuid=True), nullable=True)
    catalog_id = db.Column(PGUUID(as_uuid=True), nullable=False)
    status = db.Column(db.String(4), nullable=False)
    created_by = db.Column(PGUUID(as_uuid=True))
    created_on = db.Column(db.DateTime(timezone=True))
    last_modified_by = db.Column(PGUUID(as_uuid=True))
    last_modified_on = db.Column(db.DateTime(timezone=True))


class CategoryDraftModel(db.Model):
    __tablename__ = 'category_draft'

    id = db.Column(PGUUID(as_uuid=True), primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    description = db.Column(db.String(4096), nullable=True)
    parent_category_id = db.Column(PGUUID(as_uuid=True), nullable=True)
    catalog_id = db.Column(PGUUID(as_uuid=True), nullable=False)
    created_by = db.Column(PGUUID(as_uuid=True))
    created_on = db.Column(db.DateTime(timezone=True))
    last_modified_by = db.Column(PGUUID(as_uuid=True))
    last_modified_on = db.Column(db.DateTime(timezone=True))


class ProductTypeModel(db.Model):
    __tablename__ = 'product_type'

    id = db.Column(PGUUID(as_uuid=True), primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    description = db.Column(db.String(4096), nullable=True)
    org_id = db.Column(PGUUID(as_uuid=True), nullable=True)
    status = db.Column(db.String(4), nullable=False)
    field_groups = db.Column(JSONB(), nullable=True)
    fields = db.Column(JSONB(), nullable=True)
    variation_fields = db.Column(JSONB(), nullable=True)
    units = db.Column(JSONB(), nullable=True)
    created_by = db.Column(PGUUID(as_uuid=True))
    created_on = db.Column(db.DateTime(timezone=True))
    last_modified_by = db.Column(PGUUID(as_uuid=True))
    last_modified_on = db.Column(db.DateTime(timezone=True))
