from flask import Blueprint, request, abort
from .create_product_type_service import create_product_type
from .update_product_type_service import update_product_type
from .get_product_type_service import get_product_types

product_type_bp = Blueprint('product_type', __name__)


@product_type_bp.route('/product-types', methods=('POST', 'GET'))
def product_types():
    if request.method == 'POST':
        return create_product_type()
    elif request.method == 'GET':
        return get_product_types()

    abort(404)


@product_type_bp.route('/product-types/<uuid:id>', methods=('PUT', 'DELETE'))
def product_type(id):
    if request.method == 'PUT':
        return update_product_type(id)

    abort(404)
