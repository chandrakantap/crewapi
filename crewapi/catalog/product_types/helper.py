import kanpai as Kanpai
from crewapi.exceptions import ValidationError
from crewapi.ext import get_indexes_with_duplicate

KEY_FOR_NO_GROUP = '_not_in_group_'

FIELD_OBJECT_SCHEMA = {
    'displayName': Kanpai.String().trim().required('Please provide field display name'),
    'displayOrder': Kanpai.Number().integer().required('Please provide display order').min(1),
    'key': Kanpai.String().trim().required('Please provide field key.'),
    'type': (Kanpai.String().trim()
             .required('Please provide field type.')
             .anyOf(['STXT', 'LTXT', 'COLR', 'NMBR', 'MKDN'], error="Invalid type provided."))
}

FIELD_SCHEMA = Kanpai.Array(convert_none_to_empty=True).of(Kanpai.Object(FIELD_OBJECT_SCHEMA))

VARIATION_SCHEMA = Kanpai.Array(convert_none_to_empty=True).of(Kanpai.Object({
    **FIELD_OBJECT_SCHEMA,
    'type': Kanpai.String().trim().anyOf(['STXT', 'COLR', 'NMBR'], error="Invalid type provided.")
}))

UNIT_SCHEMA = Kanpai.Array(convert_none_to_empty=True).of(Kanpai.Object({
    'key': Kanpai.String().trim().required('Please provide key'),
    'singular': Kanpai.String().trim().required('Please provide unit singular form.'),
    'plural': Kanpai.String().trim().required('Please provide unit plural form.')
})).min(1, error="Please provide at least one unit.")

FIELD_GROUP_SCHEMA = Kanpai.Array(convert_none_to_empty=True).of(Kanpai.Object({
    'displayName': Kanpai.String().trim().required('Please provide field display name'),
    'displayOrder': Kanpai.Number().integer().required('Please provide display order').min(1),
    'key': Kanpai.String().trim().required('Please provide group key.'),
    'items': Kanpai.Array(convert_none_to_empty=True).of(Kanpai.Object(FIELD_OBJECT_SCHEMA)).min(1, error="Please provide at least one item.")
}))

PRODUCT_TYPE_SCHEMA = Kanpai.Object({
    'name': Kanpai.String().trim().required('Please provide product type name.'),
    'description': Kanpai.String().trim(),
    'status': Kanpai.String().trim().anyOf(['A', 'IA'], error="Invalid status given."),
    'fieldGroups': FIELD_GROUP_SCHEMA,
    'fields': FIELD_SCHEMA,
    'variations': VARIATION_SCHEMA,
    'units': UNIT_SCHEMA
}).required('Please provide product type data.')


def ensure_group_key_are_unique(product_type_data):
    has_error = False
    errors = {}

    duplicates = get_indexes_with_duplicate(product_type_data.get('fieldGroups'), lambda group: group.get('key'))
    if len(duplicates) > 0:
        errors['fieldGroups'] = {}
        has_error = True
        for index in duplicates:
            errors['fieldGroups'][str(index)] = {}
            errors['fieldGroups'][str(index)]['key'] = 'Group key must be unique'

    if has_error:
        raise ValidationError(error=errors)


def ensure_field_keys_are_unique(product_type_data):
    has_error = False
    errors = {}

    product_type_fields = [*product_type_data.get('fields'), *product_type_data.get('variations')]
    for group in product_type_data.get('fieldGroups'):
        product_type_fields += group.get('items')

    field_duplicates = get_indexes_with_duplicate(product_type_fields, lambda field: field.get('key'))
    if len(field_duplicates) > 0:
        errors['fields'] = {}
        has_error = True
        for index in field_duplicates:
            errors['fields'][str(index)] = {}
            errors['fields'][str(index)]['key'] = 'Field key must be unique'

    unit_duplicates = get_indexes_with_duplicate(product_type_data.get('units'), lambda unit: unit.get('key'))
    if len(unit_duplicates) > 0:
        errors = {'units': {}}
        for index in unit_duplicates:
            errors['units'][str(index)] = {}
            errors['units'][str(index)]['key'] = 'Unit key must be unique'
            raise ValidationError(error=errors)

    if has_error:
        raise ValidationError(error=errors)


def map_product_type_model_to_dict(product_type_model):
    return {
        "id": product_type_model.id,
        "name": product_type_model.name,
        "description": product_type_model.description,
        "status": product_type_model.status,
        "fields": product_type_model.fields,
        "variations": product_type_model.variation_fields,
        "units": product_type_model.units,
        "fieldGroups": product_type_model.field_groups,
        'createdBy': product_type_model.created_by,
        'createdOn': product_type_model.created_on.isoformat(),
        'lastModifiedBy': product_type_model.last_modified_by,
        'lastModifiedOn': product_type_model.last_modified_on.isoformat()
    }
