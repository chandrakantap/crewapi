from crewapi.ext import ValidateRequest, db, API_SUCCESS, Transactional
from crewapi.session_helper import SessionContext, PermissionRequired, OrgContextRequired
from .helper import (PRODUCT_TYPE_SCHEMA, map_product_type_model_to_dict,
                     ensure_group_key_are_unique,
                     ensure_field_keys_are_unique)
from crewapi.catalog.models import ProductTypeModel
from crewapi.exceptions import ValidationError
from sqlalchemy import func
from flask import request


@Transactional
@SessionContext(required=True)
@OrgContextRequired
@PermissionRequired(any_of=('org.product_type.manage_product_type',))
@ValidateRequest(PRODUCT_TYPE_SCHEMA)
def update_product_type(product_type_id):
    product_type_data = request.validated_json
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    product_type = _ensure_product_type_exist(product_type_id, org_id)
    _ensure_product_type_name_is_unique(product_type_id, product_type_data.get('name'), org_id)
    ensure_group_key_are_unique(product_type_data)
    ensure_field_keys_are_unique(product_type_data)

    product_type.name = product_type_data.get('name')
    product_type.description = product_type_data.get('description')
    product_type.status = product_type_data.get('status') or product_type.status
    product_type.field_groups = product_type_data.get('fieldGroups')
    product_type.fields = product_type_data.get('fields')
    product_type.variation_fields = product_type_data.get('variations')
    product_type.units = product_type_data.get('units')
    product_type.last_modified_by = cru_session.user_id,
    product_type.last_modified_on = func.now()

    db.session.flush()
    return API_SUCCESS(msg="Successfully updated product type.", payload=map_product_type_model_to_dict(product_type))


def _ensure_product_type_name_is_unique(id, name, org_id):
    COUNT_SQL = """SELECT count(*) as count FROM product_type
                            WHERE LOWER(name)=LOWER(:NAME)
                            AND org_id=:ORG_ID
                            AND id!=:ID"""

    count = db.session.execute(COUNT_SQL, {
        'NAME': name,
        'ORG_ID': org_id,
        'ID': id
    }).scalar()

    if count != 0:
        raise ValidationError(
            error={"name": "A product type with same name already exist."}
        )


def _ensure_product_type_exist(product_type_id, org_id):
    product_type = ProductTypeModel.query.filter_by(id=product_type_id, org_id=org_id).first()
    if product_type is None:
        raise ValidationError(message="No product type found.")

    return product_type
