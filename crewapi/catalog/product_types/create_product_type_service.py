from crewapi.ext import ValidateRequest, db, API_SUCCESS, Transactional, STATUS_CONST
from crewapi.session_helper import SessionContext, PermissionRequired, OrgContextRequired
from .helper import (PRODUCT_TYPE_SCHEMA, map_product_type_model_to_dict,
                     ensure_group_key_are_unique,
                     ensure_field_keys_are_unique)
from crewapi.catalog.models import ProductTypeModel
from crewapi.exceptions import ValidationError
from sqlalchemy import func
from flask import request
from uuid import uuid4


@Transactional
@SessionContext(required=True)
@OrgContextRequired
@PermissionRequired(any_of=('org.product_type.manage_product_type',))
@ValidateRequest(PRODUCT_TYPE_SCHEMA)
def create_product_type():
    product_type_data = request.validated_json
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    _ensure_product_type_name_is_unique(product_type_data.get('name'), org_id)
    ensure_group_key_are_unique(product_type_data)
    ensure_field_keys_are_unique(product_type_data)

    product_type = ProductTypeModel(
        id=uuid4(),
        name=product_type_data.get('name'),
        description=product_type_data.get('description'),
        org_id=org_id,
        status=STATUS_CONST.ACTIVE,
        field_groups=product_type_data.get('fieldGroups'),
        fields=product_type_data.get('fields'),
        variation_fields=product_type_data.get('variations'),
        units=product_type_data.get('units'),
        created_by=cru_session.user_id,
        created_on=func.now(),
        last_modified_by=cru_session.user_id,
        last_modified_on=func.now()
    )
    db.session.add(product_type)
    db.session.flush()
    return API_SUCCESS(msg="Successfully created product type.", payload=map_product_type_model_to_dict(product_type))


def _ensure_product_type_name_is_unique(name, org_id):
    COUNT_SQL = """SELECT count(*) as count FROM product_type
                            WHERE LOWER(name)=LOWER(:NAME)
                            AND org_id=:ORG_ID"""

    count = db.session.execute(COUNT_SQL, {
        'NAME': name,
        'ORG_ID': org_id
    }).scalar()

    if count != 0:
        raise ValidationError(
            error={"name": "A product type with same name already exist."}
        )
