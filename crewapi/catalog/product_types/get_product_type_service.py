from flask import request, current_app
from crewapi.ext import Transactional, API_SUCCESS, empty_result, paged_result
from crewapi.session_helper import SessionContext, PermissionRequired, OrgContextRequired
from crewapi.catalog.models import ProductTypeModel
from .helper import map_product_type_model_to_dict


@Transactional
@SessionContext(required=True)
@OrgContextRequired
@PermissionRequired(any_of=('org.product_type.manage_product_type', 'org.product_type.view_product_type'))
def get_product_types():
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    item_per_page_default = current_app.config.get('ITEM_PER_PAGE')
    page = request.args.get('page', default=1, type=int)
    item_per_page = request.args.get('item_per_page', default=item_per_page_default, type=int)

    query_result = (ProductTypeModel.query
                    .filter_by(org_id=org_id)
                    .order_by(ProductTypeModel.last_modified_on.desc())
                    .paginate(page, item_per_page, error_out=False))

    if query_result.total == 0:
        return API_SUCCESS(payload=empty_result())
    else:
        product_types = query_result.items
        return API_SUCCESS(payload=paged_result(
            query_result,
            items=[map_product_type_model_to_dict(product_type) for product_type in product_types]))
