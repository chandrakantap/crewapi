from uuid import uuid4
from crewapi.core.models import PermissionModel, UserModel, RoleModel, Constants
from crewapi.session_helper import CruSession
from crewapi.ext import db


def prepare_session_resource(user: UserModel) -> CruSession:
    selected_org, selected_org_role, accessible_org_count = _get_selected_org(user.id, user.last_selected_org_id)
    selected_opunit, selected_opunit_role, visible_opunit_count = _get_selected_opunit(user.id, selected_org, user.last_selected_opunit_id)

    if selected_org is not None:
        user.last_selected_org_id = selected_org.get('id')

    if selected_opunit is not None:
        user.last_selected_opunit_id = selected_opunit.get('id')

    return CruSession(
        id=uuid4(),
        user_id=user.id,
        user_name=user.name,
        is_first_login=user.is_first_login,
        app_role=None if user.app_role is None else {
            'id': user.app_role.id,
            'name': user.app_role.name
        },
        organisation=selected_org,
        organisation_role=None if selected_org_role is None else {
            'id': selected_org_role.id,
            'name': selected_org_role.name
        },
        accessible_org_count=accessible_org_count,
        opunit=selected_opunit,
        opunit_role=None if selected_opunit is None else {
            'id': selected_opunit_role.id,
            'name': selected_opunit_role.name
        },
        accessible_opunit_count=visible_opunit_count,
        permissions=_get_permissions(
            app_role=user.app_role,
            org_role=selected_org_role,
            opunit_role=selected_opunit_role)
    )


def _get_permissions(app_role=None, org_role=None, opunit_role=None):
    permission_models = []
    if app_role is not None:
        if app_role.is_admin():
            permission_models = permission_models + PermissionModel.query.filter_by(
                entity_name=Constants.PERMISSION_ENTITY_APP).all()
        else:
            permission_models = permission_models + app_role.permissions

    if org_role is not None:
        if org_role.is_admin():
            permission_models = permission_models + PermissionModel.query.filter_by(
                entity_name=Constants.PERMISSION_ENTITY_ORG).all()
        else:
            permission_models = permission_models + org_role.permissions

    if opunit_role is not None:
        if opunit_role.is_admin():
            permission_models = permission_models + PermissionModel.query.filter_by(
                entity_name=Constants.PERMISSION_ENTITY_OPUNIT).all()
        else:
            permission_models = permission_models + opunit_role.permissions

    permission = {}

    for permission_model in permission_models:
        entity_name = permission_model.entity_name.lower()
        resource_name = permission_model.resource_name.replace(r' ', '_').lower()
        operation_name = permission_model.operation_name.lower()

        permission[entity_name] = permission.get(entity_name, {})
        permission[entity_name][resource_name] = permission.get(
            entity_name).get(resource_name, {})
        permission[entity_name][resource_name][operation_name] = True

    return permission


GET_SELECTED_ORG_SQL = """SELECT id, name FROM opunit_user ou
                          JOIN opunit o ON ou.org_id = o.id
                          WHERE ou.user_id =:USER_ID
                          AND ou.org_id =:SELECTED_ORG_ID
                          LIMIT 1"""


def _get_selected_org(user_id, last_selected_org_id=None):
    if last_selected_org_id is None:
        return _get_an_accessible_org(user_id)
    else:
        selected_org = db.session.execute(GET_SELECTED_ORG_SQL, params={
            'USER_ID': user_id,
            'SELECTED_ORG_ID': last_selected_org_id
        }).first()

        if selected_org is not None:
            selected_org_role = _get_selected_org_role(selected_org['id'], user_id)
            accessible_org_count = _get_accessible_org_count(user_id)
            return (
                {
                    'id': selected_org['id'],
                    'name': selected_org['name']
                },
                selected_org_role,
                accessible_org_count)
        else:
            return _get_an_accessible_org(user_id)


GET_AN_ACCESSIBLE_ORG_SQL = """SELECT id, name FROM opunit_user ou
                               JOIN opunit o ON ou.org_id = o.id
                               WHERE ou.user_id =:USER_ID
                               LIMIT 1"""


def _get_an_accessible_org(user_id):
    accessible_org = db.session.execute(GET_AN_ACCESSIBLE_ORG_SQL, params={
        'USER_ID': user_id
    }).first()

    if accessible_org is not None:
        selected_org_role = _get_selected_org_role(accessible_org['id'], user_id)
        accessible_org_count = _get_accessible_org_count(user_id)
        return (
            {
                'id': accessible_org['id'],
                'name': accessible_org['name']
            },
            selected_org_role,
            accessible_org_count)

    return (None, None, 0)


GET_SELECTED_ORG_ROLE_SQL = """SELECT role_id  FROM opunit_user ou
                               WHERE ou.user_id =:USER_ID
                               AND ou.opunit_id =:ORG_ID
                               LIMIT 1"""


def _get_selected_org_role(selected_org_id, user_id):
    if selected_org_id is None:
        return None

    role_id = db.session.execute(GET_SELECTED_ORG_ROLE_SQL, params={
        'USER_ID': user_id,
        'ORG_ID': selected_org_id
    }).scalar()

    if role_id is not None:
        return RoleModel.query.get(role_id)

    return None


GET_ACCESSIBLE_ORGS_COUNT_SQL = """SELECT COUNT(DISTINCT org_id) as accessibleOrgCount
                                   FROM opunit_user ou
                                   WHERE ou.user_id =:USER_ID"""


def _get_accessible_org_count(user_id):
    return db.session.execute(GET_ACCESSIBLE_ORGS_COUNT_SQL, params={
        'USER_ID': user_id
    }).scalar()


GET_SELECTED_OPUNIT_SQL = """SELECT o.id, o.name, ou.role_id
                                FROM opunit_user ou
                                JOIN visible_opunits vou ON ou.opunit_id = vou.opunit_id
                                JOIN opunit o ON o.id = vou.visible_opunit
                                WHERE ou.org_id = :ORG_ID
                                AND ou.user_id = :USER_ID
                                AND vou.visible_opunit = :SELECTED_OPUNIT_ID
                                LIMIT 1"""


def _get_selected_opunit(user_id, organisation=None, last_selected_opunit_id=None):
    if organisation is None:
        return (None, None, 0)

    if last_selected_opunit_id is None:
        return _get_an_accessible_opunit(user_id, organisation.get('id'))
    else:
        selected_opunit = db.session.execute(GET_SELECTED_OPUNIT_SQL, params={
            'USER_ID': user_id,
            'ORG_ID': organisation.get('id'),
            'SELECTED_OPUNIT_ID': last_selected_opunit_id
        }).first()

        if selected_opunit is not None:
            selected_opunit_role = RoleModel.query.get(selected_opunit['role_id'])
            visible_opunit_count = _get_visible_opunit_count(organisation.get('id'), user_id)
            return (
                {
                    'id': selected_opunit['id'],
                    'name': selected_opunit['name']
                },
                selected_opunit_role,
                visible_opunit_count
            )
        else:
            return _get_an_accessible_opunit(user_id, organisation.get('id'))


GET_AN_ACCESSIBLE_OPUNIT_SQL = """SELECT o.id,o.name,ou.role_id
                                    FROM opunit_user ou
                                    JOIN opunit o ON o.id = ou.opunit_id
                                    WHERE ou.org_id= :ORG_ID
                                    AND ou.user_id = :USER_ID
                                    LIMIT 1"""


def _get_an_accessible_opunit(user_id, org_id):
    accessible_opunit = db.session.execute(GET_AN_ACCESSIBLE_OPUNIT_SQL, params={
        'USER_ID': user_id,
        'ORG_ID': org_id
    }).first()

    if accessible_opunit is not None:
        selected_opunit_role = RoleModel.query.get(accessible_opunit['role_id'])
        visible_opunit_count = _get_visible_opunit_count(org_id, user_id)
        return (
            {
                'id': accessible_opunit['id'],
                'name': accessible_opunit['name']
            },
            selected_opunit_role,
            visible_opunit_count)

    return (None, None, 0)


VISIBLE_OPUNIT_COUNT_SQL = """SELECT COUNT(DISTINCT vou.visible_opunit) as resultCount
                                FROM opunit_user ou
                                JOIN visible_opunits vou ON ou.opunit_id = vou.opunit_id
                                WHERE ou.org_id = :ORG_ID
                                AND ou.user_id = :USER_ID"""


def _get_visible_opunit_count(org_id, user_id):
    return db.session.execute(VISIBLE_OPUNIT_COUNT_SQL, params={
        'USER_ID': user_id,
        'ORG_ID': org_id
    }).scalar()
