from flask import Blueprint, request, abort
from .service import create_session, get_session, delete_session

sessionbp = Blueprint('session', __name__)


@sessionbp.route('/sessions', methods=('POST', 'GET', 'DELETE'))
def sessions():
    if request.method == 'POST':
        return create_session()
    elif request.method == 'GET':
        return get_session()
    elif request.method == 'DELETE':
        return delete_session()
    else:
        abort(404)
