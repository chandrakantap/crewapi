import kanpai as Kanpai
from flask import request, make_response, current_app, json
from crewapi.ext import Transactional, API_SUCCESS, ValidateRequest
from crewapi.exceptions import ValidationError
from crewapi.core.models import UserModel
from .helper import prepare_session_resource
from crewapi.core.helper import password_does_not_match
from crewapi.session_helper import CruSession, register_session, remove_session, SessionContext
from jwt import encode as jwt_encode


SESSION_SCHEMA = Kanpai.Object({
    'mobileNo': Kanpai.String().trim().required(error="Please enter your mobile no."),
    'mobileNoCountryCode': Kanpai.String().trim().required(error="Please enter country code of mobile no."),
    'password': Kanpai.String().required(error="Please enter password")
})


@Transactional
@ValidateRequest(SESSION_SCHEMA)
def create_session():
    session_data = request.validated_json
    user = UserModel.query.filter_by(mobile_no=session_data.get('mobileNo'),
                                     mobile_no_country_code=session_data.get('mobileNoCountryCode')).first()

    if user is None or password_does_not_match(user.password, session_data.get('password')):
        raise ValidationError(message="No user found with provide mobile no and password.")

    session_resource = prepare_session_resource(user)
    register_session(session_resource)
    return prepare_response(session_resource, message="Log in successfully.")


@Transactional
@SessionContext(required=True)
def get_session():
    return prepare_response(session_resource=request.cru_session)


@Transactional
@SessionContext(required=False)
def delete_session():
    cru_session = request.cru_session

    if cru_session is not None:
        remove_session(cru_session.id)

    response = make_response(API_SUCCESS())
    token_name = current_app.config.get('AUTH_TOKEN_NAME')
    response.set_cookie(token_name, value='', httponly=True, expires=6307200)

    return response


def prepare_response(session_resource: CruSession, message="Success"):
    response = make_response(API_SUCCESS(msg=message, payload=session_resource.to_dict()))

    token_name = current_app.config.get('AUTH_TOKEN_NAME')
    jwt_secrety_key = current_app.config.get('JWT_SECRET_KEY')
    jwt_algo = current_app.config.get('JWT_ALGO')

    token_data = {'id': session_resource.id}
    session_token = jwt_encode(token_data, jwt_secrety_key, algorithm=jwt_algo, json_encoder=json.JSONEncoder)

    response.set_cookie(token_name, session_token, httponly=True)
    return response
