from crewapi.ext import API_SUCCESS, Transactional, empty_result
from crewapi.session_helper import (SessionContext, PermissionRequired)
from crewapi.core.models import PermissionModel


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('app.role.manage_role',))
def get_app_permissions():
    return _get_permissions(('APP',))


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('org.role.manage_role',))
def get_org_permissions():
    return _get_permissions(('ORG', 'OPUNIT'))


def _get_permissions(entities):
    query_result = (PermissionModel.query
                    .filter(PermissionModel.entity_name.in_(entities))
                    .order_by(PermissionModel.resource_name.desc())
                    .all())

    return API_SUCCESS(payload=[_map_permission_model_to_dict(permission) for permission in query_result])


def _map_permission_model_to_dict(permission):
    return {
        "id": permission.id,
        "resourceName": permission.resource_name,
        "name": permission.name,
        "description": permission.description
    }
