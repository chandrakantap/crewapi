from flask import request, current_app
from crewapi.ext import API_SUCCESS, Transactional, app_id, empty_result, paged_result
from crewapi.session_helper import (SessionContext, PermissionRequired, OrgContextRequired)
from .helper import map_role_model_to_dict, get_can_grant_roles
from crewapi.core.models import RoleModel


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('app.role.view_roles',))
def get_app_roles():
    return _get_roles(app_id)


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('org.role.view_roles',))
@OrgContextRequired
def get_org_roles():
    return _get_roles(request.cru_session.organisation.get('id'))


def _get_roles(owner_id):
    item_per_page_default = current_app.config.get('ITEM_PER_PAGE')
    name_starts_with = request.args.get('nameStartsWith', type=str)
    page = request.args.get('page', default=1, type=int)
    item_per_page = request.args.get('itemPerPage', default=item_per_page_default, type=int)

    filters = [RoleModel.owner_id == owner_id]
    if name_starts_with is not None:
        filters.append(RoleModel.name.startswith(name_starts_with, autoescape=True))

    query_result = (RoleModel.query
                    .join(RoleModel.permissions)
                    .filter(*filters)
                    .order_by(RoleModel.last_modified_by.desc())
                    .paginate(page, item_per_page, error_out=False))

    if query_result.total == 0:
        return API_SUCCESS(payload=empty_result())
    else:
        roles = query_result.items
        role_can_grant_roles = get_can_grant_roles([role.id for role in roles])
        return API_SUCCESS(payload=paged_result(
            query_result,
            items=[map_role_model_to_dict(role, role_can_grant_roles.get(role.id, [])) for role in roles]
        ))
