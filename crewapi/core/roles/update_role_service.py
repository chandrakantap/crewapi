from flask import request
from crewapi.ext import ValidateRequest, API_SUCCESS, Transactional, app_id, db
from crewapi.session_helper import (SessionContext, PermissionRequired, OrgContextRequired)
from .helper import (map_role_model_to_dict, filter_app_permissions, filter_app_roles,
                     ensure_role_name_is_not_admin, ensure_role_name_is_unique_for_edit,
                     ROLE_SCHEMA, filter_org_permissions, filter_org_roles,
                     ensure_role_exists)
from sqlalchemy import func


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('app.role.manage_role',))
@ValidateRequest(ROLE_SCHEMA)
def edit_app_role(role_id):
    role_data = request.validated_json
    cru_session = request.cru_session

    role = ensure_role_exists(app_id, role_id)

    ensure_role_name_is_not_admin(role_data.get('name'))
    ensure_role_name_is_unique_for_edit(role_data.get('name'), app_id, role_id)

    permissions = filter_app_permissions(role_data.get('permissions'))
    can_grant_roles = filter_app_roles(role_data.get('canGrantRoles'))

    role.name = role_data.get('name')
    role.description = role_data.get('description')
    role.last_modified_by = cru_session.user_id
    role.last_modified_on = func.now()
    role.permissions = permissions
    role.can_grant_roles = can_grant_roles

    db.session.flush()

    return API_SUCCESS(msg="Successfully updated role.", payload=map_role_model_to_dict(role, role.can_grant_roles))


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('org.role.manage_role',))
@ValidateRequest(ROLE_SCHEMA)
@OrgContextRequired
def edit_org_role(role_id):

    role_data = request.validated_json
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    role = ensure_role_exists(org_id, role_id)

    ensure_role_name_is_not_admin(role_data.get('name'))
    ensure_role_name_is_unique_for_edit(role_data.get('name'), org_id, role_id)

    permissions = filter_org_permissions(role_data.get('permissions'))
    can_grant_roles = filter_org_roles(role_data.get('canGrantRoles'), org_id)

    role.name = role_data.get('name')
    role.description = role_data.get('description')
    role.last_modified_by = cru_session.user_id
    role.last_modified_on = func.now()
    role.permissions = permissions
    role.can_grant_roles = can_grant_roles

    db.session.flush()

    return API_SUCCESS(msg="Successfully updated role.", payload=map_role_model_to_dict(role, role.can_grant_roles))
