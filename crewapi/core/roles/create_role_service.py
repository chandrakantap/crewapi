from flask import request
from crewapi.ext import ValidateRequest, API_SUCCESS, Transactional, app_id, db
from crewapi.session_helper import (SessionContext, PermissionRequired, OrgContextRequired)
from .helper import (map_role_model_to_dict, filter_app_permissions, filter_app_roles,
                     ensure_role_name_is_not_admin, ensure_role_name_is_unique,
                     ROLE_SCHEMA, filter_org_permissions, filter_org_roles)
from crewapi.core.models import RoleModel
from uuid import uuid4
from sqlalchemy import func


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('app.role.manage_role',))
@ValidateRequest(ROLE_SCHEMA)
def create_app_role():
    role_data = request.validated_json
    cru_session = request.cru_session

    ensure_role_name_is_not_admin(role_data.get('name'))
    ensure_role_name_is_unique(role_data.get('name'), app_id)

    permissions = filter_app_permissions(role_data.get('permissions'))
    can_grant_roles = filter_app_roles(role_data.get('canGrantRoles'))

    role = RoleModel(
        id=uuid4(),
        name=role_data.get('name'),
        description=role_data.get('description'),
        owner_id=app_id,
        created_by=cru_session.user_id,
        created_on=func.now(),
        last_modified_by=cru_session.user_id,
        last_modified_on=func.now(),
        permissions=permissions,
        can_grant_roles=can_grant_roles
    )
    db.session.add(role)
    return API_SUCCESS(msg="Successfully created role.", payload=map_role_model_to_dict(role, role.can_grant_roles))


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('org.role.manage_role',))
@ValidateRequest(ROLE_SCHEMA)
@OrgContextRequired
def create_org_role():
    role_data = request.validated_json
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    ensure_role_name_is_not_admin(role_data.get('name'))
    ensure_role_name_is_unique(role_data.get('name'), org_id)

    permissions = filter_org_permissions(role_data.get('permissions'))
    can_grant_roles = filter_org_roles(role_data.get('canGrantRoles'), org_id)

    role = RoleModel(
        id=uuid4(),
        name=role_data.get('name'),
        description=role_data.get('description'),
        owner_id=org_id,
        created_by=cru_session.user_id,
        created_on=func.now(),
        last_modified_by=cru_session.user_id,
        last_modified_on=func.now(),
        permissions=permissions,
        can_grant_roles=can_grant_roles
    )
    db.session.add(role)
    return API_SUCCESS(msg="Successfully created role.", payload=map_role_model_to_dict(role, role.can_grant_roles))
