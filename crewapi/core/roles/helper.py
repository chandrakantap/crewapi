import kanpai as Kanpai
from crewapi.exceptions import ValidationError
from crewapi.ext import db, app_id, MinimalDTO
from crewapi.core.models import PermissionModel, RoleModel

ROLE_SCHEMA = Kanpai.Object({
    'name': Kanpai.String().trim().required('Please provide role name'),
    'description': Kanpai.String().trim(),
    'permissions': (Kanpai.Array(convert_none_to_empty=True)
                    .of(Kanpai.UUID())
                    .min(1, error="At least one permission is required.")),
    'canGrantRoles': Kanpai.Array(convert_none_to_empty=True).of(Kanpai.UUID())
}).required('Missing required data')


def ensure_role_name_is_not_admin(role_name):
    role_name_lower = role_name.lower()
    if role_name_lower == 'admin' or role_name_lower == 'administrator':
        raise ValidationError(error={'name': f'Please choose a different role name than {role_name}'})


def filter_app_roles(roles):
    if len(roles) == 0:
        return []

    return RoleModel.query.filter(
        RoleModel.id.in_(roles),
        RoleModel.owner_id == app_id
    ).all()


def filter_app_permissions(permissions):
    return PermissionModel.query.filter(
        PermissionModel.id.in_(permissions),
        PermissionModel.entity_name == 'APP'
    ).all()


def ensure_role_name_is_unique(role_name, owner_id):
    existing_role_count_with_same_name = db.session.execute("""SELECT COUNT(r) FROM role r
      WHERE LOWER(r.name) = LOWER(:NAME) AND r.owner_id = :OWNER_ID""", {
        'NAME': role_name,
        'OWNER_ID': owner_id
    }).scalar()

    if existing_role_count_with_same_name != 0:
        raise ValidationError(
            error={"name": "A role with same name already exist."}
        )


def filter_org_permissions(permissions):
    return PermissionModel.query.filter(
        PermissionModel.id.in_(permissions),
        PermissionModel.entity_name.in_(('ORG', 'OPUNIT'))
    ).all()


def filter_org_roles(roles, org_id):
    if len(roles) == 0:
        return []

    return RoleModel.query.filter(
        RoleModel.id.in_(roles),
        RoleModel.owner_id == org_id
    ).all()


def get_can_grant_roles(role_ids=[]):
    if len(role_ids) == 0:
        return []

    resultset = db.session.execute("""SELECT role_grant_role.role_id as role_id,
     role.id as can_grant_role_id,role.name as can_grant_role_name
     FROM role_grant_role INNER JOIN role ON role_grant_role.can_grant_role = role.id
     WHERE role_grant_role.role_id IN :ROLE_IDS""", {
        'ROLE_IDS': tuple(role_ids)
    })

    can_grant_roles = {

    }

    for row in resultset:
        if can_grant_roles.get(row['role_id']) is None:
            can_grant_roles[row['role_id']] = []

        can_grant_roles.get(row['role_id']).append(MinimalDTO(
            id=row['can_grant_role_id'],
            name=row['can_grant_role_name']))

    return can_grant_roles


def map_role_model_to_dict(role, can_grant_roles):
    return {
        'id': role.id,
        'name': role.name,
        'description': role.description,
        'permissions': [
            {
                'id': permission.id,
                'name': permission.name,
                'description': permission.description,
                'resourceName': permission.resource_name
            } for permission in role.permissions
        ],
        'canGrantRoles': [{'id': role.id, 'name': role.name} for role in can_grant_roles]
    }


def ensure_role_exists(owner_id, role_id):
    role = RoleModel.query.filter_by(owner_id=owner_id, id=role_id).first()

    if role is None:
        raise ValidationError(message="No data found with provided app role id.")

    return role


def ensure_role_name_is_unique_for_edit(role_name, owner_id, role_id):
    existing_role_count_with_same_name = db.session.execute("""SELECT COUNT(r) FROM role r
      WHERE LOWER(r.name) = LOWER(:NAME) AND r.owner_id = :OWNER_ID
       AND r.id != :ROLE_ID""", {
        'NAME': role_name,
        'OWNER_ID': owner_id,
        'ROLE_ID': role_id
    }).scalar()

    if existing_role_count_with_same_name != 0:
        raise ValidationError(
            error={"name": "A role with same name already exist."}
        )
