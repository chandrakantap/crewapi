from flask import Blueprint, request, abort
from .create_role_service import create_app_role, create_org_role
from .get_roles_service import get_app_roles, get_org_roles
from .update_role_service import edit_app_role, edit_org_role
from .get_permission_service import get_org_permissions, get_app_permissions

rolebp = Blueprint('roles', __name__)


@rolebp.route('/app/roles', methods=('POST', 'GET'))
def app_roles():
    if request.method == 'POST':
        return create_app_role()
    elif request.method == 'GET':
        return get_app_roles()
    abort(404)


@rolebp.route('/app/roles/<uuid:role_id>', methods=('PUT',))
def app_role(role_id):
    if request.method == 'PUT':
        return edit_app_role(role_id)

    abort(405)


@rolebp.route('/org/roles', methods=('POST', 'GET'))
def org_roles():
    if request.method == 'POST':
        return create_org_role()
    elif request.method == 'GET':
        return get_org_roles()
    abort(404)


@rolebp.route('/org/roles/<uuid:role_id>', methods=('PUT',))
def org_role(role_id):
    if request.method == 'PUT':
        return edit_org_role(role_id)
    abort(404)


@rolebp.route('/app/permissions', methods=('GET',))
def app_permissions():
    if request.method == 'GET':
        return get_app_permissions()
    abort(404)


@rolebp.route('/org/permissions', methods=('GET',))
def org_permission():
    if request.method == 'GET':
        return get_org_permissions()
    abort(404)
