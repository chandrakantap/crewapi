import random
from werkzeug.security import generate_password_hash, check_password_hash
from flask import request
from crewapi.exceptions import UnauthorizeError
from crewapi.core.models import VisibleOpunitModel


def generate_password(N=6):
    return ''.join(random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
                   for _ in range(N))


def password_does_not_match(password_hash, raw_password):
    return not check_password_hash(password_hash, raw_password)


def hash_password(raw_password):
    return generate_password_hash(raw_password)


def ensure_visible_opunit(opunit_id):
    cru_session = request.cru_session
    if cru_session is None or cru_session.opunit is None:
        raise UnauthorizeError()

    visible_opunit = VisibleOpunitModel.query.filter(
        VisibleOpunitModel.opunit_id == cru_session.opunit.get('id'),
        VisibleOpunitModel.visible_opunit == opunit_id).first()

    if visible_opunit is None:
        raise UnauthorizeError()
