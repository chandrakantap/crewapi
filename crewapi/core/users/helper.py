import kanpai as Kanpai
from crewapi.core.models import RoleModel, UserModel, Constants
from crewapi.exceptions import ValidationError
from crewapi.ext import db, is_not_same_uuid, is_same_uuid
from flask import request

ROLE_SCHEMA = Kanpai.Object({
    'name': Kanpai.String().trim().required('Please provide user name'),
    'mobileNo': Kanpai.String().trim().match('^[6-9][0-9]{9}$', 'Invalid mobile no provided').required('Please provide ten digit mobile no'),
    'mobileNoCountryCode': Kanpai.String().trim().match('^\+[0-9][0-9]$', 'Invalid mobile no country code provided').required('Please provide mobile no country code'),
    'emailId': Kanpai.String().trim().match('[^@]+@[^@]+\.[^@]+', 'Invalid emailId provided'),
    'role': Kanpai.UUID().required('Please provide role of the user')
})


def ensure_role_exists(owner_id, role_id):

    if is_same_uuid(role_id, Constants.ROLE_ADMIN):
        role = RoleModel.query.filter_by(id = role_id).first()
    else:
        role = RoleModel.query.filter_by(owner_id = owner_id, id = role_id).first()

    if role is None:
        raise ValidationError(message = "Please provide a valid role.")

    return role


def ensure_can_grant_roles(owner_id, role):
    session_user_role = RoleModel.query.filter_by(owner_id = owner_id, id = request.cru_session.app_role.get('id')).first()
    grant_roles = db.session.execute("""SELECT COUNT(r) FROM role_grant_role r 
        WHERE role_id = :ROLE_ID AND can_grant_role = :CAN_GRANT_ROLE""", {
            'ROLE_ID': session_user_role.id,
            'CAN_GRANT_ROLE': role.id
        }).scalar()

    if not (session_user_role.is_admin() or (grant_roles != 0)):
        raise ValidationError(message= f"You are not authorized to add user with role {role.name}")

    return session_user_role


def ensure_role_is_admin(role, session_user_role):
    if (role.is_admin() and not session_user_role.is_admin()):
        raise ValidationError(message= "Only an Admin can add user in admin role.")


def validate_user_data(owner_id, user_data, role):
    FieldErrors.check(user_exist_with_mobile_no(user_data.get('mobileNo')))\
        .and_add_message("mobileNo", f"{user_data.get('mobileNo')} being used by another user.")\
        .then_check(user_exist_with_email_id(user_data.get('emailId')))\
        .and_add_message("emailId", f"{user_data.get('emailId')} being used by another user.")\
        .then_check((not role.is_admin()) and (is_not_same_uuid(role.owner_id, owner_id)))\
        .and_add_message("role", "Invalid role provided.")\
        .then_throw_error()



class FieldErrors(object):
    def __init__(self, current_check):
        self.current_check = current_check
        self.field_errors = {}

    @staticmethod    
    def check(current_check):
        return FieldErrors(current_check)

    def then_check(self, current_check):
        self.current_check = current_check
        return  self

    def and_add_message(self, field, message):
        if self.current_check:
            self.field_errors[field] = message

        return self

    def then_throw_error(self):
        if len(self.field_errors) > 0:
            raise ValidationError(error = self.field_errors)



def user_exist_with_mobile_no(mobile_no):
    return db.session.execute(""" SELECT COUNT(c) FROM cru_user c 
    WHERE mobile_no = :MOBILE_NO""", {
        'MOBILE_NO': mobile_no
    }).scalar() != 0



def user_exist_with_email_id(email_id):
    return db.session.execute(""" SELECT COUNT(c) FROM cru_user c 
    WHERE email_id = :EMAIL_ID""", {
        'EMAIL_ID': email_id
    }).scalar() != 0


def map_user_model_to_dict(user_model, role):
    return {
        'id': user_model.id,
        'mobileNo': user_model.mobile_no,
        'mobileNoCountryCode': user_model.mobile_no_country_code,
        'name': user_model.name,
        'address': user_model.address,
        'emailId': user_model.email_id,
        'role': {
            'id': role.id,
            'name': role.name
        }
    }
