import kanpai as Kanpai
from flask import request
from crewapi.ext import ValidateRequest, Transactional, API_SUCCESS
from crewapi.session_helper import SessionContext
from crewapi.core.models import UserModel
from crewapi.exceptions import ValidationError
from crewapi.core.helper import password_does_not_match, hash_password

CHANGE_PASSWORD_SCHEMA = Kanpai.Object({
    "currentPassword": Kanpai.String("Invalid current password").required("Please enter your current password"),
    "newPassword": Kanpai.String("Invalid new password").required("Please enter a new password"),
    "confirmPassword": Kanpai.String("Invalid  password").required("Please confirm new password")
}).required("Missing required request data").assert_equal_field(
    "newPassword", "confirmPassword", error="Password do not match.")


@Transactional
@SessionContext(required=True)
@ValidateRequest(schema=CHANGE_PASSWORD_SCHEMA)
def change_password():
    change_passwd_data = request.validated_json
    user = UserModel.query.get(request.cru_session.user_id)

    if user is None:
        raise ValidationError(message="Account not found")

    if password_does_not_match(user.password, change_passwd_data.get("currentPassword")):
        raise ValidationError(error={"currentPassword": "Incorrect password"})

    new_password_hash = hash_password(
        change_passwd_data.get("newPassword"))

    user.password = new_password_hash
    user.is_first_login = False

    return API_SUCCESS(msg="Password changed successfully")
