from flask import request
from crewapi.ext import Transactional, ValidateRequest, app_id, db, API_SUCCESS
from crewapi.session_helper import SessionContext, PermissionRequired
from .helper import ROLE_SCHEMA, ensure_role_exists, ensure_can_grant_roles, ensure_role_is_admin, validate_user_data, map_user_model_to_dict
from crewapi.core.models import UserModel
from uuid import uuid4
from sqlalchemy import func
from crewapi.core.helper import generate_password
from werkzeug.security import generate_password_hash

@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('app.user.create_user',))
@ValidateRequest(ROLE_SCHEMA)
def create_app_user():
    user_data = request.validated_json

    role = ensure_role_exists(app_id, user_data.get('role'))
    session_user_role = ensure_can_grant_roles(app_id, role)

    ensure_role_is_admin(role, session_user_role)

    validate_user_data(app_id, user_data, role)

    raw_password = generate_password()

    app_user = UserModel(
        id = uuid4(),
        mobile_no = user_data.get('mobileNo'),
        mobile_no_country_code = user_data.get('mobileNoCountryCode'),
        password = generate_password_hash(raw_password),
        name = user_data.get('name'),
        address = user_data.get('address'),
        email_id = user_data.get('emailId'),
        owner = app_id,
        app_role_id = user_data.get('role'),
        created_by = app_id,
        created_on = func.now(),
        last_modified_by = app_id,
        last_modified_on = func.now()
    )

    db.session.add(app_user)
    db.session.flush()

    return API_SUCCESS(payload= map_user_model_to_dict(app_user, role), msg= "Successfully created app user.")
