from flask import Blueprint, request, abort
from .create_user_service import create_app_user
from .change_password_service import change_password

appuserbp = Blueprint('appusers', __name__)

usersbp = Blueprint('users', __name__)


@usersbp.route('/password', methods=('POST', 'PUT'))
def password():
    if request.method == 'PUT':
        return change_password()

    abort(404)


@appuserbp.route('/app/users', methods= ('POST', 'GET'))
def app_users():
    if request.method == 'POST':
        return create_app_user()
    abort(404)
