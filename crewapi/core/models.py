from crewapi.ext import db, is_same_uuid
from sqlalchemy.dialects.postgresql import UUID as PGUUID
from uuid import UUID


class Constants:
    ROLE_ADMIN = UUID('0ba7eee1-a0dd-4067-8626-39cd1c7138fa')
    PERMISSION_ENTITY_APP = 'APP'
    PERMISSION_ENTITY_ORG = 'ORG'
    PERMISSION_ENTITY_OPUNIT = 'OPUNIT'


role_permission = db.Table('role_has_permission',
                           db.Column('role_id', PGUUID(as_uuid=True),
                                     db.ForeignKey('role.id'), primary_key=True),
                           db.Column('permission_id', PGUUID(as_uuid=True), db.ForeignKey(
                               'permission.id'), primary_key=True)
                           )

role_can_grant_role = db.Table('role_grant_role',
                               db.Column('role_id', PGUUID(as_uuid=True), db.ForeignKey(
                                   'role.id'), primary_key=True),
                               db.Column('can_grant_role', PGUUID(as_uuid=True),
                                         db.ForeignKey('role.id'), primary_key=True)
                               )


class PermissionModel(db.Model):
    __tablename__ = 'permission'

    id = db.Column(PGUUID(as_uuid=True), primary_key=True)
    name = db.Column(db.String(256), nullable=False, unique=True)
    description = db.Column(db.String(4096), nullable=True)
    entity_name = db.Column(db.String(6), nullable=False)
    resource_name = db.Column(db.String(128), nullable=False)
    operation_name = db.Column(db.String(256), nullable=False)


class RoleModel(db.Model):
    __tablename__ = 'role'
    __table_args__ = (
        db.UniqueConstraint('name', 'owner_id', name='UNQ_Role_name'),
    )

    id = db.Column(PGUUID(as_uuid=True), primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    description = db.Column(db.String(4096), nullable=True)
    owner_id = db.Column(PGUUID(as_uuid=True), nullable=False)
    created_by = db.Column(PGUUID(as_uuid=True))
    created_on = db.Column(db.DateTime(timezone=True))
    last_modified_by = db.Column(PGUUID(as_uuid=True))
    last_modified_on = db.Column(db.DateTime(timezone=True))
    permissions = db.relationship('PermissionModel', secondary=role_permission)
    can_grant_roles = db.relationship('RoleModel', secondary=role_can_grant_role,
                                      primaryjoin=id == role_can_grant_role.c.role_id,
                                      secondaryjoin=id == role_can_grant_role.c.can_grant_role)

    def is_admin(self):
        return is_same_uuid(self.id, Constants.ROLE_ADMIN)


class OpunitModel(db.Model):
    __tablename__ = 'opunit'

    id = db.Column(PGUUID(as_uuid=True), primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    description = db.Column(db.String(4096), nullable=True)
    email_id = db.Column(db.String(256))
    phone_no = db.Column(db.String(128))
    parent_opunit = db.Column(PGUUID(as_uuid=True))
    office_address = db.Column(db.String(4096), nullable=True)
    org_id = db.Column(PGUUID(as_uuid=True))
    created_by = db.Column(PGUUID(as_uuid=True), nullable=False)
    created_on = db.Column(db.DateTime(timezone=True), nullable=False)
    last_modified_by = db.Column(PGUUID(as_uuid=True), nullable=False)
    last_modified_on = db.Column(db.DateTime(timezone=True), nullable=False)


class UserModel(db.Model):
    __tablename__ = 'cru_user'

    id = db.Column(PGUUID(as_uuid=True), primary_key=True)
    mobile_no = db.Column(db.String(10), nullable=False)
    mobile_no_country_code = db.Column(db.String(3), nullable=False)
    password = db.Column(db.String(4096), nullable=False)
    name = db.Column(db.String(256), nullable=False)
    address = db.Column(db.String(4096), nullable=True)
    email_id = db.Column(db.String(256), nullable=True)
    owner = db.Column(PGUUID(as_uuid=True), nullable=False)
    app_role_id = db.Column(PGUUID(as_uuid=True),
                            db.ForeignKey('role.id'), nullable=True)
    is_first_login = db.Column(db.Boolean(), nullable=False, default=True)
    password_retry_left = db.Column(db.Integer(), nullable=False, default=4)
    last_selected_org_id = db.Column(PGUUID(as_uuid=True), nullable=True)
    last_selected_opunit_id = db.Column(PGUUID(as_uuid=True), nullable=True)
    created_by = db.Column(PGUUID(as_uuid=True), nullable=False)
    created_on = db.Column(db.DateTime(timezone=True), nullable=False)
    last_modified_by = db.Column(PGUUID(as_uuid=True), nullable=False)
    last_modified_on = db.Column(db.DateTime(timezone=True), nullable=False)
    app_role = db.relationship('RoleModel', uselist=False)


class OpunitUserModel(db.Model):
    __tablename__ = 'opunit_user'

    opunit_id = db.Column(PGUUID(as_uuid=True), nullable=False, primary_key=True)
    user_id = db.Column(PGUUID(as_uuid=True), nullable=False, primary_key=True)
    org_id = db.Column(PGUUID(as_uuid=True), nullable=False)
    role_id = db.Column(PGUUID(as_uuid=True), nullable=False)


class VisibleOpunitModel(db.Model):
    __tablename__ = 'visible_opunits'

    opunit_id = db.Column(PGUUID(as_uuid=True), nullable=False, primary_key=True)
    visible_opunit = db.Column(PGUUID(as_uuid=True), nullable=False, primary_key=True)
