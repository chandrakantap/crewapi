def core_init(app):
    from crewapi.core.opunits import opunitbp
    app.register_blueprint(opunitbp, url_prefix=app.config['APPLICATION_ROOT'])

    from crewapi.core.roles import rolebp
    app.register_blueprint(rolebp, url_prefix=app.config['APPLICATION_ROOT'])

    from crewapi.core.session import sessionbp
    app.register_blueprint(sessionbp, url_prefix=app.config['APPLICATION_ROOT'])

    from crewapi.core.users import usersbp, appuserbp
    app.register_blueprint(usersbp, url_prefix=app.config['APPLICATION_ROOT'])

    app.register_blueprint(appuserbp, url_prefix=app.config['APPLICATION_ROOT'])
