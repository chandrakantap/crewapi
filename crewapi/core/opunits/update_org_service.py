from crewapi.ext import ValidateRequest, db, API_SUCCESS, Transactional
from crewapi.session_helper import SessionContext, ensure_selected_org
from crewapi.exceptions import RequiredPermissionMissingError
from .helper import map_opunit_model_to_dict, ORG_SCHEMA
from crewapi.core.models import OpunitModel
from crewapi.exceptions import ValidationError
from sqlalchemy import func
from flask import request


@Transactional
@SessionContext(required=True)
def edit_org_details(org_id):
    _ensure_user_has_edit_org_details_permission(org_id)
    return _update_details(org_id)


@ValidateRequest(schema=ORG_SCHEMA)
def _update_details(org_id):

    org_data = request.validated_json
    cru_session = request.cru_session

    organisation = _ensure_org_exist(org_id)
    _ensure_unique_org_name(org_id, org_data.get('name'))

    organisation.name = org_data.get("name")
    organisation.description = org_data.get("description")
    organisation.office_address = org_data.get("officeAddress")
    organisation.phone_no = org_data.get("phoneNo")
    organisation.email_id = org_data.get("emailId")
    organisation.last_modified_by = cru_session.user_id
    organisation.last_modified_on = func.now()

    db.session.flush()

    return API_SUCCESS(payload=map_opunit_model_to_dict(organisation), msg="Successfully updated organisation.")


def _ensure_user_has_edit_org_details_permission(org_id):
    has_app_permission = request.cru_session.permissions.get('app', {}).get('organisation', {}).get('edit_org_details', False)
    if not has_app_permission:
        ensure_selected_org(org_id)
        has_org_permission = request.cru_session.permissions.get('org', {}).get('organisation', {}).get('edit_org_details', False)

        if not has_org_permission:
            raise RequiredPermissionMissingError()


def _ensure_org_exist(org_id):
    org = OpunitModel.query.filter(OpunitModel.id == org_id,
                                   OpunitModel.org_id.is_(None)).first()

    if org is None:
        raise ValidationError(message="No data found with provided organisation id.")

    return org


def _ensure_unique_org_name(org_id, org_name):
    ORG_COUNT_BY_NAME_SQL = """SELECT count(*) as opunit_count FROM opunit
                                     WHERE LOWER(opunit.name)=LOWER(:NAME)
                                     AND (opunit.org_id is NULL OR opunit.org_id = :ORG_ID)
                                     AND opunit.id != :ORG_ID"""
    existing_org = db.session.execute(ORG_COUNT_BY_NAME_SQL, {
        'NAME': org_name,
        'ORG_ID': org_id
    }).scalar()

    if existing_org != 0:
        raise ValidationError(
            error={"name": "An organisation with same name already exist."}
        )
