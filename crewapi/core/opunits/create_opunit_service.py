import kanpai as Kanpai
from crewapi.ext import ValidateRequest, db, API_SUCCESS, Transactional
from crewapi.session_helper import SessionContext, PermissionRequired, OrgContextRequired
from .helper import add_ancestors, update_visible_opunits, map_opunit_model_to_dict
from crewapi.core.models import OpunitModel
from crewapi.exceptions import ValidationError
from sqlalchemy import func
from flask import request
from uuid import uuid4

OPUNIT_SCHEMA = Kanpai.Object({
    "name": (Kanpai.String(error="Opunit name must be a string")
             .trim()
             .required(error="Please provide operational unit name")
             .max(256, "Operational name should not be more than 256 character.")),
    "description": (Kanpai.String(error="Description must be a string")
                    .trim()
                    .max(4096, "Description should not be more than 4096 character.")),
    "officeAddress": (Kanpai.String(error="Office address must be a string")
                      .trim()
                      .max(4096, "Office address should not be more than 4096 character.")),
    "phoneNo": (Kanpai.String(error="Phone no must be a string")
                .trim()
                .max(256, "Phone no should not be more than 256 character.")),
    "emailId": (Kanpai.Email(error="Please provide a valid email")
                .trim()
                .max(256, "Email Id should not be more than 4096 character.")),
    "parentOpunit": (Kanpai.UUID(error="Invalid parent opunit provided.")
                     .required(error="Please provide parent operational unit"))
}).required("Missing required data.")


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('opunit.opunit.create_opunit',))
@ValidateRequest(OPUNIT_SCHEMA)
@OrgContextRequired
def create_opunit():
    opunit_data = request.validated_json
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    _ensure_unique_opunit_name(opunit_data.get('name'), org_id)
    _ensure_parent_opunit_exist(opunit_data.get('parentOpunit'), org_id)

    opunit = OpunitModel(
        id=uuid4(),
        name=opunit_data.get("name"),
        description=opunit_data.get("description"),
        office_address=opunit_data.get("officeAddress"),
        phone_no=opunit_data.get("phoneNo"),
        email_id=opunit_data.get("emailId"),
        parent_opunit=opunit_data.get("parentOpunit"),
        org_id=org_id,
        created_by=cru_session.user_id,
        created_on=func.now(),
        last_modified_by=cru_session.user_id,
        last_modified_on=func.now(),
    )
    db.session.add(opunit)
    db.session.flush()

    add_ancestors(opunit)
    db.session.flush()

    update_visible_opunits(opunit)
    db.session.flush()

    return API_SUCCESS(msg="Successfully created operational unit.", payload=map_opunit_model_to_dict(opunit))


def _ensure_unique_opunit_name(opunit_name, org_id):
    OPUNIT_COUNT_BY_NAME_SQL = """SELECT count(*) as opunit_count FROM opunit
                              WHERE LOWER(opunit.name)=LOWER(:NAME)
                              AND (opunit.org_id=:ORG_ID OR opunit.id=:ORG_ID )"""

    existing_opunit = db.session.execute(OPUNIT_COUNT_BY_NAME_SQL, {
        'NAME': opunit_name,
        'ORG_ID': org_id
    }).scalar()

    if existing_opunit != 0:
        raise ValidationError(
            error={"name": "An operational unit with same name already exist."}
        )


def _ensure_parent_opunit_exist(parent_opunit_id, org_id):
    PARENT_OPUNIT_EXISTS_SQL = """SELECT count(*) as opunit_count FROM opunit
                              WHERE opunit.id = :PARENT_OPUNIT_ID
                              AND (opunit.org_id=:ORG_ID OR opunit.id=:ORG_ID )"""

    existing_opunit = db.session.execute(PARENT_OPUNIT_EXISTS_SQL, {
        'PARENT_OPUNIT_ID': parent_opunit_id,
        'ORG_ID': org_id
    }).scalar()

    if existing_opunit == 0:
        raise ValidationError(
            error={"parentOpunit": "Unable to find parent operational unit."}
        )
