from flask import request, current_app
from crewapi.ext import Transactional, API_SUCCESS, empty_result, paged_result
from crewapi.session_helper import SessionContext, PermissionRequired
from crewapi.core.models import OpunitModel
from .helper import map_opunit_model_to_dict


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('app.organisation.view_org',))
def get_organisations():
    item_per_page_default = current_app.config.get('ITEM_PER_PAGE')
    page = request.args.get('page', default=1, type=int)
    item_per_page = request.args.get('item_per_page', default=item_per_page_default, type=int)
    query_result = (OpunitModel.query
                    .filter(OpunitModel.org_id.is_(None))
                    .order_by(OpunitModel.last_modified_on.desc())
                    .paginate(page, item_per_page, error_out=False))

    if query_result.total == 0:
        return API_SUCCESS(payload=empty_result())
    else:
        organisations = query_result.items
        return API_SUCCESS(payload=paged_result(
            query_result,
            items=[map_opunit_model_to_dict(org) for org in organisations]))
