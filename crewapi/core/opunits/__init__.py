from flask import Blueprint, request, abort
from .create_org_service import create_organisation
from .create_opunit_service import create_opunit
from .update_opunit_service import edit_opunit
from .get_organisations_service import get_organisations
from .update_org_service import edit_org_details
from .get_opunit_service import get_opunits


opunitbp = Blueprint('opunits', __name__)


@opunitbp.route('/organisations', methods=('POST', 'GET'))
def organisations():
    if request.method == 'POST':
        return create_organisation()
    elif request.method == 'GET':
        return get_organisations()

    abort(404)


@opunitbp.route('/organisations/<uuid:org_id>', methods=('PUT',))
def organisation(org_id):
    if request.method == 'PUT':
        return edit_org_details(org_id)

    abort(405)


@opunitbp.route('/opunits', methods=('POST', 'GET'))
def opunits():
    if request.method == 'POST':
        return create_opunit()
    elif request.method == 'GET':
        return get_opunits()

    abort(404)


@opunitbp.route('/opunits/<uuid:opunit_id>', methods=('PUT',))
def opunit(opunit_id):
    if request.method == 'PUT':
        return edit_opunit(opunit_id)

    abort(404)
