import kanpai as Kanpai
from crewapi.ext import ValidateRequest, db, API_SUCCESS, Transactional
from crewapi.session_helper import SessionContext, PermissionRequired, OrgContextRequired
from .helper import map_opunit_model_to_dict
from crewapi.core.models import OpunitModel
from crewapi.exceptions import ValidationError
from sqlalchemy import func
from flask import request

OPUNIT_SCHEMA = Kanpai.Object({
    "name":             (Kanpai.String(error="Opunit name must be a string")
                         .trim()
                         .required(error="Please provide operational unit name")
                         .max(256, "Operational name should not be more than 256 character.")),
    "description":      (Kanpai.String(error="Description must be a string")
                         .trim()
                         .max(4096, "Description should not be more than 4096 character.")),
    "officeAddress":   (Kanpai.String(error="Office address must be a string")
                        .trim()
                        .max(4096, "Office address should not be more than 4096 character.")),
    "phoneNo":         (Kanpai.String(error="Phone no must be a string")
                        .trim()
                        .max(256, "Phone no should not be more than 256 character.")),
    "emailId":         (Kanpai.Email(error="Please provide a valid email")
                        .trim()
                        .max(256, "Email Id should not be more than 4096 character."))
}).required("Missing required data.")


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('opunit.opunit.edit_opunit_details',))
@ValidateRequest(schema=OPUNIT_SCHEMA)
@OrgContextRequired
def edit_opunit(opunit_id):
    opunit_data = request.validated_json
    cru_session = request.cru_session
    org_id = cru_session.organisation.get('id')

    opunit = _ensure_opunit_exist(org_id, opunit_id)
    _ensure_unique_opunit_name(opunit_data.get('name'), org_id, opunit_id)

    # TODO Check if opunit is visible for selected opunit

    opunit.name = opunit_data.get("name")
    opunit.description = opunit_data.get("description")
    opunit.office_address = opunit_data.get("officeAddress")
    opunit.phone_no = opunit_data.get("phoneNo")
    opunit.email_id = opunit_data.get("emailId")
    opunit.parent_opunit = opunit_data.get("parentOpunit")
    opunit.last_modified_by = cru_session.user_id
    opunit.last_modified_on = func.now()

    db.session.flush()

    return API_SUCCESS(msg="Successfully updated operational unit.", payload=map_opunit_model_to_dict(opunit))


def _ensure_opunit_exist(org_id, opunit_id):
    opunit = OpunitModel.query.filter_by(id=opunit_id, org_id=org_id).first()

    if opunit is None:
        raise ValidationError(message="No data found with provided organisation and operation unit.")

    return opunit


def _ensure_unique_opunit_name(opunit_name, org_id, opunit_id):
    OPUNIT_COUNT_BY_NAME_SQL_UPDATE = """SELECT count(*) as opunit_count FROM opunit
                                     WHERE LOWER(opunit.name)=LOWER(:NAME)
                                     AND (opunit.org_id=:ORG_ID OR opunit.id=:ORG_ID )
                                     AND opunit.id != :OPUNIT_ID"""
    existing_opunit = db.session.execute(OPUNIT_COUNT_BY_NAME_SQL_UPDATE, {
        'NAME': opunit_name,
        'ORG_ID': org_id,
        'OPUNIT_ID': opunit_id
    }).scalar()

    if existing_opunit != 0:
        raise ValidationError(
            error={"name": "An operational unit with same name already exist."}
        )
