import kanpai as Kanpai
from flask import request
from crewapi.session_helper import SessionContext, PermissionRequired
from crewapi.ext import ValidateRequest, db, app_id, API_SUCCESS, Transactional
from .helper import update_visible_opunits, map_opunit_model_to_dict, ORG_SCHEMA
from crewapi.core.models import OpunitModel, UserModel, OpunitUserModel, Constants
from sqlalchemy import func
from crewapi.exceptions import ValidationError
from uuid import uuid4
from crewapi.core.helper import generate_password
from werkzeug.security import generate_password_hash


REGISTER_ORG_REQUEST_SCHEMA = Kanpai.Object({
    "adminUserName": (Kanpai.String(error="Please enter a valid  name.")
                      .trim()
                      .required("Please provide your name.")
                      .max(512, "Name should not be more than 512 character.")),
    "adminUserMobileNoCountryCode": (Kanpai.String(error="Please enter valid country.")
                                     .trim()
                                     .required(error="Please enter your country")
                                     .max(5, error="Please enter valid country.")),
    "adminUserMobileNo": (Kanpai.String(error="Please enter valid mobile not")
                          .trim()
                          .required(error='Please enter mobile no')
                          .max(10, error="Please enter valid mobile no.")),
    "organisationName": (Kanpai.String(error="Please enter valid organisation name.")
                         .trim()
                         .required(error="Organisation name is required.")
                         .max(256, "Organisation name should not be more than 256 character."))
}).required(error="Please provide organisation data.")


@Transactional
@SessionContext(required=False)
def create_organisation():
    if request.cru_session is None:
        return _register_organisation()
    else:
        return _create_organisation()


@ValidateRequest(schema=REGISTER_ORG_REQUEST_SCHEMA)
def _register_organisation():
    org_data = request.validated_json
    _ensure_unique_organisation_name(org_data.get("organisationName"), "organisationName")
    _ensure_unique_user_mobile_no(
        org_data.get("adminUserMobileNo"),
        org_data.get("adminUserMobileNoCountryCode"),
    )

    organisation = OpunitModel(
        id=uuid4(),
        name=org_data.get("organisationName"),
        created_by=app_id,
        created_on=func.now(),
        last_modified_by=app_id,
        last_modified_on=func.now(),
    )
    db.session.add(organisation)
    db.session.flush()

    update_visible_opunits(organisation)

    user_password = generate_password()
    admin_user = UserModel(
        id=uuid4(),
        name=org_data.get("adminUserName"),
        mobile_no=org_data.get("adminUserMobileNo"),
        mobile_no_country_code=org_data.get("adminUserMobileNoCountryCode"),
        password=generate_password_hash(user_password),
        owner=organisation.id,
        last_selected_org_id=organisation.id,
        last_selected_opunit_id=organisation.id,
        created_by=app_id,
        created_on=func.now(),
        last_modified_by=app_id,
        last_modified_on=func.now(),
    )
    db.session.add(admin_user)
    db.session.flush()

    opunit_user = OpunitUserModel(
        user_id=admin_user.id,
        opunit_id=organisation.id,
        org_id=organisation.id,
        role_id=Constants.ROLE_ADMIN,
    )
    db.session.add(opunit_user)
    db.session.flush()

    return API_SUCCESS(payload=map_opunit_model_to_dict(organisation))


@PermissionRequired(any_of=('app.organisation.create_org',))
@ValidateRequest(schema=ORG_SCHEMA)
def _create_organisation():
    cru_session = request.cru_session
    org_data = request.validated_json

    _ensure_unique_organisation_name(org_data.get("name"), "name")
    organisation = OpunitModel(
        id=uuid4(),
        name=org_data.get("name"),
        email_id=org_data.get("emailId"),
        phone_no=org_data.get("phoneNo"),
        office_address=org_data.get("officeAddress"),
        created_by=cru_session.user_id,
        created_on=func.now(),
        last_modified_by=cru_session.user_id,
        last_modified_on=func.now(),
    )
    db.session.add(organisation)
    db.session.flush()

    update_visible_opunits(organisation)

    opunit_user = OpunitUserModel(
        user_id=cru_session.user_id,
        opunit_id=organisation.id,
        org_id=organisation.id,
        role_id=Constants.ROLE_ADMIN,
    )
    db.session.add(opunit_user)
    db.session.flush()

    return API_SUCCESS(payload=map_opunit_model_to_dict(organisation), msg="Successfully created organisation.")


def _ensure_unique_organisation_name(org_name, field_name):
    org = (
        OpunitModel.query.filter(func.lower(OpunitModel.name) == func.lower(org_name))
        .filter(OpunitModel.parent_opunit.is_(None))
        .first()
    )

    if org is not None:
        raise ValidationError(
            error={field_name: "An organisation with same name already exist."}
        )


def _ensure_unique_user_mobile_no(mobile_no, country_code):
    user = UserModel.query.filter_by(
        mobile_no=mobile_no, mobile_no_country_code=country_code
    ).first()

    if user is not None:
        raise ValidationError(
            error={"adminUserMobileNo": "An user with same mobile no exist."}
        )
