import kanpai as Kanpai
from crewapi.ext import db

ORG_SCHEMA = Kanpai.Object({
    "name": (Kanpai.String(error="Please enter valid organisation name.")
             .trim()
             .required(error="Organisation name is required.")
             .max(256, "Organisation name should not be more than 256 character.")),
    "officeAddress": Kanpai.String().trim(),
    "phoneNo": Kanpai.String().trim().max(128),
    "emailId": Kanpai.Email(error="Please provide a valid email Id")
}).required(error="Please provide organisation data.")


def add_ancestors(opunit):
    ADD_ANCESTORS_SQL = """ INSERT INTO opunit_ancestors
                        SELECT opunit_id,ancestor_id FROM
                        (
                            SELECT :OPUNIT_ID as opunit_id,ancestor_id
                            FROM opunit_ancestors WHERE opunit_id = :PARENT_OPUNIT_ID
                            UNION
                            SELECT :OPUNIT_ID as opunit_id,:PARENT_OPUNIT_ID as ancestor_id
                        ) ancestors"""
    db.session.execute(ADD_ANCESTORS_SQL, params={
                       "OPUNIT_ID": opunit.id,
                       "PARENT_OPUNIT_ID": opunit.parent_opunit}).close()


def update_visible_opunits(opunit):
    UPDATE_VISIBLE_OPUNITS = """INSERT INTO visible_opunits
                            SELECT opunit_id,visible_opunit_id FROM
                            (
                                SELECT ancestor_id as opunit_id,:OPUNIT_ID as visible_opunit_id
                                FROM opunit_ancestors
                                WHERE opunit_id = :OPUNIT_ID
                                UNION
                                SELECT :OPUNIT_ID as opunit_id,:OPUNIT_ID as visible_opunit_id
                            ) vopunits"""
    db.session.execute(UPDATE_VISIBLE_OPUNITS, params={"OPUNIT_ID": opunit.id}).close()


def map_opunit_model_to_dict(opunit_model):
    return {
        'id': opunit_model.id,
        'name': opunit_model.name,
        'description': opunit_model.description,
        'emailId': opunit_model.email_id,
        'phoneNo': opunit_model.phone_no,
        'parentOpunit': opunit_model.parent_opunit,
        'officeAddress': opunit_model.office_address,
        'createdBy': opunit_model.created_by,
        'createdOn': opunit_model.created_on.isoformat(),
        'lastModifiedBy': opunit_model.last_modified_by,
        'lastModifiedOn': opunit_model.last_modified_on.isoformat()
    }
