from flask import request, current_app
from crewapi.ext import Transactional, API_SUCCESS, empty_result, paged_result
from crewapi.session_helper import (SessionContext, PermissionRequired, OrgContextRequired)
from crewapi.core.models import OpunitModel
from crewapi.core.helper import ensure_visible_opunit
from .helper import map_opunit_model_to_dict
from uuid import UUID


@Transactional
@SessionContext(required=True)
@PermissionRequired(any_of=('opunit.opunit.view_opunit',))
@OrgContextRequired
def get_opunits():
    org_id = request.cru_session.organisation.get('id')

    item_per_page_default = current_app.config.get('ITEM_PER_PAGE')
    name_starts_with = request.args.get('nameStartsWith', type=str)
    page = request.args.get('page', default=1, type=int)
    item_per_page = request.args.get('itemPerPage', default=item_per_page_default, type=int)

    parent_opunit_id = request.args.get('parentOpunitId', default=org_id, type=UUID)
    ensure_visible_opunit(parent_opunit_id)

    filters = [OpunitModel.org_id == org_id, OpunitModel.parent_opunit == parent_opunit_id]
    if name_starts_with is not None:
        filters.append(OpunitModel.name.startswith(name_starts_with, autoescape=True))

    query_result = (OpunitModel.query
                    .filter(*filters)
                    .order_by(OpunitModel.last_modified_on.desc())
                    .paginate(page, item_per_page, error_out=False))

    if query_result.total == 0:
        return API_SUCCESS(payload=empty_result())
    else:
        opunits = query_result.items
        return API_SUCCESS(payload=paged_result(
            query_result,
            items=[map_opunit_model_to_dict(opunit) for opunit in opunits]))
