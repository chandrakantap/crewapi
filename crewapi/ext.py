from flask import jsonify, request, json
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from crewapi.exceptions import ValidationError
from uuid import UUID


class CruSQLAlchemy(SQLAlchemy):
    def apply_driver_hacks(self, app, info, options):
        options['json_serializer'] = json.dumps
        super().apply_driver_hacks(app, info, options)


db = CruSQLAlchemy()

app_id = '93ee912b-d051-46cc-83bb-988c6a44459f'

migrate = Migrate()


def is_same_uuid(val_one, val_two):
    try:
        if type(val_one) == str:
            val_one = UUID(val_one)

        if type(val_two) == str:
            val_two = UUID(val_two)

        return val_two == val_one

    except Exception:
        return False


def is_not_same_uuid(val_one, val_two):
    return not is_same_uuid(val_one, val_two)


class RESPONSE_CODE:
    SUCCESS = 2000
    BAD_REQUEST = 4000
    UNKNOWN_ERROR = 4010
    AUTHENTICATION_ERROR = 4020
    ORG_CONTEXT_MISSING = 4022
    AUTHENTICATION_REQUIRED = 4021
    UNAUTHORIZE = 4030
    RESOURCE_NOT_FOUND = 4040
    GATEWAY_TIMEOUT = 5040


def API_SUCCESS(msg="Success", payload=None):
    response = {
        'status': RESPONSE_CODE.SUCCESS,
        'message': msg
    }
    if payload is not None:
        response['payload'] = payload

    return jsonify(response)


def API_ERROR(error_code, msg, error=None, status_code=200):
    response = {
        'status': error_code,
        'message': msg
    }
    if error is not None:
        response['errors'] = error

    response_json = jsonify(response)
    response_json.status_code = status_code
    return response_json


def Transactional(func):
    def with_transaction(*args, **kwargs):
        funct_return = func(*args, **kwargs)
        db.session.commit()
        return funct_return

    return with_transaction


def ValidateRequest(schema):
    def decorator(func):
        def decorated_function(*args, **kwargs):
            validation_result = schema.validate(request.json)
            if validation_result.get('success', False) is False:
                raise ValidationError(validation_result.get('error'))

            request.validated_json = validation_result.get('data')
            return func(*args, **kwargs)
        return decorated_function
    return decorator


class MinimalDTO(object):
    def __init__(self, id, name):
        self.id = id
        self.name = name


def empty_result():
    return {
        'curPage': 0,
        'totalPages': 0,
        'totalCount': 0,
        'items': [],
        'curPageStart': 0,
        'curPageEnd': 0
    }


def paged_result(query_result, items):
    result = {
        'curPage': query_result.page,
        'totalPages': query_result.pages,
        'totalCount': query_result.total,
        'items': items
    }
    result['curPageStart'] = ((result.get('curPage') - 1) * query_result.per_page) + 1
    result['curPageEnd'] = result.get('curPage') * query_result.per_page

    if result['curPageEnd'] > result['totalCount']:
        result['curPageEnd'] = result['totalCount']

    return result


class STATUS_CONST:
    DRAFT = 'D'
    PUBLISHED = 'P'
    PULISHED_WITH_DRAFT = 'PD'
    ACTIVE = 'A'
    INACTIVE = 'IA'


def get_indexes_with_duplicate(source_array=[], get_value=lambda x: x):
    """This method return indexes which contains duplicate value.
    e.g. for source_array[2,3,5,6,3,8,2,9] and default get_value return [4,6]
    for source_array = [
        {'id':12,'name':'Jamie'},
        {'id':13,'name':'Tywin'},
        {'id':12,'name':'Joffery'},
        {'id':14,'name':'Jamie'},
        {'id':15,'name':'Jamie'},
        ]
        and get_value = lambda x: x.get('name')
    return will be [3,4]
    """
    duplicate_indexes = []
    value_index = {}

    for index, element in enumerate(source_array):
        value = get_value(element)
        if value_index.get(value, False):
            duplicate_indexes.append(index)
        else:
            value_index[value] = True

    return duplicate_indexes
