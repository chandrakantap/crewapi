def create_app(app_config={}):
    app_config_env_name = 'CREW_APP_CONFIG'

    from flask import Flask, jsonify
    app = Flask('crewapi')

    app.config['APPLICATION_ROOT'] = '/api'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://localhost:5432/crewapi'
    app.config['SQLALCHEMY_ECHO'] = False
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['AUTH_TOKEN_NAME'] = 'CREW_SESSION_TOKEN'
    app.config['JWT_SECRET_KEY'] = '40Xq9Ko3U2iJv8YBH2snkZ3PZw53zyJ4ukAOkfyRYftDj7QxhaD0OcTCx\
    1GMD0qFGwKYJ2sYyxzqGLHkyBASb9IFjkKSDxiTssgsHFiucuql4y2pRgoXh24fk0TLmwTrV'
    app.config['JWT_ALGO'] = 'HS256'
    app.config['AUTH_TOKEN_NAME'] = 'CREW_SESSION_TOKEN'
    app.config['ITEM_PER_PAGE'] = 30

    for key, value in app_config.items():
        app.config[key] = value

    import os
    if os.getenv(app_config_env_name):
        app.config.from_envvar(app_config_env_name)

    from crewapi.ext import db, migrate
    db.init_app(app)
    migrate.init_app(app, db)

    from crewapi.errorhandler import register_error_handlers
    register_error_handlers(app)

    from crewapi.core import core_init
    core_init(app)

    from crewapi.catalog import catalog_init
    catalog_init(app)

    @app.route('/ping')
    def hello():
        return jsonify({
            'status': 'success',
            'alive': True
        })

    return app
