from flask import current_app
from crewapi.exceptions import (ValidationError, SessionRequiredError,
                                RequiredPermissionMissingError, UnauthorizeError,
                                OrgContextMissingError)
from werkzeug.exceptions import (MethodNotAllowed, NotFound)
from jwt.exceptions import InvalidSignatureError, DecodeError
from crewapi.ext import RESPONSE_CODE, API_ERROR


def handle_generic_error(e):
    if current_app.config.get('DEBUG', False) is True:
        raise e

    return API_ERROR(RESPONSE_CODE.UNKNOWN_ERROR, msg='Some unknown error occured.')


def handle_session_not_found_error(e):
    return API_ERROR(RESPONSE_CODE.AUTHENTICATION_REQUIRED,
                     msg="You need to login before performing this operation")


def handle_unauthorize_error(e):
    return API_ERROR(
        RESPONSE_CODE.UNAUTHORIZE, 'You are not authorized to perform this action.')


def register_error_handlers(app):
    app.register_error_handler(ValidationError, lambda e: API_ERROR(
        RESPONSE_CODE.BAD_REQUEST, e.message, e.error))

    app.register_error_handler(
        SessionRequiredError, handle_session_not_found_error)

    app.register_error_handler(
        InvalidSignatureError, handle_session_not_found_error)

    app.register_error_handler(
        DecodeError, handle_session_not_found_error)

    app.register_error_handler(
        RequiredPermissionMissingError, handle_unauthorize_error)

    app.register_error_handler(
        UnauthorizeError, handle_unauthorize_error)

    app.register_error_handler(OrgContextMissingError, lambda e: API_ERROR(
        RESPONSE_CODE.ORG_CONTEXT_MISSING, 'Organisation context is missing'))
    app.register_error_handler(NotFound, lambda e: API_ERROR(404, 'Not found', status_code=404))
    app.register_error_handler(MethodNotAllowed, lambda e: API_ERROR(405, 'Method not allowed', status_code=405))
    app.register_error_handler(MethodNotAllowed, lambda e: API_ERROR(405, 'Method not allowed', status_code=405))

    app.register_error_handler(Exception, handle_generic_error)
