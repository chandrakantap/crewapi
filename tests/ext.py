from functools import wraps

AUDIT_PAYLOAD_AUTO_GEN_ATTR = {
    'id': 'AUTO_GEN',
    'createdOn': 'NOW()',
    'lastModifiedOn': 'NOW()'
}

ADMIN_ROLE_ID = '0ba7eee1-a0dd-4067-8626-39cd1c7138fa'


def db_teardown(db_connection):
    TEAR_DOWN_QUERIES = (
        "DELETE FROM product_type",
        "DELETE FROM category_draft",
        "DELETE FROM category",
        "DELETE FROM catalog",
        "DELETE FROM opunit_user",
        "DELETE FROM opunit_ancestors",
        "DELETE FROM visible_opunits",
        "DELETE FROM opunit",
        "DELETE FROM cru_user",
        """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                'b516e3aa-a3af-11e7-bbd2-63ad024569ce',
                '2345623456',
                '+91',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'App admin',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
        """,
        "DELETE FROM role_has_permission",
        "DELETE FROM role_grant_role",
        "DELETE FROM role",
        """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
            'ADMIN',
            'Administrator role. User having this role can perform all the task.',
            'cd350095-dfde-4364-be14-9c2af94b2a93',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )
        """,
        "DELETE FROM session"
    )
    with db_connection as conn:
        with conn.cursor() as cursor:
            for query in TEAR_DOWN_QUERIES:
                cursor.execute(query)


def DatabaseSetup(setup_queries=[]):
    def decorator(test_func):
        @wraps(test_func)
        def decorated_function(*args, **kwargs):
            db_connection = kwargs.get('db_connection')
            if db_connection is None:
                raise RuntimeError('db_connection fixture is missing.')

            with db_connection as conn:
                with conn.cursor() as cursor:
                    for query in setup_queries:
                        cursor.execute(query)

            try:
                test_func(*args, **kwargs)
            finally:
                db_teardown(db_connection)

        return decorated_function
    return decorator
