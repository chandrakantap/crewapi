# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_org_context_required 1'] = {
    'message': 'Organisation context is missing',
    'status': 4022
}

snapshots['test_user_must_have_manage_catalog_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'name': 'Please provide catalog name'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_catalog_name_must_be_unique 1'] = {
    'errors': {
        'name': 'A catalog with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_other_org_can_have_same_catalog_name 1'] = {
    'message': 'Successfully created catalog.',
    'payload': {
        'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'createdOn': 'NOW()',
        'description': 'Avionos catalog',
        'id': 'AUTO_GEN',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'NOW()',
        'name': 'AvINOS',
        'status': 'D'
    },
    'status': 2000
}

snapshots['test_catalog_create_by_admin 1'] = {
    'message': 'Successfully created catalog.',
    'payload': {
        'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'createdOn': 'NOW()',
        'description': 'Avionos catalog',
        'id': 'AUTO_GEN',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'NOW()',
        'name': 'AvINOS',
        'status': 'D'
    },
    'status': 2000
}

snapshots['test_catalog_create_by_user_with_manage_role_permission 1'] = {
    'message': 'Successfully created catalog.',
    'payload': {
        'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'createdOn': 'NOW()',
        'description': 'Avionos catalog',
        'id': 'AUTO_GEN',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'NOW()',
        'name': 'AvINOS',
        'status': 'D'
    },
    'status': 2000
}
