from tests.ext import DatabaseSetup, AUDIT_PAYLOAD_AUTO_GEN_ATTR
import datasetup_category as datasetup
import tests.common_datasetup as common_datasetup
import tests.catalog.datasetup_catalog as catalog_datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', 'e00a7b33-76ca-4e38-9c2a-562a1183004e'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.simple_user_without_any_access)
def test_org_context_required(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', 'e00a7b33-76ca-4e38-9c2a-562a1183004e'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc_user_without_any_access)
def test_user_must_have_manage_category_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', 'e00a7b33-76ca-4e38-9c2a-562a1183004e'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', 'e00a7b33-76ca-4e38-9c2a-562a1183004e'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_error_when_invalid_catalog(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('category').format('87496fe8-b3b0-4aab-b6bf-5beded7926e7', 'e00a7b33-76ca-4e38-9c2a-562a1183004e'),
        json={
            'name': 'cpvc Pipes AND Fitting',
            'description': 'CPVC Pipes and Fitting'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published)
def test_error_when_invalid_category(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', '52cd075a-0677-4a04-b532-cf58f4976378'),
        json={
            'name': 'cpvc Pipes AND Fitting',
            'description': 'CPVC Pipes and Fitting'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avions_catalog_root_categories)
def test_error_when_invalid_draft_category(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', '52cd075a-0677-4a04-b532-cf58f4976378'),
        json={
            'name': 'DIRECT ACTION PUMP MACHINE',
            'description': 'DIRECT Action Pump Machine'
        })
    snapshot.assert_match(response.json)



@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avions_catalog_root_categories)
def test_root_category_name_must_be_unique(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', '52cd075a-0677-4a04-b532-cf58f4976378'),
        json={
            'name': 'cpvc Pipes AND Fitting',
            'description': 'CPVC Pipes and Fitting'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avions_catalog_root_categories +
               datasetup.avions_catalog_draft_root_categories)
def test_root_category_name_should_not_be_same_as_any_draft(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', 'e00a7b33-76ca-4e38-9c2a-562a1183004e'),
        json={
            'name': 'Direct action HAND pump',
            'description': 'Direct action hand pump category.'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avions_catalog_categories)
def test_category_name_must_be_unique(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', 'be287385-f53e-4967-a948-e4cbe98cedf9'),
        json={
            'name': 'CPVC PIPE',
            'description': 'CPVC Pipes and Fitting',
            'parentCategoryId': 'e00a7b33-76ca-4e38-9c2a-562a1183004e'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avions_catalog_draft_categories)
def test_category_name_should_not_be_same_as_any_draft(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', 'be287385-f53e-4967-a948-e4cbe98cedf9'),
        json={
            'name': 'CPVC PIPES',
            'description': 'CPVC Pipes and Fitting',
            'parentCategoryId': 'e00a7b33-76ca-4e38-9c2a-562a1183004e'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avinos_catalog_root_category_status_draft)
def test_root_category_update_for_draft_status(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', 'e00a7b33-76ca-4e38-9c2a-562a1183007e'),
        json={
            'name': 'ERT PIPES FITTING',
            'description': 'ERT Pipes and Fitting'
        }).json
    response_json['payload']['catalog'] = {**response_json.get('payload').get('catalog'), **{'createdOn': 'NOW()',
                                                                                             'lastModifiedOn': 'NOW()'}}
    response_json['payload']['category'] = {**response_json.get('payload').get('category'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)



@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avinos_catalog_root_category_status_draft)
def test_root_category_update_for_published_status(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', 'e00a7b33-76ca-4e38-9c2a-562a1183004e'),
        json={
            'name': 'CPVC Pipes',
            'description': 'CPVC Pipes'
        }).json
    response_json['payload']['catalog'] = {**response_json.get('payload').get('catalog'), **{'createdOn': 'NOW()',
                                                                                             'lastModifiedOn': 'NOW()'}}
    response_json['payload']['category'] = {**response_json.get('payload').get('category'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avinos_catalog_root_category_status_draft)
def test_root_category_update_for_published_with_draft_status(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', '52cd075a-0677-4a04-b532-cf58f4976378'),
        json={
            'name': 'DIRECT ACTION PUMP MACHINE',
            'description': 'DIRECT Action Pump Machine'
        }).json
    response_json['payload']['catalog'] = {**response_json.get('payload').get('catalog'), **{'createdOn': 'NOW()',
                                                                                             'lastModifiedOn': 'NOW()'}}
    response_json['payload']['category'] = {**response_json.get('payload').get('category'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)



@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avinos_catalog_category_status_draft)
def test_category_update_for_draft_status(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', '94880fdb-d2fc-4b74-9674-a378ba6779ea'),
        json={
            'name': 'ERT PIPES FITTINGS',
            'description': 'ERT Pipes Fittings',
            'parentCategoryId': 'e00a7b33-76ca-4e38-9c2a-562a1183004e'
        }).json
    response_json['payload']['catalog'] = {**response_json.get('payload').get('catalog'), **{'createdOn': 'NOW()',
                                                                                             'lastModifiedOn': 'NOW()'}}
    response_json['payload']['category'] = {**response_json.get('payload').get('category'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avinos_catalog_category_status_draft)
def test_category_update_for_published_status(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', '94880fdb-d2fc-4b74-9674-a378ba6779ef'),
        json={
            'name': 'CPVC PIPES FITTINGS',
            'description': 'CPVC Pipes Fittings',
            'parentCategoryId': 'e00a7b33-76ca-4e38-9c2a-562a1183004e'
        }).json
    response_json['payload']['catalog'] = {**response_json.get('payload').get('catalog'), **{'createdOn': 'NOW()',
                                                                                             'lastModifiedOn': 'NOW()'}}
    response_json['payload']['category'] = {**response_json.get('payload').get('category'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)



@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_avinos_catalog_published +
               datasetup.avinos_catalog_category_status_draft)
def test_category_update_for_published_with_draft_status(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('category').format('b7bf521f-0131-4cb0-a705-e949b7e824ff', 'be287385-f53e-4967-a948-e4cbe98cedf9'),
        json={
            'name': 'CPVC FITTINGS MACHINE',
            'description': 'CPVC Fittings Machine',
            'parentCategoryId': 'e00a7b33-76ca-4e38-9c2a-562a1183004e'
        }).json
    response_json['payload']['catalog'] = {**response_json.get('payload').get('catalog'), **{'createdOn': 'NOW()',
                                                                                             'lastModifiedOn': 'NOW()'}}
    response_json['payload']['category'] = {**response_json.get('payload').get('category'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)



@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.ortega_inc_regoup_draft_catalog +
               datasetup.avinos_root_category_for_draft_catalog)
def test_category_update_for_draft_catalog(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('category').format('2e670ce7-502d-4aab-95cf-3c1745f327a0', 'e00a7b33-76ca-4e38-9c2a-562a1183007f'),
        json={
            'name': 'ERT PIPES FITTING',
            'description': 'ERT Pipes Fitting'
        }).json
    response_json['payload']['catalog'] = {**response_json.get('payload').get('catalog'), **{'createdOn': 'NOW()',
                                                                                             'lastModifiedOn': 'NOW()'}}
    response_json['payload']['category'] = {**response_json.get('payload').get('category'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)



@DatabaseSetup(catalog_datasetup.simple_user_with_manage_catalog_access +
               datasetup.ortega_inc_regoup_draft_catalog +
               datasetup.avinos_root_category_for_draft_catalog)
def test_category_update_by_user_with_manage_role_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('category').format('2e670ce7-502d-4aab-95cf-3c1745f327a0', 'e00a7b33-76ca-4e38-9c2a-562a1183007f'),
        json={
            'name': 'ERT PIPES FITTING',
            'description': 'ERT Pipes Fitting'
        }).json
    response_json['payload']['catalog'] = {**response_json.get('payload').get('catalog'), **{'createdOn': 'NOW()',
                                                                                             'lastModifiedOn': 'NOW()'}}
    response_json['payload']['category'] = {**response_json.get('payload').get('category'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)



@DatabaseSetup(catalog_datasetup.simple_user_with_review_catalog_access +
               datasetup.ortega_inc_regoup_draft_catalog +
               datasetup.avinos_root_category_for_draft_catalog)
def test_category_update_by_user_with_review_role_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('category').format('2e670ce7-502d-4aab-95cf-3c1745f327a0', 'e00a7b33-76ca-4e38-9c2a-562a1183007f'),
        json={
            'name': 'ERT PIPES FITTING',
            'description': 'ERT Pipes Fitting'
        }).json
    response_json['payload']['catalog'] = {**response_json.get('payload').get('catalog'), **{'createdOn': 'NOW()',
                                                                                             'lastModifiedOn': 'NOW()'}}
    response_json['payload']['category'] = {**response_json.get('payload').get('category'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)
