ortega_inc_avinos_catalog_published = ("""INSERT INTO catalog(id,name,org_id,status,
    created_by,created_on,last_modified_by,last_modified_on)
    VALUES('b7bf521f-0131-4cb0-a705-e949b7e824ff','Avinos','165fa6ab-1893-4108-b88c-a30dfbdc88de','P',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
""",)

ortega_inc_regoup_draft_catalog = ("""INSERT INTO catalog(id,name,org_id,status,
    created_by,created_on,last_modified_by,last_modified_on)
    VALUES('2e670ce7-502d-4aab-95cf-3c1745f327a0','Regoup','165fa6ab-1893-4108-b88c-a30dfbdc88de','D',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
""",)

avions_catalog_root_categories = ("""
    INSERT INTO category(id,name,catalog_id,status,
    created_by,created_on,last_modified_by,last_modified_on)
    VALUES
    ('e00a7b33-76ca-4e38-9c2a-562a1183004e','CPVC Pipes and Fitting','b7bf521f-0131-4cb0-a705-e949b7e824ff','P',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30'),
    ('52cd075a-0677-4a04-b532-cf58f4976378','DIRECT ACTION PUMP','b7bf521f-0131-4cb0-a705-e949b7e824ff','PD',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
""",)

avions_catalog_draft_root_categories = ("""
    INSERT INTO category_draft(id,name,catalog_id,
    created_by,created_on,last_modified_by,last_modified_on)
    VALUES
    ('52cd075a-0677-4a04-b532-cf58f4976378','Direct action hand pump','b7bf521f-0131-4cb0-a705-e949b7e824ff',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
""",)

avions_catalog_categories = (
    avions_catalog_root_categories +
    (
        """
        INSERT INTO category(id,name,parent_category_id,catalog_id,status,
        created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        ('94880fdb-d2fc-4b74-9674-a378ba6779ef','CPVC PIPE','e00a7b33-76ca-4e38-9c2a-562a1183004e','b7bf521f-0131-4cb0-a705-e949b7e824ff','P',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30'),
        ('be287385-f53e-4967-a948-e4cbe98cedf9','CPVC FITTINGS','e00a7b33-76ca-4e38-9c2a-562a1183004e',
        'b7bf521f-0131-4cb0-a705-e949b7e824ff','PD',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
        """,
    )
)

avions_catalog_draft_categories = (
    avions_catalog_categories +
    (
        """
        INSERT INTO category_draft(id,name,parent_category_id,catalog_id,
        created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        ('94880fdb-d2fc-4b74-9674-a378ba6779ef','CPVC PIPES','e00a7b33-76ca-4e38-9c2a-562a1183004e','b7bf521f-0131-4cb0-a705-e949b7e824ff',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
        """,
    )
)


avinos_catalog_root_category_status_draft = (
    avions_catalog_root_categories +
    avions_catalog_draft_root_categories +
    (
        """
        INSERT INTO category(id,name,catalog_id,status,
        created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        ('e00a7b33-76ca-4e38-9c2a-562a1183007e','ERT PIPES','b7bf521f-0131-4cb0-a705-e949b7e824ff','D',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
        """,
    )
)


avinos_catalog_category_status_draft = (
    avions_catalog_categories +
    (
        """
        INSERT INTO category(id,name,parent_category_id,catalog_id,status,
        created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        ('94880fdb-d2fc-4b74-9674-a378ba6779ea','ERT PIPES','e00a7b33-76ca-4e38-9c2a-562a1183004e','b7bf521f-0131-4cb0-a705-e949b7e824ff','D',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
        """,
        """
        INSERT INTO category_draft(id,name,parent_category_id,catalog_id,
        created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        ('be287385-f53e-4967-a948-e4cbe98cedf9','CPVC PIPES','e00a7b33-76ca-4e38-9c2a-562a1183004e','b7bf521f-0131-4cb0-a705-e949b7e824ff',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
        """
    )
)

avinos_root_category_for_draft_catalog = (
    """
    INSERT INTO category(id,name,catalog_id,status,
    created_by,created_on,last_modified_by,last_modified_on)
    VALUES
    ('e00a7b33-76ca-4e38-9c2a-562a1183007f','ERT PIPES','2e670ce7-502d-4aab-95cf-3c1745f327a0','D',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
    """,
)
