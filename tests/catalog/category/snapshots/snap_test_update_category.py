# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_org_context_required 1'] = {
    'message': 'Organisation context is missing',
    'status': 4022
}

snapshots['test_user_must_have_manage_category_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'name': 'Please enter category name.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_error_when_invalid_catalog 1'] = {
    'message': 'No data found with provided organisation and catalog.',
    'status': 4000
}

snapshots['test_root_category_name_must_be_unique 1'] = {
    'errors': {
        'name': 'A category with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_root_category_name_should_not_be_same_as_any_draft 1'] = {
    'errors': {
        'name': 'A category with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_category_name_must_be_unique 1'] = {
    'errors': {
        'name': 'A category with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_category_name_should_not_be_same_as_any_draft 1'] = {
    'errors': {
        'name': 'A category with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_root_category_update_for_draft_status 1'] = {
    'message': 'Successfully updated category.',
    'payload': {
        'catalog': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': None,
            'id': 'b7bf521f-0131-4cb0-a705-e949b7e824ff',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'Avinos',
            'status': 'PD'
        },
        'category': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': 'ERT Pipes and Fitting',
            'id': 'AUTO_GEN',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'ERT PIPES FITTING',
            'parentCategoryId': None,
            'status': 'D'
        }
    },
    'status': 2000
}

snapshots['test_root_category_update_for_published_status 1'] = {
    'message': 'Successfully updated category.',
    'payload': {
        'catalog': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': None,
            'id': 'b7bf521f-0131-4cb0-a705-e949b7e824ff',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'Avinos',
            'status': 'PD'
        },
        'category': {
            'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'createdOn': 'NOW()',
            'description': 'CPVC Pipes',
            'id': 'AUTO_GEN',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'CPVC Pipes',
            'parentCategoryId': None,
            'status': 'PD'
        }
    },
    'status': 2000
}

snapshots['test_root_category_update_for_published_with_draft_status 1'] = {
    'message': 'Successfully updated category.',
    'payload': {
        'catalog': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': None,
            'id': 'b7bf521f-0131-4cb0-a705-e949b7e824ff',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'Avinos',
            'status': 'PD'
        },
        'category': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': 'DIRECT Action Pump Machine',
            'id': 'AUTO_GEN',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'DIRECT ACTION PUMP MACHINE',
            'parentCategoryId': None,
            'status': 'PD'
        }
    },
    'status': 2000
}

snapshots['test_category_update_for_draft_status 1'] = {
    'message': 'Successfully updated category.',
    'payload': {
        'catalog': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': None,
            'id': 'b7bf521f-0131-4cb0-a705-e949b7e824ff',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'Avinos',
            'status': 'PD'
        },
        'category': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': 'ERT Pipes Fittings',
            'id': 'AUTO_GEN',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'ERT PIPES FITTINGS',
            'parentCategoryId': 'e00a7b33-76ca-4e38-9c2a-562a1183004e',
            'status': 'D'
        }
    },
    'status': 2000
}

snapshots['test_category_update_for_published_status 1'] = {
    'message': 'Successfully updated category.',
    'payload': {
        'catalog': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': None,
            'id': 'b7bf521f-0131-4cb0-a705-e949b7e824ff',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'Avinos',
            'status': 'PD'
        },
        'category': {
            'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'createdOn': 'NOW()',
            'description': 'CPVC Pipes Fittings',
            'id': 'AUTO_GEN',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'CPVC PIPES FITTINGS',
            'parentCategoryId': 'e00a7b33-76ca-4e38-9c2a-562a1183004e',
            'status': 'PD'
        }
    },
    'status': 2000
}

snapshots['test_category_update_for_published_with_draft_status 1'] = {
    'message': 'Successfully updated category.',
    'payload': {
        'catalog': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': None,
            'id': 'b7bf521f-0131-4cb0-a705-e949b7e824ff',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'Avinos',
            'status': 'PD'
        },
        'category': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': 'CPVC Fittings Machine',
            'id': 'AUTO_GEN',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'CPVC FITTINGS MACHINE',
            'parentCategoryId': 'e00a7b33-76ca-4e38-9c2a-562a1183004e',
            'status': 'PD'
        }
    },
    'status': 2000
}

snapshots['test_error_when_invalid_category 1'] = {
    'message': 'No data found with provided catalog and category.',
    'status': 4000
}

snapshots['test_error_when_invalid_draft_category 1'] = {
    'message': 'No data found with provided catalog and draft category.',
    'status': 4000
}

snapshots['test_category_update_for_draft_catalog 1'] = {
    'message': 'Successfully updated category.',
    'payload': {
        'catalog': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': None,
            'id': '2e670ce7-502d-4aab-95cf-3c1745f327a0',
            'lastModifiedBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'lastModifiedOn': 'NOW()',
            'name': 'Regoup',
            'status': 'D'
        },
        'category': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': 'ERT Pipes Fitting',
            'id': 'AUTO_GEN',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'ERT PIPES FITTING',
            'parentCategoryId': None,
            'status': 'D'
        }
    },
    'status': 2000
}

snapshots['test_category_update_by_user_with_manage_role_permission 1'] = {
    'message': 'Successfully updated category.',
    'payload': {
        'catalog': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': None,
            'id': '2e670ce7-502d-4aab-95cf-3c1745f327a0',
            'lastModifiedBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'lastModifiedOn': 'NOW()',
            'name': 'Regoup',
            'status': 'D'
        },
        'category': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': 'ERT Pipes Fitting',
            'id': 'AUTO_GEN',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'ERT PIPES FITTING',
            'parentCategoryId': None,
            'status': 'D'
        }
    },
    'status': 2000
}

snapshots['test_category_update_by_user_with_review_role_permission 1'] = {
    'message': 'Successfully updated category.',
    'payload': {
        'catalog': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': None,
            'id': '2e670ce7-502d-4aab-95cf-3c1745f327a0',
            'lastModifiedBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'lastModifiedOn': 'NOW()',
            'name': 'Regoup',
            'status': 'D'
        },
        'category': {
            'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
            'createdOn': 'NOW()',
            'description': 'ERT Pipes Fitting',
            'id': 'AUTO_GEN',
            'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
            'lastModifiedOn': 'NOW()',
            'name': 'ERT PIPES FITTING',
            'parentCategoryId': None,
            'status': 'D'
        }
    },
    'status': 2000
}
