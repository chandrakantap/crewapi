import tests.common_datasetup as common_datasetup

avinos_catalog = ("""
    INSERT INTO catalog(id,name,org_id,status,
    created_by,created_on,last_modified_by,last_modified_on)
    VALUES('b7bf521f-0131-4cb0-a705-e949b7e824ff','Avinos','165fa6ab-1893-4108-b88c-a30dfbdc88de','P',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
""",)


org_seinthon_inc_with_avinos_catalog = (
    """INSERT INTO opunit(id,name,created_by,created_on,
        last_modified_by,last_modified_on)
    VALUES
    (
        '352dd997-734e-4865-b4ff-f27e5d29006a',
        'Seinthon Inc',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         '2019-02-22T17:31:33+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         '2019-02-22T17:31:33+05:30'
    )""",

    """INSERT INTO catalog(id,name,org_id,status,
    created_by,created_on,last_modified_by,last_modified_on)
    VALUES('b7bf521f-0131-4cb0-a705-e949b7e824fe','Avinos','352dd997-734e-4865-b4ff-f27e5d29006a','P',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')"""
)

simple_user_with_manage_catalog_access = (
    common_datasetup.simple_user_without_any_access +
    common_datasetup.org_ortega_inc +
    common_datasetup.org_ortega_inc_zonal_manager_role +
    (
        """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','69b667bd-055c-462c-8326-ab459005c2d1')
        """,
        """INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','1c0dd6d5-6a11-4f91-82ef-e596730663ae')
        """
    )
)

berter_catalog = ("""
    INSERT INTO catalog(id,name,org_id,status,
    created_by,created_on,last_modified_by,last_modified_on)
    VALUES('b7bf521f-0131-4cb0-a705-e949b7e824fe','Berter','165fa6ab-1893-4108-b88c-a30dfbdc88de','P',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')
""",)


simple_user_with_review_catalog_access = (
    common_datasetup.simple_user_without_any_access +
    common_datasetup.org_ortega_inc +
    common_datasetup.org_ortega_inc_zonal_manager_role +
    (
        """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','b33c0447-c3af-4578-bfaf-f087bce9b15a')
        """,
        """INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','1c0dd6d5-6a11-4f91-82ef-e596730663ae')
        """
    )
)
