from tests.ext import DatabaseSetup, AUDIT_PAYLOAD_AUTO_GEN_ATTR
import datasetup_catalog as datasetup
import tests.common_datasetup as common_datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.post(
        resource_uri.get('catalogs'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.simple_user_without_any_access)
def test_org_context_required(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.post(
        resource_uri.get('catalogs'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc_user_without_any_access)
def test_user_must_have_manage_catalog_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.post(
        resource_uri.get('catalogs'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.post(
        resource_uri.get('catalogs'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.avinos_catalog
               )
def test_catalog_name_must_be_unique(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.post(
        resource_uri.get('catalogs'),
        json={
            'name': '  AvINOS  ',
            'description': 'Avionos catalog'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(datasetup.org_seinthon_inc_with_avinos_catalog +
               common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user)
def test_other_org_can_have_same_catalog_name(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.post(
        resource_uri.get('catalogs'),
        json={
            'name': '  AvINOS  ',
            'description': 'Avionos catalog'
        }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user)
def test_catalog_create_by_admin(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.post(
        resource_uri.get('catalogs'),
        json={
            'name': '  AvINOS  ',
            'description': 'Avionos catalog'
        }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)


@DatabaseSetup(datasetup.simple_user_with_manage_catalog_access)
def test_catalog_create_by_user_with_manage_role_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.post(
        resource_uri.get('catalogs'),
        json={
            'name': '  AvINOS  ',
            'description': 'Avionos catalog'
        }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)
