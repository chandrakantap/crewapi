pipe_fittings_product_type = (
    """INSERT INTO product_type(id,name,status,org_id,created_by,created_on,last_modified_by,last_modified_on)
    VALUES('efc24583-0eed-4db6-a60e-f9e748ca2418','Pipes and Fittings','A',
    '165fa6ab-1893-4108-b88c-a30dfbdc88de',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')""",
)

llp_dustbin_product_type = ("""INSERT INTO product_type(id,name,status,org_id,
    created_by,created_on,last_modified_by,last_modified_on)
    VALUES('b7bf521f-0131-4cb0-a705-e949b7e824ff','LLP DUSTBIN','A','165fa6ab-1893-4108-b88c-a30dfbdc88de',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')""",)

org_seinthon_inc_with_llpdustbin_product_type = (
    """INSERT INTO opunit(id,name,created_by,created_on,
        last_modified_by,last_modified_on)
    VALUES
    (
        '352dd997-734e-4865-b4ff-f27e5d29006a',
        'Seinthon Inc',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         '2019-02-22T17:31:33+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         '2019-02-22T17:31:33+05:30'
    )""",
    """INSERT INTO product_type(id,name,status,org_id,
    created_by,created_on,last_modified_by,last_modified_on)
    VALUES('b7bf521f-0131-4cb0-a705-e949b7e824ff','LLP DUSTBIN','A','352dd997-734e-4865-b4ff-f27e5d29006a',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
    'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30')"""
)
