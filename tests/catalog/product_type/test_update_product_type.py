from tests.ext import DatabaseSetup, AUDIT_PAYLOAD_AUTO_GEN_ATTR
import datasetup_product_type as datasetup
import tests.common_datasetup as common_datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.put(
        resource_uri.get('productTypes') + '/420cc343-528e-4f0a-8155-d095466b8b36',
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.simple_user_without_any_access)
def test_org_context_required(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('productTypes') + '/420cc343-528e-4f0a-8155-d095466b8b36',
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc_user_without_any_access)
def test_user_must_have_manage_product_type_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('productTypes') + '/420cc343-528e-4f0a-8155-d095466b8b36',
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('productTypes') + '/420cc343-528e-4f0a-8155-d095466b8b36',
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_field_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('productTypes') + '/420cc343-528e-4f0a-8155-d095466b8b36',
        json={
            'name': 'Pipes and Fittings',
            'fields': [
                {
                    'type': 'STXT'
                },
                {
                    'displayName': 'Size'
                },
                {
                    'color': 'red'
                }
            ]
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_product_type_must_exist(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('productTypes') + '/420cc343-528e-4f0a-8155-d095466b8b36',
        json={
            'name': 'Pipes and Fittings',
            'units': [{'singular': 'Pc', 'plural': 'Pcs', 'key': 'pc'}],
            'fields': [
                {
                    'type': 'STXT',
                    'displayName': 'Size',
                    'key': 'size',
                    'displayOrder': 1
                }
            ]
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.pipe_fittings_product_type +
               datasetup.llp_dustbin_product_type)
def test_product_type_name_must_be_unique(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('productTypes') + '/efc24583-0eed-4db6-a60e-f9e748ca2418',
        json={
            'name': '   LLP DusTBIN     ',
            'units': [{'singular': 'Pc', 'plural': 'Pcs', 'key': 'pc'}],
            'fields': [
                {
                    'type': 'STXT',
                    'displayName': 'Size',
                    'key': 'size',
                    'displayOrder': 1
                }
            ]
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(datasetup.org_seinthon_inc_with_llpdustbin_product_type +
               common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.pipe_fittings_product_type)
def test_other_org_can_have_same_product_type_name(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('productTypes') + '/efc24583-0eed-4db6-a60e-f9e748ca2418',
        json={
            'name': 'LLP DUSTBIN',
            'description': 'LLP DUSTBIN',
            'units': [{'singular': 'Pc', 'plural': 'Pcs', 'key': 'pc'}],
        }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.pipe_fittings_product_type)
def test_product_type_updated_with_fields(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response_json = client.put(
        resource_uri.get('productTypes') + '/efc24583-0eed-4db6-a60e-f9e748ca2418',
        json={
            'name': 'Pipes and Fittings',
            'units': [{'singular': 'Pc', 'plural': 'Pcs', 'key': 'pc'}],
            'fields': [
                {
                    'displayName': 'Material',
                    'key': 'material',
                    'displayOrder': 1,
                    'type': 'STXT'
                },
                {
                    'displayName': 'Body Type',
                    'key': 'body_type',
                    'type': 'LTXT',
                    'displayOrder': 2
                }
            ],
            'variations': [
                {
                    'displayName': 'Size',
                    'key': 'size',
                    'type': 'STXT',
                    'displayOrder': 1
                },
                {
                    'displayName': 'Colour',
                    'key': 'color',
                    'type': 'COLR',
                    'displayOrder': 2
                }
            ]
        }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)
