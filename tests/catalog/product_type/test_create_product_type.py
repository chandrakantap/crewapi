from tests.ext import DatabaseSetup, AUDIT_PAYLOAD_AUTO_GEN_ATTR
import datasetup_product_type as datasetup
import tests.common_datasetup as common_datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.post(
        resource_uri.get('productTypes'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.simple_user_without_any_access)
def test_org_context_required(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo='9888898888',
          mobileNoCountryCode='+32',
          password='password')
    response = client.post(
        resource_uri.get('productTypes'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc_user_without_any_access)
def test_user_must_have_manage_product_type_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo='9888898888',
          mobileNoCountryCode='+32',
          password='password')
    response = client.post(
        resource_uri.get('productTypes'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo='9888898888',
          mobileNoCountryCode='+32',
          password='password')
    response = client.post(
        resource_uri.get('productTypes'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_field_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo='9888898888',
          mobileNoCountryCode='+32',
          password='password')
    response = client.post(
        resource_uri.get('productTypes'),
        json={
            'name': 'Pipes and Fittings',
            'fields': [
                {'type': 'STXT'},
                {'displayName': 'Size'},
                {'color': 'red'}],
            'variations': [
                {'type': 'MKDN'},
                {'displayName': 'Colour'}],
            'fieldGroups': [
                {'displayName': 'General'},
                {'items': [
                    {'displayOrder': 1,
                     'displayName': 'In The Box',
                     'key': 'in_the_box',
                     'type': 'STXT'}]
                 }]
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_unit_data_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo='9888898888',
          mobileNoCountryCode='+32',
          password='password')
    response = client.post(
        resource_uri.get('productTypes'),
        json={
            'name': 'Pipes and Fittings',
            'units': [
                {'singular': 'Box', 'plural': 'Boxes'},
                {'singular': 'Pc'},
                {'plural': 'Bottle'}
            ]
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user +
               datasetup.pipe_fittings_product_type
               )
def test_product_type_name_must_be_unique(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo='9888898888',
          mobileNoCountryCode='+32',
          password='password')
    response = client.post(
        resource_uri.get('productTypes'),
        json={
            'name': '  pIpeS And fittinGS  ',
            'description': 'pipes and fittings product type',
            'units': [{'singular': 'Pc', 'plural': 'Pcs', 'key': 'pc'}]
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_field_keys_are_unique(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo='9888898888',
          mobileNoCountryCode='+32',
          password='password')
    response = client.post(
        resource_uri.get('productTypes'),
        json={
            'name': 'Pipes and Fittings',
            'units': [{'singular': 'Pc', 'plural': 'Pcs', 'key': 'pc'}],
            'fields': [
                {
                    'displayOrder': 1,
                    'displayName': 'Highlights',
                    'key': 'highlights',
                    'type': 'MKDN'
                },
                {
                    'displayOrder': 2,
                    'displayName': 'Important Note',
                    'key': 'important_note',
                    'type': 'LTXT'
                }
            ],
            'variations': [
                {
                    'displayOrder': 1,
                    'displayName': 'Color',
                    'key': 'highlights',
                    'type': 'COLR'
                },
                {
                    'displayOrder': 2,
                    'displayName': 'Size',
                    'key': 'size',
                    'type': 'LTXT'
                }
            ],
            'fieldGroups': [
                {
                    'displayName': 'General',
                    'displayOrder': 1,
                    'key': 'general',
                    'items': [
                        {
                            'displayOrder': 1,
                            'displayName': 'In The Box',
                            'key': 'important_note',
                            'type': 'STXT'
                        },
                        {
                            'displayOrder': 2,
                            'displayName': 'Type',
                            'key': 'shelf_material',
                            'type': 'STXT'
                        }
                    ]
                },
                {
                    'displayName': 'Body And Design Features',
                    'displayOrder': 2,
                    'key': 'body_and_design_features',
                    'items': [
                        {
                            'displayOrder': 1,
                            'displayName': 'Shelf Material',
                            'key': 'shelf_material',
                            'type': 'STXT'
                        },
                        {
                            'displayOrder': 2,
                            'displayName': 'Size',
                            'key': 'size',
                            'type': 'LTXT'
                        }
                    ]
                }
            ]
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(datasetup.org_seinthon_inc_with_llpdustbin_product_type +
               common_datasetup.org_ortega_inc +
               common_datasetup.org_ortega_inc_admin_user)
def test_other_org_can_have_same_product_type_name(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo='9888898888',
          mobileNoCountryCode='+32',
          password='password')
    response_json = client.post(
        resource_uri.get('productTypes'),
        json={
            'name': 'LLP DUSTBIN',
            'description': 'LLP DUSTBIN',
            'units': [{'singular': 'Pc', 'plural': 'Pcs', 'key': 'pc'}]
        }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_product_type_created_with_fields(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo='9888898888',
          mobileNoCountryCode='+32',
          password='password')
    response_json = client.post(
        resource_uri.get('productTypes'),
        json={
            'name': 'Refrigerators',
            'fields': [
                {
                    'displayOrder': 1,
                    'displayName': 'Highlights',
                    'key': 'highlights',
                    'type': 'MKDN'
                },
                {
                    'displayOrder': 2,
                    'displayName': 'Important Note',
                    'key': 'important_note',
                    'type': 'LTXT'
                }
            ],
            'variations': [
                {
                    'displayOrder': 1,
                    'displayName': 'Color',
                    'key': 'color',
                    'type': 'COLR'
                }
            ],
            'units': [
                {
                    'singular': 'Pc',
                    'plural': 'Pcs',
                    'key': 'pc'
                }
            ]
        }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)


@DatabaseSetup(common_datasetup.org_ortega_inc + common_datasetup.org_ortega_inc_admin_user)
def test_product_type_created_with_field_groups(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo='9888898888',
          mobileNoCountryCode='+32',
          password='password')
    response_json = client.post(
        resource_uri.get('productTypes'),
        json={
            'name': 'Refrigerators',
            'fields': [
                {
                    'displayOrder': 1,
                    'displayName': 'Highlights',
                    'key': 'highlights',
                    'type': 'MKDN'
                },
                {
                    'displayOrder': 2,
                    'displayName': 'Important Note',
                    'key': 'important_note',
                    'type': 'LTXT'
                }
            ],
            'variations': [
                {
                    'displayOrder': 1,
                    'displayName': 'Color',
                    'key': 'color',
                    'type': 'COLR'
                }
            ],
            'units': [
                {
                    'singular': 'Pc',
                    'plural': 'Pcs',
                    'key': 'pc'
                }
            ],
            'fieldGroups': [
                {
                    'displayName': 'General',
                    'displayOrder': 1,
                    'key': 'general',
                    'items': [{
                        'displayOrder': 1,
                        'displayName': 'In The Box',
                        'key': 'in_the_box',
                        'type': 'STXT'
                    },
                        {
                        'displayOrder': 2,
                        'displayName': 'Type',
                        'key': 'type',
                        'type': 'STXT'
                    },
                        {
                        'displayOrder': 3,
                        'displayName': 'Refrigerator Type',
                        'key': 'refrigerator_type',
                        'type': 'STXT'
                    },
                        {
                        'displayOrder': 4,
                        'displayName': 'Compressor Type',
                        'key': 'compressor_type',
                        'type': 'STXT'
                    },
                        {
                        'displayOrder': 5,
                        'displayName': 'Capacity',
                        'key': 'capacity',
                        'type': 'NMBR'
                    }
                    ]
                },
                {
                    'displayName': 'Body And Design Features',
                    'displayOrder': 2,
                    'key': 'body_and_design_features',
                    'items': [
                        {
                            'displayOrder': 1,
                            'displayName': 'Shelf Material',
                            'key': 'shelf_material',
                            'type': 'STXT'
                        },
                        {
                            'displayOrder': 2,
                            'displayName': 'Handle Type',
                            'key': 'handle_type',
                            'type': 'STXT'
                        }
                    ]
                }
            ]
        }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)
