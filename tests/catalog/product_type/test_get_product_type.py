from tests.ext import DatabaseSetup
import datasetup_product_type as datasetup
import tests.common_datasetup as common_datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.get(
        resource_uri.get('productTypes'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.simple_user_without_any_access)
def test_org_context_required(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.get(
        resource_uri.get('productTypes'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc_user_without_any_access)
def test_user_must_have_manage_or_view_product_type_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.get(
        resource_uri.get('productTypes'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(common_datasetup.org_ortega_inc+common_datasetup.org_ortega_inc_admin_user)
def test_empty_result_when_no_data(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.get(resource_uri.get('productTypes'))
    snapshot.assert_match(response.json)
