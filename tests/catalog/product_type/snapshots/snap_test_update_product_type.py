# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_org_context_required 1'] = {
    'message': 'Organisation context is missing',
    'status': 4022
}

snapshots['test_user_must_have_manage_product_type_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'name': 'Please provide product type name.',
        'units': 'Please provide at least one unit.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_field_validation 1'] = {
    'errors': {
        'fields': {
            '0': {
                'displayName': 'Please provide field display name',
                'displayOrder': 'Please provide display order',
                'key': 'Please provide field key.'
            },
            '1': {
                'displayOrder': 'Please provide display order',
                'key': 'Please provide field key.',
                'type': 'Please provide field type.'
            },
            '2': {
                'color': 'Unexpected color',
                'displayName': 'Please provide field display name',
                'displayOrder': 'Please provide display order',
                'key': 'Please provide field key.',
                'type': 'Please provide field type.'
            }
        },
        'units': 'Please provide at least one unit.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_product_type_must_exist 1'] = {
    'message': 'No product type found.',
    'status': 4000
}

snapshots['test_product_type_name_must_be_unique 1'] = {
    'errors': {
        'name': 'A product type with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_other_org_can_have_same_product_type_name 1'] = {
    'message': 'Successfully updated product type.',
    'payload': {
        'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
        'createdOn': 'NOW()',
        'description': 'LLP DUSTBIN',
        'fieldGroups': [
        ],
        'fields': [
        ],
        'id': 'AUTO_GEN',
        'lastModifiedBy': [
            '2e7a6f90-46ac-4d1b-b308-688b37005a97'
        ],
        'lastModifiedOn': 'NOW()',
        'name': 'LLP DUSTBIN',
        'status': 'A',
        'units': [
            {
                'key': 'pc',
                'plural': 'Pcs',
                'singular': 'Pc'
            }
        ],
        'variations': [
        ]
    },
    'status': 2000
}

snapshots['test_product_type_updated_with_fields 1'] = {
    'message': 'Successfully updated product type.',
    'payload': {
        'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
        'createdOn': 'NOW()',
        'description': None,
        'fieldGroups': [
        ],
        'fields': [
            {
                'displayName': 'Material',
                'displayOrder': 1,
                'key': 'material',
                'type': 'STXT'
            },
            {
                'displayName': 'Body Type',
                'displayOrder': 2,
                'key': 'body_type',
                'type': 'LTXT'
            }
        ],
        'id': 'AUTO_GEN',
        'lastModifiedBy': [
            '2e7a6f90-46ac-4d1b-b308-688b37005a97'
        ],
        'lastModifiedOn': 'NOW()',
        'name': 'Pipes and Fittings',
        'status': 'A',
        'units': [
            {
                'key': 'pc',
                'plural': 'Pcs',
                'singular': 'Pc'
            }
        ],
        'variations': [
            {
                'displayName': 'Size',
                'displayOrder': 1,
                'key': 'size',
                'type': 'STXT'
            },
            {
                'displayName': 'Colour',
                'displayOrder': 2,
                'key': 'color',
                'type': 'COLR'
            }
        ]
    },
    'status': 2000
}
