# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_org_context_required 1'] = {
    'message': 'Organisation context is missing',
    'status': 4022
}

snapshots['test_user_must_have_manage_or_view_product_type_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_empty_result_when_no_data 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 0,
        'curPageEnd': 0,
        'curPageStart': 0,
        'items': [
        ],
        'totalCount': 0,
        'totalPages': 0
    },
    'status': 2000
}
