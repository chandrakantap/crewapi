# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_org_context_required 1'] = {
    'message': 'Organisation context is missing',
    'status': 4022
}

snapshots['test_user_must_have_manage_product_type_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'name': 'Please provide product type name.',
        'units': 'Please provide at least one unit.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_field_validation 1'] = {
    'errors': {
        'fieldGroups': {
            '0': {
                'displayOrder': 'Please provide display order',
                'items': 'Please provide at least one item.',
                'key': 'Please provide group key.'
            },
            '1': {
                'displayName': 'Please provide field display name',
                'displayOrder': 'Please provide display order',
                'key': 'Please provide group key.'
            }
        },
        'fields': {
            '0': {
                'displayName': 'Please provide field display name',
                'displayOrder': 'Please provide display order',
                'key': 'Please provide field key.'
            },
            '1': {
                'displayOrder': 'Please provide display order',
                'key': 'Please provide field key.',
                'type': 'Please provide field type.'
            },
            '2': {
                'color': 'Unexpected color',
                'displayName': 'Please provide field display name',
                'displayOrder': 'Please provide display order',
                'key': 'Please provide field key.',
                'type': 'Please provide field type.'
            }
        },
        'units': 'Please provide at least one unit.',
        'variations': {
            '0': {
                'displayName': 'Please provide field display name',
                'displayOrder': 'Please provide display order',
                'key': 'Please provide field key.',
                'type': 'Invalid type provided.'
            },
            '1': {
                'displayOrder': 'Please provide display order',
                'key': 'Please provide field key.'
            }
        }
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_unit_data_validation 1'] = {
    'errors': {
        'units': {
            '0': {
                'key': 'Please provide key'
            },
            '1': {
                'key': 'Please provide key',
                'plural': 'Please provide unit plural form.'
            },
            '2': {
                'key': 'Please provide key',
                'singular': 'Please provide unit singular form.'
            }
        }
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_product_type_name_must_be_unique 1'] = {
    'errors': {
        'name': 'A product type with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_field_keys_are_unique 1'] = {
    'errors': {
        'variations': {
            '1': {
                'type': 'Invalid type provided.'
            }
        }
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_product_type_created_with_field_groups 1'] = {
    'message': 'Successfully created product type.',
    'payload': {
        'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'createdOn': 'NOW()',
        'description': None,
        'fieldGroups': [
            {
                'displayName': 'General',
                'displayOrder': 1,
                'items': [
                    {
                        'displayName': 'In The Box',
                        'displayOrder': 1,
                        'key': 'in_the_box',
                        'type': 'STXT'
                    },
                    {
                        'displayName': 'Type',
                        'displayOrder': 2,
                        'key': 'type',
                        'type': 'STXT'
                    },
                    {
                        'displayName': 'Refrigerator Type',
                        'displayOrder': 3,
                        'key': 'refrigerator_type',
                        'type': 'STXT'
                    },
                    {
                        'displayName': 'Compressor Type',
                        'displayOrder': 4,
                        'key': 'compressor_type',
                        'type': 'STXT'
                    },
                    {
                        'displayName': 'Capacity',
                        'displayOrder': 5,
                        'key': 'capacity',
                        'type': 'NMBR'
                    }
                ],
                'key': 'general'
            },
            {
                'displayName': 'Body And Design Features',
                'displayOrder': 2,
                'items': [
                    {
                        'displayName': 'Shelf Material',
                        'displayOrder': 1,
                        'key': 'shelf_material',
                        'type': 'STXT'
                    },
                    {
                        'displayName': 'Handle Type',
                        'displayOrder': 2,
                        'key': 'handle_type',
                        'type': 'STXT'
                    }
                ],
                'key': 'body_and_design_features'
            }
        ],
        'fields': [
            {
                'displayName': 'Highlights',
                'displayOrder': 1,
                'key': 'highlights',
                'type': 'MKDN'
            },
            {
                'displayName': 'Important Note',
                'displayOrder': 2,
                'key': 'important_note',
                'type': 'LTXT'
            }
        ],
        'id': 'AUTO_GEN',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'NOW()',
        'name': 'Refrigerators',
        'status': 'A',
        'units': [
            {
                'key': 'pc',
                'plural': 'Pcs',
                'singular': 'Pc'
            }
        ],
        'variations': [
            {
                'displayName': 'Color',
                'displayOrder': 1,
                'key': 'color',
                'type': 'COLR'
            }
        ]
    },
    'status': 2000
}

snapshots['test_product_type_created_with_fields 1'] = {
    'message': 'Successfully created product type.',
    'payload': {
        'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'createdOn': 'NOW()',
        'description': None,
        'fieldGroups': [
        ],
        'fields': [
            {
                'displayName': 'Highlights',
                'displayOrder': 1,
                'key': 'highlights',
                'type': 'MKDN'
            },
            {
                'displayName': 'Important Note',
                'displayOrder': 2,
                'key': 'important_note',
                'type': 'LTXT'
            }
        ],
        'id': 'AUTO_GEN',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'NOW()',
        'name': 'Refrigerators',
        'status': 'A',
        'units': [
            {
                'key': 'pc',
                'plural': 'Pcs',
                'singular': 'Pc'
            }
        ],
        'variations': [
            {
                'displayName': 'Color',
                'displayOrder': 1,
                'key': 'color',
                'type': 'COLR'
            }
        ]
    },
    'status': 2000
}

snapshots['test_other_org_can_have_same_product_type_name 1'] = {
    'message': 'Successfully created product type.',
    'payload': {
        'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'createdOn': 'NOW()',
        'description': 'LLP DUSTBIN',
        'fieldGroups': [
        ],
        'fields': [
        ],
        'id': 'AUTO_GEN',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'NOW()',
        'name': 'LLP DUSTBIN',
        'status': 'A',
        'units': [
            {
                'key': 'pc',
                'plural': 'Pcs',
                'singular': 'Pc'
            }
        ],
        'variations': [
        ]
    },
    'status': 2000
}
