from tests.ext import ADMIN_ROLE_ID


# password is 'password'
simple_user_without_any_access = ("""INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                null,
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """, )


# password is 'password'
app_admin_user = ("""INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '7689345621',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """, )

org_ortega_inc = (
    """INSERT INTO opunit(
        id,name,created_by,created_on,
        last_modified_by,last_modified_on)
    VALUES
    (
        '165fa6ab-1893-4108-b88c-a30dfbdc88de',
        'Ortega Inc',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         NOW(),
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         NOW()
    )""",
    """INSERT INTO visible_opunits(opunit_id,visible_opunit)
       VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de')""",
)

org_ortega_inc_zonal_manager_role = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'Zonal Managger',
            'Zonal Managger role',
            '165fa6ab-1893-4108-b88c-a30dfbdc88de',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",)

org_ortega_inc_admin_user = (
    simple_user_without_any_access +
    ("""INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','"""+ADMIN_ROLE_ID+"""')
    """,)
)

org_ortega_inc_user_without_any_access = (
    simple_user_without_any_access +
    org_ortega_inc +
    org_ortega_inc_zonal_manager_role +
    ('''
    INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','1c0dd6d5-6a11-4f91-82ef-e596730663ae')
    ''',)
)
