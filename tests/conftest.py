import pytest
from crewapi import create_app
import psycopg2

TEST_DB_CONFIG = {
    'host': 'localhost',
    'port': 5432,
    'database': 'crewapi_test'
}


@pytest.fixture(scope="session")
def app():

    app = create_app({
        'SQLALCHEMY_DATABASE_URI': f"""postgresql+psycopg2://{TEST_DB_CONFIG.get('host')}:{TEST_DB_CONFIG.get('port')}
                                       /{TEST_DB_CONFIG.get('database')}""",
        'SQLALCHEMY_ECHO': False,
        'SQLALCHEMY_TRACK_MODIFICATIONS': False
    })

    return app


@pytest.fixture(scope="session")
def db_connection():
    conn = psycopg2.connect(**TEST_DB_CONFIG)
    yield conn
    conn.close()


@pytest.fixture(scope="session", autouse=True)
def db_migrate(app):
    with app.app_context():
        from flask_migrate import upgrade as _upgrade
        _upgrade()


@pytest.fixture(scope="session")
def resource_uri(app):
    api_prefix = app.config['APPLICATION_ROOT']
    return {
        "sessions": f"{api_prefix}/sessions",
        "organisations": f"{api_prefix}/organisations",
        "organisation": api_prefix + "/organisations/{0}",
        "password": f"{api_prefix}/password",
        "opunits": api_prefix + "/opunits",
        "app_roles": f"{api_prefix}/app/roles",
        "opunit": api_prefix + "/opunits/{0}",
        "app_role": api_prefix + "/app/roles/{0}",
        "catalogs": api_prefix + "/catalogs",
        "categories": api_prefix + "/catalogs/{0}/categories",
        "productTypes": api_prefix+"/product-types",
        "app_users": f"{api_prefix}/app/users",
        "catalog": api_prefix + "/catalogs/{0}",
        "category": api_prefix + "/catalogs/{0}/categories/{1}"
    }


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture(scope="function")
def login(client, resource_uri):
    def login_function(mobileNo, mobileNoCountryCode, password):
        return client.post(
            resource_uri.get('sessions'),
            json={
                "mobileNo": mobileNo,
                "mobileNoCountryCode": mobileNoCountryCode,
                "password": password
            })

    return login_function
