from tests.ext import DatabaseSetup
from datasetup_session import simple_user_without_any_access


def test_success_when_not_logged_in(client, resource_uri, snapshot):
    response = client.delete(resource_uri.get('sessions'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=simple_user_without_any_access)
def test_success_when_logged_in(db_connection, login, client, resource_uri, snapshot):
    login(
        mobileNo="9888898888",
        mobileNoCountryCode="+32",
        password="password")

    response_json = client.get(resource_uri.get('sessions')).json

    response_payload = response_json.get("payload", {})
    assert response_payload.get("id") is not None
    response_payload["id"] = "AUTO GENERATED"
    response_json["payload"] = response_payload
    snapshot.assert_match(response_json)

    response = client.delete(resource_uri.get('sessions'))
    snapshot.assert_match(response.json)

    response = client.get(resource_uri.get('sessions'))
    snapshot.assert_match(response.json)
