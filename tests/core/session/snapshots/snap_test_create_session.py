# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_successful_login_not_app_admin 1'] = {
    'message': 'Log in successfully.',
    'payload': {
        'accessibleOpunitCount': 0,
        'accessibleOrgCount': 0,
        'appRole': {
            'id': '4187d451-55df-47df-815d-101f8ad6f587',
            'name': 'MANAGER'
        },
        'id': 'AUTO GENERATED',
        'isFirstLogin': True,
        'opunit': None,
        'opunitRole': None,
        'organisation': None,
        'organisationRole': None,
        'permissions': {
            'app': {
                'user': {
                    'edit_profile': True,
                    'reset_credentials': True,
                    'view_users': True
                }
            }
        },
        'userId': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'userName': 'Mary Barry'
    },
    'status': 2000
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'mobileNo': 'Please enter your mobile no.',
        'mobileNoCountryCode': 'Please enter country code of mobile no.',
        'password': 'Please enter password'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_invalid_mobile_no 1'] = {
    'message': 'No user found with provide mobile no and password.',
    'status': 4000
}

snapshots['test_invalid_mobile_no_country_code 1'] = {
    'message': 'No user found with provide mobile no and password.',
    'status': 4000
}

snapshots['test_invalid_password 1'] = {
    'message': 'No user found with provide mobile no and password.',
    'status': 4000
}

snapshots['test_successful_login_app_admin 1'] = {
    'message': 'Log in successfully.',
    'payload': {
        'accessibleOpunitCount': 0,
        'accessibleOrgCount': 0,
        'appRole': {
            'id': '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
            'name': 'ADMIN'
        },
        'id': 'AUTO GENERATED',
        'isFirstLogin': True,
        'opunit': None,
        'opunitRole': None,
        'organisation': None,
        'organisationRole': None,
        'permissions': {
            'app': {
                'organisation': {
                    'create_org': True,
                    'deactivate_org': True,
                    'edit_org_details': True,
                    'view_org': True
                },
                'role': {
                    'manage_role': True,
                    'view_roles': True
                },
                'user': {
                    'create_user': True,
                    'edit_profile': True,
                    'remove_user': True,
                    'reset_credentials': True,
                    'view_users': True
                }
            }
        },
        'userId': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'userName': 'Mary Barry'
    },
    'status': 2000
}

snapshots['test_successful_login_org_admin 1'] = {
    'message': 'Log in successfully.',
    'payload': {
        'accessibleOpunitCount': 1,
        'accessibleOrgCount': 1,
        'appRole': None,
        'id': 'AUTO_GENERATED',
        'isFirstLogin': True,
        'opunit': {
            'id': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
            'name': 'Ortega Inc'
        },
        'opunitRole': {
            'id': '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
            'name': 'ADMIN'
        },
        'organisation': {
            'id': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
            'name': 'Ortega Inc'
        },
        'organisationRole': {
            'id': '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
            'name': 'ADMIN'
        },
        'permissions': {
            'opunit': {
                'opunit': {
                    'create_opunit': True,
                    'delete_opunit': True,
                    'edit_opunit_details': True,
                    'view_opunit': True
                },
                'user': {
                    'create_user': True,
                    'edit_profile': True,
                    'remove_user': True,
                    'reset_credentials': True,
                    'view_users': True
                }
            },
            'org': {
                'catalog': {
                    'manage_catalog': True,
                    'review_catalog': True,
                    'view_catalog': True
                },
                'organisation': {
                    'edit_org_details': True
                },
                'product_type': {
                    'manage_product_type': True,
                    'view_product_type': True
                },
                'role': {
                    'manage_role': True,
                    'view_roles': True
                }
            }
        },
        'userId': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'userName': 'Mary Barry'
    },
    'status': 2000
}

snapshots['test_successful_login_org_admin_with_last_selected 1'] = {
    'message': 'Log in successfully.',
    'payload': {
        'accessibleOpunitCount': 1,
        'accessibleOrgCount': 1,
        'appRole': None,
        'id': 'AUTO_GENERATED',
        'isFirstLogin': True,
        'opunit': {
            'id': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
            'name': 'Ortega Inc'
        },
        'opunitRole': {
            'id': '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
            'name': 'ADMIN'
        },
        'organisation': {
            'id': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
            'name': 'Ortega Inc'
        },
        'organisationRole': {
            'id': '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
            'name': 'ADMIN'
        },
        'permissions': {
            'opunit': {
                'opunit': {
                    'create_opunit': True,
                    'delete_opunit': True,
                    'edit_opunit_details': True,
                    'view_opunit': True
                },
                'user': {
                    'create_user': True,
                    'edit_profile': True,
                    'remove_user': True,
                    'reset_credentials': True,
                    'view_users': True
                }
            },
            'org': {
                'catalog': {
                    'manage_catalog': True,
                    'review_catalog': True,
                    'view_catalog': True
                },
                'organisation': {
                    'edit_org_details': True
                },
                'product_type': {
                    'manage_product_type': True,
                    'view_product_type': True
                },
                'role': {
                    'manage_role': True,
                    'view_roles': True
                }
            }
        },
        'userId': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'userName': 'Mary Barry'
    },
    'status': 2000
}
