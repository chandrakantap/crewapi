# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_success_when_not_logged_in 1'] = {
    'message': 'Success',
    'status': 2000
}

snapshots['test_success_when_logged_in 1'] = {
    'message': 'Success',
    'payload': {
        'accessibleOpunitCount': 0,
        'accessibleOrgCount': 0,
        'appRole': None,
        'id': 'AUTO GENERATED',
        'isFirstLogin': True,
        'opunit': None,
        'opunitRole': None,
        'organisation': None,
        'organisationRole': None,
        'permissions': {
        },
        'userId': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'userName': 'Mary Barry'
    },
    'status': 2000
}

snapshots['test_success_when_logged_in 2'] = {
    'message': 'Success',
    'status': 2000
}

snapshots['test_success_when_logged_in 3'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}
