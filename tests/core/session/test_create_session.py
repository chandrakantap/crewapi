from tests.ext import DatabaseSetup
import datasetup_session as datasetup


def test_request_validation(client, resource_uri, snapshot):
    response = client.post(resource_uri.get('sessions'), json={})
    snapshot.assert_match(response.json)


def test_invalid_mobile_no(login, snapshot):
    response = login(
        mobileNo="9876543456",
        mobileNoCountryCode="+91",
        password="riythu")
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_invalid_mobile_no_country_code(db_connection, client, resource_uri, snapshot):
    response = client.post(
        resource_uri.get('sessions'),
        json={
            "mobileNo": "9888898888",
            "mobileNoCountryCode": "+91",
            "password": "riythu"
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_invalid_password(db_connection, client, resource_uri, snapshot):
    response = client.post(
        resource_uri.get('sessions'),
        json={
            "mobileNo": "9888898888",
            "mobileNoCountryCode": "+32",
            "password": "DifferentP@$$w0rd"
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_successful_login_app_admin(db_connection, client, resource_uri, snapshot):
    response_json = client.post(
        resource_uri.get('sessions'),
        json={
            "mobileNo": "9888898888",
            "mobileNoCountryCode": "+32",
            "password": "password"
        }).json

    response_payload = response_json.get("payload", {})
    assert response_payload.get("id") is not None

    response_payload["id"] = "AUTO GENERATED"
    response_json["payload"] = response_payload
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=datasetup.simple_user_with_manager_access)
def test_successful_login_not_app_admin(db_connection, client, resource_uri, snapshot):
    response_json = client.post(
        resource_uri.get('sessions'),
        json={
            "mobileNo": "9888898888",
            "mobileNoCountryCode": "+32",
            "password": "password"
        }).json

    response_payload = response_json.get("payload", {})
    assert response_payload.get("id") is not None

    response_payload["id"] = "AUTO GENERATED"
    response_json["payload"] = response_payload
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc+datasetup.org_admin_user)
def test_successful_login_org_admin(db_connection, client, resource_uri, snapshot):
    response_json = client.post(
        resource_uri.get('sessions'),
        json={
            "mobileNo": "9888898888",
            "mobileNoCountryCode": "+32",
            "password": "password"
        }).json

    response_json["payload"] = {**response_json.get("payload", {}), **{'id': 'AUTO_GENERATED'}}
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc+datasetup.org_admin_user_with_last_selected)
def test_successful_login_org_admin_with_last_selected(db_connection, client, resource_uri, snapshot):
    response_json = client.post(
        resource_uri.get('sessions'),
        json={
            "mobileNo": "9888898888",
            "mobileNoCountryCode": "+32",
            "password": "password"
        }).json

    response_json["payload"] = {**response_json.get("payload", {}), **{'id': 'AUTO_GENERATED'}}
    snapshot.assert_match(response_json)
