from tests.ext import ADMIN_ROLE_ID

# password is 'password'
app_admin_user = ("""INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                '"""+ADMIN_ROLE_ID+"""',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """, )

# password is 'password'
simple_user_without_any_access = ("""INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                null,
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """, )

simple_user_with_manager_access = (
    """
    INSERT INTO role(id,name,description,owner_id,created_by,created_on,
                    last_modified_by,last_modified_on)
        VALUES
        (
            '4187d451-55df-47df-815d-101f8ad6f587',
            'MANAGER',
            'Manager role.',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )
    """,
    """
        INSERT INTO role_has_permission(role_id,permission_id)
        VALUES
        ('4187d451-55df-47df-815d-101f8ad6f587','fb3cd7ca-908a-4071-b62f-70d72a4f906f'),
        ('4187d451-55df-47df-815d-101f8ad6f587','d9a5fe2c-e08a-4dca-bc25-09b2c3da4ae1'),
        ('4187d451-55df-47df-815d-101f8ad6f587','83eb58b2-60a3-4ef3-85b7-db9580da5a2f')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                '4187d451-55df-47df-815d-101f8ad6f587',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
)

org_ortega_inc = (
    """INSERT INTO opunit(
        id,name,created_by,created_on,
        last_modified_by,last_modified_on)
    VALUES
    (
        '165fa6ab-1893-4108-b88c-a30dfbdc88de',
        'Ortega Inc',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         NOW(),
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         NOW()
    )""",
    """INSERT INTO visible_opunits(opunit_id,visible_opunit)
       VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de')"""
)

org_admin_user = (
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                null,
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
    """INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','"""+ADMIN_ROLE_ID+"""')
    """
)

org_admin_user_with_last_selected = (
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            last_selected_org_id,
            last_selected_opunit_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                null,
                '165fa6ab-1893-4108-b88c-a30dfbdc88de',
                '165fa6ab-1893-4108-b88c-a30dfbdc88de',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
    """INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','"""+ADMIN_ROLE_ID+"""')
    """
)
