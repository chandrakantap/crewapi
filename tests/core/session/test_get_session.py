from tests.ext import DatabaseSetup
from datasetup_session import simple_user_without_any_access
from flask import json
from jwt import encode as jwt_encode


def test_error_when_not_logged_in(client, resource_uri, snapshot):
    response = client.get(resource_uri.get('sessions'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=simple_user_without_any_access)
def test_error_when_forged_jwt(db_connection, app, login, resource_uri, snapshot):
    response = login(
        mobileNo="9888898888",
        mobileNoCountryCode="+32",
        password="password")

    session_id = response.json.get('payload', {}).get('id', None)
    assert session_id is not None

    jwt_algo = app.config.get('JWT_ALGO')
    token_name = app.config.get('AUTH_TOKEN_NAME')

    session_token = jwt_encode(
        {
            'id': session_id
        },
        'TBvsGybvt4yVb0p2CiszNyheF4jN8U+MeqMMArjxjtwv7VIw8P7o1t2Y6CQKalpHOd\
        oxBQn7lIiEU1K9gYNChfBBcoElZaX5ObH1j7THjn',
        algorithm=jwt_algo,
        json_encoder=json.JSONEncoder)

    client = app.test_client()
    client.set_cookie('/', token_name, session_token)
    response = client.get(resource_uri.get('sessions'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=simple_user_without_any_access)
def test_error_when_invalid_jwt(db_connection, app, resource_uri, snapshot):
    client = app.test_client()

    token_name = app.config.get('AUTH_TOKEN_NAME')

    client.set_cookie(
        '/', token_name, 'TBvsGybvt4yVb0p2CiszNyheF4jN8U+MeqMMArjxjtwv\
        7VIw8P7o1t2Y6CQKalpHOdoxBQn7lIiEU1K9gYNChfBBcoElZaX5ObH1j7THjn')
    response = client.get(resource_uri.get('sessions'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=simple_user_without_any_access)
def test_success_when_logged_in(db_connection, login, client, resource_uri, snapshot):
    login(
        mobileNo="9888898888",
        mobileNoCountryCode="+32",
        password="password")

    response_json = client.get(resource_uri.get('sessions')).json
    response_payload = response_json.get("payload", {})
    assert response_payload.get("id") is not None

    response_payload["id"] = "AUTO GENERATED"
    response_json["payload"] = response_payload
    snapshot.assert_match(response_json)
