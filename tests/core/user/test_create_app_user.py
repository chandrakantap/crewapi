from tests.ext import DatabaseSetup, AUDIT_PAYLOAD_AUTO_GEN_ATTR
import datasetup_user as datasetup

def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.post(
        resource_uri.get('app_users'),
        json = {})

    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries = datasetup.app_user_without_create_app_user_access)
def test_user_must_have_create_app_user_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_users'),
        json = {})

    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries = datasetup.app_user_with_create_app_user_access)
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_users'),
        json = {})

    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries = datasetup.app_user_with_create_app_user_access)
def test_error_when_app_role_not_defined(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_users'),
        json = {
            'name': 'John Kim',
            'mobileNo': '9888898899',
            'mobileNoCountryCode': '+32'
        })

    snapshot.assert_match(response.json)



@DatabaseSetup(setup_queries = datasetup.app_user_with_create_app_user_access)
def test_error_when_app_role_not_exist(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_users'),
        json = {
            'name': 'John Kim',
            'mobileNo': '9888898899',
            'mobileNoCountryCode': '+32',
            'role': '1c0dd6d5-6a11-4f91-82ef-e596730663af'
        })

    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries = datasetup.app_user_with_create_app_user_access+datasetup.app_user_new_role)
def test_error_when_user_cannot_grant_role(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_users'),
        json = {
            'name': 'John Kim',
            'mobileNo': '9888898899',
            'mobileNoCountryCode': '+32',
            'role': '1c0dd6d5-6a11-4f91-82ef-e596730663af'
        })

    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries = datasetup.app_user_with_create_app_user_access+datasetup.app_user_new_role+datasetup.app_user_can_grant_roles)
def test_when_new_user_role_isadmin(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_users'),
        json = {
            'name': 'John Kim',
            'mobileNo': '9888898899',
            'mobileNoCountryCode': '+32',
            'role': '0ba7eee1-a0dd-4067-8626-39cd1c7138fa'
        })

    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries = datasetup.app_user_with_create_app_user_access+datasetup.app_user_new_role+datasetup.app_user_can_grant_roles)
def test_error_when_request_validation_fails(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_users'),
        json = {
            'name': 'John Kim',
            'mobileNo': '9888898888',
            'mobileNoCountryCode': '+32',
            'role': '1c0dd6d5-6a11-4f91-82ef-e596730663af'
        })

    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries = datasetup.app_user_with_create_app_user_access+datasetup.app_user_new_role+datasetup.app_user_can_grant_roles)
def test_successful_app_user_creation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.post(
        resource_uri.get('app_users'),
        json = {
            'name': 'John Kim',
            'mobileNo': '9888898899',
            'mobileNoCountryCode': '+32',
            'role': '1c0dd6d5-6a11-4f91-82ef-e596730663af'
        }).json

    response_json["payload"] = {**response_json.get("payload", {}), **{'id': 'AUTO_GENERATED'}}
    snapshot.assert_match(response_json)
    