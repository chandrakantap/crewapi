# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_error_when_request_validation_fails 1'] = {
    'errors': {
        'mobileNo': '9888898888 being used by another user.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_user_must_have_create_app_user_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'mobileNo': 'Please provide ten digit mobile no',
        'mobileNoCountryCode': 'Please provide mobile no country code',
        'name': 'Please provide user name',
        'role': 'Please provide role of the user'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_error_when_app_role_not_defined 1'] = {
    'errors': {
        'role': 'Please provide role of the user'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_error_when_app_role_not_exist 1'] = {
    'message': 'Please provide a valid role.',
    'status': 4000
}

snapshots['test_error_when_user_cannot_grant_role 1'] = {
    'message': 'You are not authorized to add user with role APP User',
    'status': 4000
}

snapshots['test_successful_app_user_creation 1'] = {
    'message': 'Successfully created app user.',
    'payload': {
        'address': None,
        'emailId': None,
        'id': 'AUTO_GENERATED',
        'mobileNo': '9888898899',
        'mobileNoCountryCode': '+32',
        'name': 'John Kim',
        'role': {
            'id': '1c0dd6d5-6a11-4f91-82ef-e596730663af',
            'name': 'APP User'
        }
    },
    'status': 2000
}

snapshots['test_when_new_user_role_isadmin 1'] = {
    'message': 'Only an Admin can add user in admin role.',
    'status': 4000
}
