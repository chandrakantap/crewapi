# password is 'password'
app_user_without_create_app_user_access = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'APP Managger',
            'APP Managger role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','83eb58b2-60a3-4ef3-85b7-db9580da5a2f')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                '93ee912b-d051-46cc-83bb-988c6a44459f',
                '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
                '93ee912b-d051-46cc-83bb-988c6a44459f',
                 NOW(),
                '93ee912b-d051-46cc-83bb-988c6a44459f',
                 NOW()
            )
    """
)

# password is 'password'
app_user_with_create_app_user_access = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'APP Managger',
            'APP Managger role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','316352f3-25fc-4881-9a62-370a1e7dc84b')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                '93ee912b-d051-46cc-83bb-988c6a44459f',
                '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
                '93ee912b-d051-46cc-83bb-988c6a44459f',
                 NOW(),
                '93ee912b-d051-46cc-83bb-988c6a44459f',
                 NOW()
            )
    """
)


app_user_new_role = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663af',
            'APP User',
            'APP User role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663af','83eb58b2-60a3-4ef3-85b7-db9580da5a2f')
    """
)


app_user_can_grant_roles = (
    """INSERT INTO role_grant_role(role_id, can_grant_role)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae', '1c0dd6d5-6a11-4f91-82ef-e596730663af'
        )
    """,
    """INSERT INTO role_grant_role(role_id, can_grant_role)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae', '0ba7eee1-a0dd-4067-8626-39cd1c7138fa'
        )
    """
)
