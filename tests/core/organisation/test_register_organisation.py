from tests.ext import DatabaseSetup, AUDIT_PAYLOAD_AUTO_GEN_ATTR
import datasetup_organisation as datasetup


def test_request_validation(client, resource_uri, snapshot):
    response = client.post(resource_uri.get('organisations'), json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc)
def test_unique_organisation_name_case_insensitive(db_connection, client, resource_uri, snapshot):
    response = client.post(resource_uri.get('organisations'), json={
        'adminUserName': 'Chandrakanta Pal',
        'adminUserMobileNoCountryCode': '+91',
        'adminUserMobileNo': '7898786756',
        'organisationName': '   OrteGa INC   '
    })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.simple_user_without_any_access)
def test_unique_user(db_connection, client, resource_uri, snapshot):
    response = client.post(resource_uri.get('organisations'), json={
        'adminUserName': 'Chandrakanta Pal',
        'adminUserMobileNoCountryCode': '+32',
        'adminUserMobileNo': '9888898888',
        'organisationName': 'Castillo-Wood'
    })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.simple_user_without_any_access)
def test_create_mobile_no_same_country_code_different(db_connection, client, resource_uri, snapshot):
    response_json = client.post(resource_uri.get('organisations'), json={
        'adminUserName': 'Chandrakanta Pal',
        'adminUserMobileNoCountryCode': '+91',
        'adminUserMobileNo': '9888898888',
        'organisationName': 'Scott LLC'
    }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)
