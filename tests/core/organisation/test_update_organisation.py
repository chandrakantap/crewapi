from tests.ext import DatabaseSetup
import datasetup_organisation as datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.put(
        resource_uri.get('organisation').format('3de1611d-64df-4a8e-83a4-23d50fdb078e'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc + datasetup.simple_user_without_any_access))
def test_error_if_user_dont_have_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.put(resource_uri.get('organisation').format('165fa6ab-1893-4108-b88c-a30dfbdc88de'), json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc + datasetup.simple_user_with_update_org_access))
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.put(resource_uri.get('organisation').format('165fa6ab-1893-4108-b88c-a30dfbdc88de'), json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_update_org_access +
                              datasetup.org_seinthon_inc))
def test_unique_organisation_name_case_insensitive(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.put(resource_uri.get('organisation').format('165fa6ab-1893-4108-b88c-a30dfbdc88de'), json={
        'name': 'SeinTHon Inc    '
    })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc + datasetup.app_admin_user))
def test_success_for_app_admin(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="7689345621",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.put(resource_uri.get('organisation').format('165fa6ab-1893-4108-b88c-a30dfbdc88de'), json={
        'name': 'SeinTHon Inc    '
    }).json
    response_json['payload'] = {**response_json.get('payload'), **{'lastModifiedOn': 'AUTO GENERATED'}}
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc + datasetup.org_ortega_inc_admin_user))
def test_success_for_org_admin(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.put(resource_uri.get('organisation').format('165fa6ab-1893-4108-b88c-a30dfbdc88de'), json={
        'name': 'SeinTHon Inc    '
    }).json
    response_json['payload'] = {**response_json.get('payload'), **{'lastModifiedOn': 'AUTO GENERATED'}}
    snapshot.assert_match(response_json)


def test_success_when_have_app_org_update_permission():
    pass


def test_success_when_have_org_org_update_permission():
    pass
