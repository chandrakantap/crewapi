# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_unique_organisation_name_case_insensitive 1'] = {
    'errors': {
        'name': 'An organisation with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_error_if_user_dont_have_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'name': 'Organisation name is required.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_successful_org_creation_when_user_have_access 1'] = {
    'message': 'Successfully created organisation.',
    'payload': {
        'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'createdOn': 'NOW()',
        'description': None,
        'emailId': None,
        'id': 'AUTO_GEN',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'NOW()',
        'name': 'Wofied Oreatra',
        'officeAddress': None,
        'parentOpunit': None,
        'phoneNo': None
    },
    'status': 2000
}

snapshots['test_successful_org_creation_when_user_is_app_admin 1'] = {
    'message': 'Successfully created organisation.',
    'payload': {
        'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'createdOn': 'NOW()',
        'description': None,
        'emailId': None,
        'id': 'AUTO_GEN',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'NOW()',
        'name': 'Wofied Oreatra',
        'officeAddress': None,
        'parentOpunit': None,
        'phoneNo': None
    },
    'status': 2000
}

snapshots['test_user_must_be_logged_in 1'] = {
    'errors': {
        'adminUserMobileNo': 'Please enter mobile no',
        'adminUserMobileNoCountryCode': 'Please enter your country',
        'adminUserName': 'Please provide your name.',
        'organisationName': 'Organisation name is required.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}
