# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_error_if_user_dont_have_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_empty_result_when_no_data 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 0,
        'curPageEnd': 0,
        'curPageStart': 0,
        'items': [
        ],
        'totalCount': 0,
        'totalPages': 0
    },
    'status': 2000
}

snapshots['test_success_with_proper_response 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 1,
        'curPageEnd': 5,
        'curPageStart': 1,
        'items': [
            {
                'createdBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
                'createdOn': '2019-02-18T12:13:45+05:30',
                'description': None,
                'emailId': None,
                'id': 'e05b4741-8691-4f62-adfd-6c894d05e53e',
                'lastModifiedBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
                'lastModifiedOn': '2019-02-18T12:13:45+05:30',
                'name': 'Ruiz-Shaffer',
                'officeAddress': None,
                'parentOpunit': None,
                'phoneNo': None
            },
            {
                'createdBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
                'createdOn': '2019-02-18T11:38:45+05:30',
                'description': None,
                'emailId': None,
                'id': '6fecedb9-506a-4359-95c7-094bccf1d6b2',
                'lastModifiedBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
                'lastModifiedOn': '2019-02-18T11:38:45+05:30',
                'name': 'Greene-Rocha',
                'officeAddress': None,
                'parentOpunit': None,
                'phoneNo': None
            },
            {
                'createdBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
                'createdOn': '2019-02-18T11:35:45+05:30',
                'description': None,
                'emailId': None,
                'id': '8f3bfdfa-dcfb-4d33-8a14-d546da753e48',
                'lastModifiedBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
                'lastModifiedOn': '2019-02-18T11:35:45+05:30',
                'name': 'Trujillo-Wolf',
                'officeAddress': None,
                'parentOpunit': None,
                'phoneNo': None
            },
            {
                'createdBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
                'createdOn': '2019-02-18T11:30:45+05:30',
                'description': None,
                'emailId': None,
                'id': 'bc7379f8-2fa5-4e6c-aee0-19a048e4cdf3',
                'lastModifiedBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
                'lastModifiedOn': '2019-02-18T11:30:45+05:30',
                'name': 'Hensley PLC',
                'officeAddress': None,
                'parentOpunit': None,
                'phoneNo': None
            },
            {
                'createdBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
                'createdOn': '2019-02-18T10:00:45+05:30',
                'description': None,
                'emailId': None,
                'id': 'b862cbef-f156-49a3-98a6-402ac2014fc1',
                'lastModifiedBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
                'lastModifiedOn': '2019-02-18T10:25:45+05:30',
                'name': 'Velazquez and Sons',
                'officeAddress': None,
                'parentOpunit': None,
                'phoneNo': None
            }
        ],
        'totalCount': 5,
        'totalPages': 1
    },
    'status': 2000
}
