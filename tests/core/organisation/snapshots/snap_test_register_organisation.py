# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_request_validation 1'] = {
    'errors': {
        'adminUserMobileNo': 'Please enter mobile no',
        'adminUserMobileNoCountryCode': 'Please enter your country',
        'adminUserName': 'Please provide your name.',
        'organisationName': 'Organisation name is required.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_unique_organisation_name_case_insensitive 1'] = {
    'errors': {
        'organisationName': 'An organisation with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_unique_user 1'] = {
    'errors': {
        'adminUserMobileNo': 'An user with same mobile no exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_create_mobile_no_same_country_code_different 1'] = {
    'message': 'Success',
    'payload': {
        'createdBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
        'createdOn': 'NOW()',
        'description': None,
        'emailId': None,
        'id': 'AUTO_GEN',
        'lastModifiedBy': '93ee912b-d051-46cc-83bb-988c6a44459f',
        'lastModifiedOn': 'NOW()',
        'name': 'Scott LLC',
        'officeAddress': None,
        'parentOpunit': None,
        'phoneNo': None
    },
    'status': 2000
}
