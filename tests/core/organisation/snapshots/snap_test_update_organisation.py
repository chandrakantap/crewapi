# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_error_if_user_dont_have_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'name': 'Organisation name is required.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_unique_organisation_name_case_insensitive 1'] = {
    'errors': {
        'name': 'An organisation with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_success_for_app_admin 1'] = {
    'message': 'Successfully updated organisation.',
    'payload': {
        'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
        'createdOn': '2019-02-22T17:31:33+05:30',
        'description': None,
        'emailId': None,
        'id': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'AUTO GENERATED',
        'name': 'SeinTHon Inc',
        'officeAddress': None,
        'parentOpunit': None,
        'phoneNo': None
    },
    'status': 2000
}

snapshots['test_success_for_org_admin 1'] = {
    'message': 'Successfully updated organisation.',
    'payload': {
        'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
        'createdOn': '2019-02-22T17:31:33+05:30',
        'description': None,
        'emailId': None,
        'id': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'AUTO GENERATED',
        'name': 'SeinTHon Inc',
        'officeAddress': None,
        'parentOpunit': None,
        'phoneNo': None
    },
    'status': 2000
}
