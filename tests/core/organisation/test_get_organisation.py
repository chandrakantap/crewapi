from tests.ext import DatabaseSetup
import datasetup_organisation as datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.get(resource_uri.get('organisations'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.simple_user_without_any_access)
def test_error_if_user_dont_have_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.get(resource_uri.get('organisations'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_empty_result_when_no_data(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="7689345621",
          mobileNoCountryCode="+32",
          password="password")

    response = client.get(resource_uri.get('organisations'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user + datasetup.opunits)
def test_success_with_proper_response(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="7689345621",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.get(resource_uri.get('organisations')).json
    snapshot.assert_match(response_json)
