from tests.ext import ADMIN_ROLE_ID

org_ortega_inc = ("""INSERT INTO opunit(
                    id,name,created_by,created_on,
                    last_modified_by,last_modified_on)
                    VALUES
                    (
                        '165fa6ab-1893-4108-b88c-a30dfbdc88de',
                        'Ortega Inc',
                        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                        '2019-02-22T17:31:33+05:30',
                        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                        '2019-02-22T17:31:33+05:30'
                    )""",
                  """INSERT INTO visible_opunits(opunit_id,visible_opunit)
                    VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de')""")

org_seinthon_inc = (
    """INSERT INTO opunit(
        id,name,created_by,created_on,
        last_modified_by,last_modified_on)
    VALUES
    (
        '352dd997-734e-4865-b4ff-f27e5d29006a',
        'Seinthon Inc',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         '2019-02-22T17:31:33+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         '2019-02-22T17:31:33+05:30'
    )""",
)
# password is 'password'
simple_user_without_any_access = (
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                null,
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
)

# password is 'password'
simple_user_with_create_org_access = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'Zonal Managger',
            'Zonal Managger role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','8ec5842d-4c2e-45b9-8e1e-e2fb4aa6957a')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
)

# password is 'password'
app_admin_user = ("""INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '7689345621',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """, )

opunits = ("""INSERT INTO opunit(id,name,parent_opunit,org_id,
              created_by,created_on,last_modified_by,last_modified_on)
              VALUES
              ('6fecedb9-506a-4359-95c7-094bccf1d6b2','Greene-Rocha',NULL,NULL,
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 11:38:45+05:30',
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 11:38:45+05:30'),

              ('bc7379f8-2fa5-4e6c-aee0-19a048e4cdf3','Hensley PLC',NULL,NULL,
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 11:30:45+05:30',
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 11:30:45+05:30'),

               ('8f3bfdfa-dcfb-4d33-8a14-d546da753e48','Trujillo-Wolf',NULL,NULL,
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 11:35:45+05:30',
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 11:35:45+05:30'),

               ('e05b4741-8691-4f62-adfd-6c894d05e53e','Ruiz-Shaffer',NULL,NULL,
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 12:13:45+05:30',
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 12:13:45+05:30'),

               ('b862cbef-f156-49a3-98a6-402ac2014fc1','Velazquez and Sons',NULL,NULL,
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 10:00:45+05:30',
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 10:25:45+05:30'),

               ('e18bff7b-d9d8-49b5-bb05-f3253d69594a','North Zone','b862cbef-f156-49a3-98a6-402ac2014fc1',
               'b862cbef-f156-49a3-98a6-402ac2014fc1',
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 11:38:45+05:30',
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 11:40:45+05:30'),
               
               ('335fb751-1f43-494c-b336-53a8b03b8c80','South Zone','b862cbef-f156-49a3-98a6-402ac2014fc1',
               'b862cbef-f156-49a3-98a6-402ac2014fc1',
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 11:38:45+05:30',
               '93ee912b-d051-46cc-83bb-988c6a44459f','2019-02-18 11:45:45+05:30')
              """,)

simple_user_with_update_org_access = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'Zonal Managger',
            'Zonal Managger role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','9033f511-56b4-4f6f-baaa-2151f81ae3f7')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
)

org_ortega_inc_admin_user = (
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NULL,
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
    """INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','"""+ADMIN_ROLE_ID+"""')
    """)
