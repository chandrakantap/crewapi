from tests.ext import DatabaseSetup, AUDIT_PAYLOAD_AUTO_GEN_ATTR
import datasetup_organisation as datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.post(
        resource_uri.get('organisations'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.simple_user_without_any_access)
def test_error_if_user_dont_have_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(resource_uri.get('organisations'), json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.simple_user_with_create_org_access)
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(resource_uri.get('organisations'), json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.simple_user_with_create_org_access + datasetup.org_ortega_inc)
def test_unique_organisation_name_case_insensitive(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(resource_uri.get('organisations'), json={
        'name': 'orTega inc'
    })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.simple_user_with_create_org_access)
def test_successful_org_creation_when_user_have_access(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.post(resource_uri.get('organisations'), json={
        'name': 'Wofied Oreatra'
    }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_successful_org_creation_when_user_is_app_admin(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="7689345621",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.post(resource_uri.get('organisations'), json={
        'name': 'Wofied Oreatra'
    }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)
