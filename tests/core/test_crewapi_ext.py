from crewapi.ext import get_indexes_with_duplicate


def get_indexes_with_duplicate_plain_array():
    source_array = [5, 7, 8, 3, 4, 9, 10, 23, "4", 34, "10", 7]
    duplicates = get_indexes_with_duplicate(source_array)
    print(duplicates)
