from tests.ext import DatabaseSetup, AUDIT_PAYLOAD_AUTO_GEN_ATTR
import datasetup_opunit as datasetup


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc)
def test_user_must_be_logged_in(db_connection, client, resource_uri, snapshot):
    response = client.post(
        resource_uri.get('opunits'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc + datasetup.simple_user_without_create_opunit_access)
def test_user_must_have_create_opunit_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.post(
        resource_uri.get('opunits'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc + datasetup.simple_user_with_create_opunit_access)
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.post(
        resource_uri.get('opunits'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_create_opunit_access +
                              datasetup.soulful_mall_opunit))
def test_unique_opunit_name(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('opunits'),
        json={
            'name': 'Soulful Mall',
            'parentOpunit': '165fa6ab-1893-4108-b88c-a30dfbdc88de'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_create_opunit_access))
def test_opunit_name_should_not_be_same_as_org_name(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('opunits'),
        json={
            'name': 'Ortega Inc',
            'parentOpunit': '165fa6ab-1893-4108-b88c-a30dfbdc88de'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_create_opunit_access +
                              datasetup.org_seinthon_inc))
def test_opunit_name_can_be_same_as_other_org_name(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.post(
        resource_uri.get('opunits'),
        json={
            'name': 'Seinthon Inc',
            'parentOpunit': '165fa6ab-1893-4108-b88c-a30dfbdc88de'
        }).json

    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_create_opunit_access))
def test_error_when_parent_opunit_not_exist(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('opunits'),
        json={
            'name': 'Elate Bar',
            'parentOpunit': '1d9a9e38-2b6a-406c-8a86-cd5c989d749e'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_create_opunit_access +
                              datasetup.org_seinthon_inc))
def test_error_when_parent_opunit_is_not_part_of_org(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('opunits'),
        json={
            'name': 'Elate Bar',
            'parentOpunit': '352dd997-734e-4865-b4ff-f27e5d29006a'
        })
    snapshot.assert_match(response.json)
