org_ortega_inc = (
    """INSERT INTO opunit(
        id,name,created_by,created_on,
        last_modified_by,last_modified_on)
    VALUES
    (
        '165fa6ab-1893-4108-b88c-a30dfbdc88de',
        'Ortega Inc',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         NOW(),
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         NOW()
    )""",
    """INSERT INTO visible_opunits(opunit_id,visible_opunit)
       VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de')""",
)

org_seinthon_inc = (
    """INSERT INTO opunit(
        id,name,created_by,created_on,
        last_modified_by,last_modified_on)
    VALUES
    (
        '352dd997-734e-4865-b4ff-f27e5d29006a',
        'Seinthon Inc',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         NOW(),
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         NOW()
    )""",
)

# password is 'password'
simple_user_without_create_opunit_access = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'Zonal Managger',
            'Zonal Managger role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','95105da3-4d0b-4de1-9443-9ae6caa3eb29')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                null,
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
    """INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','1c0dd6d5-6a11-4f91-82ef-e596730663ae')
    """
)

simple_user_without_view_opunit_permission = simple_user_without_create_opunit_access
simple_user_without_edit_opunit_access = simple_user_without_create_opunit_access

# password is 'password'
simple_user_with_create_opunit_access = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'Zonal Managger',
            'Zonal Managger role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','ab777e31-0e5f-4131-87ef-0f32955d1c28')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                null,
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
    """INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','1c0dd6d5-6a11-4f91-82ef-e596730663ae')
    """
)

# password is 'password'
simple_user_with_edit_opunit_access = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'Zonal Managger',
            'Zonal Managger role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','02675ea6-736e-4dee-b26f-286dec7c13e9')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                null,
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
    """INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','1c0dd6d5-6a11-4f91-82ef-e596730663ae')
    """
)

soulful_mall_opunit = (
    """INSERT INTO opunit(
        id,name,parent_opunit,org_id,created_by,created_on,
        last_modified_by,last_modified_on)
    VALUES
    (
        'aac9e91e-92e5-4049-8263-34ab96d383f8',
        'Soulful Mall',
        '165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '165fa6ab-1893-4108-b88c-a30dfbdc88de',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
        '2019-02-18 11:38:45+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         '2019-02-18 11:38:45+05:30'
    )
    """,
)

water_owens_opunit = (
    """INSERT INTO opunit(
        id,name,parent_opunit,org_id,created_by,created_on,
        last_modified_by,last_modified_on)
    VALUES
    (
        '0970063a-c126-4212-b6d8-74fa5fcec9f0',
        'Waters Owens',
        '165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '165fa6ab-1893-4108-b88c-a30dfbdc88de',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         '2019-02-18 11:38:45+05:30',
        'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
         '2019-02-18 11:38:45+05:30'
    )
    """,
)

simple_user_with_get_opunit_access = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'Zonal Managger',
            'Zonal Managger role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','d6cab86e-af33-445f-b3c7-74bf98ee75d7')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                null,
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
    """INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','1c0dd6d5-6a11-4f91-82ef-e596730663ae')
    """
)

"""
Mer%wik LLC
 |- Webb Johnston
 |- Martinez PLC
 |- Duncan Silva

Mer%quil LLC
 |- Ramirez Richardson
 |- Robinson LLC
 |- Moore Ltd
 |- Gibson PLC
 |- Franco Hart
 |- Walker Mcdaniel

Vasquez Scott
"""
opunits = (
    """INSERT INTO visible_opunits(opunit_id,visible_opunit)
       VALUES('165fa6ab-1893-4108-b88c-a30dfbdc88de','e79e176e-5227-4b86-a308-e2494051c38f'),
       ('9634e013-a77f-49ab-92e7-d31b819bd7eb','9634e013-a77f-49ab-92e7-d31b819bd7eb')
       """,
    """INSERT INTO opunit(
        id,name,
        parent_opunit,org_id,
        created_by,created_on,last_modified_by,last_modified_on)
       VALUES
       ('d54b6efc-955d-4017-ab8e-e424c7241884','Mer%wik LLC',
       '165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:23:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 10:45:34+05:30'),

       ('bf034897-cfa4-4b21-bbbf-9acdb3573eae','Webb Johnston',
       'd54b6efc-955d-4017-ab8e-e424c7241884','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 11:13:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 11:19:34+05:30'),

       ('ce2a5208-721d-4328-bfbe-257752df46b5','Martinez PLC',
       'd54b6efc-955d-4017-ab8e-e424c7241884','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 12:41:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 12:41:34+05:30'),

       ('1988ea20-236a-4baa-8be5-526a9131f81a','Duncan Silva',
       'd54b6efc-955d-4017-ab8e-e424c7241884','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 15:41:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-19 15:41:34+05:30'),

       ('e79e176e-5227-4b86-a308-e2494051c38f','Mer%quil LLC',
       '165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 10:41:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 10:41:34+05:30'),

       ('d40beca1-e379-491c-80ad-f138cf890c2a','Ramirez Richardson',
       'e79e176e-5227-4b86-a308-e2494051c38f','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 11:41:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 11:41:34+05:30'),

       ('9634e013-a77f-49ab-92e7-d31b819bd7eb','Robinson LLC',
       'e79e176e-5227-4b86-a308-e2494051c38f','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 12:41:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 12:41:34+05:30'),

       ('7a571df5-9192-4628-bba9-a72614af691f','Moore Ltd',
       'e79e176e-5227-4b86-a308-e2494051c38f','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 13:41:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 13:41:34+05:30'),

       ('ec17ded7-2487-4759-96ef-6d758f1eb2ab','Gibson PLC',
       'e79e176e-5227-4b86-a308-e2494051c38f','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 14:41:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 14:41:34+05:30'),

       ('b20a0346-0910-4e7b-abe8-f3baad0e7ebb','Franco Hart',
       'e79e176e-5227-4b86-a308-e2494051c38f','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 15:41:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 15:41:34+05:30'),

       ('ab8ef1a5-846e-4a64-bbb4-314997536565','Walker Mcdaniel',
       'e79e176e-5227-4b86-a308-e2494051c38f','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 16:41:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-20 16:41:34+05:30'),

       ('91226388-017b-4254-8b3f-0059b7a20ce8','Vasquez Scott',
       '165fa6ab-1893-4108-b88c-a30dfbdc88de','165fa6ab-1893-4108-b88c-a30dfbdc88de',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-21 17:41:34+05:30',
       'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a','2019-02-21 17:41:34+05:30')
    """,)

simple_user_of_Robinson_LLC = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'Zonal Managger',
            'Zonal Managger role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','d6cab86e-af33-445f-b3c7-74bf98ee75d7')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                null,
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """,
    """INSERT INTO opunit_user(opunit_id,org_id,user_id,role_id)
        VALUES('9634e013-a77f-49ab-92e7-d31b819bd7eb','165fa6ab-1893-4108-b88c-a30dfbdc88de',
        '2e7a6f90-46ac-4d1b-b308-688b37005a97','1c0dd6d5-6a11-4f91-82ef-e596730663ae')
    """
)
