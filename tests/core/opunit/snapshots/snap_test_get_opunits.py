# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_user_must_have_view_opunit_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_empty_result_when_no_data 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 0,
        'curPageEnd': 0,
        'curPageStart': 0,
        'items': [
        ],
        'totalCount': 0,
        'totalPages': 0
    },
    'status': 2000
}

snapshots['test_success_with_proper_response 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 1,
        'curPageEnd': 3,
        'curPageStart': 1,
        'items': [
            {
                'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'createdOn': '2019-02-21T17:41:34+05:30',
                'description': None,
                'emailId': None,
                'id': '91226388-017b-4254-8b3f-0059b7a20ce8',
                'lastModifiedBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'lastModifiedOn': '2019-02-21T17:41:34+05:30',
                'name': 'Vasquez Scott',
                'officeAddress': None,
                'parentOpunit': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
                'phoneNo': None
            },
            {
                'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'createdOn': '2019-02-20T10:41:34+05:30',
                'description': None,
                'emailId': None,
                'id': 'e79e176e-5227-4b86-a308-e2494051c38f',
                'lastModifiedBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'lastModifiedOn': '2019-02-20T10:41:34+05:30',
                'name': 'Mer%quil LLC',
                'officeAddress': None,
                'parentOpunit': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
                'phoneNo': None
            },
            {
                'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'createdOn': '2019-02-19T10:23:34+05:30',
                'description': None,
                'emailId': None,
                'id': 'd54b6efc-955d-4017-ab8e-e424c7241884',
                'lastModifiedBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'lastModifiedOn': '2019-02-19T10:45:34+05:30',
                'name': 'Mer%wik LLC',
                'officeAddress': None,
                'parentOpunit': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
                'phoneNo': None
            }
        ],
        'totalCount': 3,
        'totalPages': 1
    },
    'status': 2000
}

snapshots['test_success_when_name_starts_with_filter_applied 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 1,
        'curPageEnd': 2,
        'curPageStart': 1,
        'items': [
            {
                'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'createdOn': '2019-02-20T12:41:34+05:30',
                'description': None,
                'emailId': None,
                'id': '9634e013-a77f-49ab-92e7-d31b819bd7eb',
                'lastModifiedBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'lastModifiedOn': '2019-02-20T12:41:34+05:30',
                'name': 'Robinson LLC',
                'officeAddress': None,
                'parentOpunit': 'e79e176e-5227-4b86-a308-e2494051c38f',
                'phoneNo': None
            },
            {
                'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'createdOn': '2019-02-20T11:41:34+05:30',
                'description': None,
                'emailId': None,
                'id': 'd40beca1-e379-491c-80ad-f138cf890c2a',
                'lastModifiedBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'lastModifiedOn': '2019-02-20T11:41:34+05:30',
                'name': 'Ramirez Richardson',
                'officeAddress': None,
                'parentOpunit': 'e79e176e-5227-4b86-a308-e2494051c38f',
                'phoneNo': None
            }
        ],
        'totalCount': 2,
        'totalPages': 1
    },
    'status': 2000
}

snapshots['test_name_starts_with_filter_must_escape_wild_chars 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 1,
        'curPageEnd': 2,
        'curPageStart': 1,
        'items': [
            {
                'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'createdOn': '2019-02-20T10:41:34+05:30',
                'description': None,
                'emailId': None,
                'id': 'e79e176e-5227-4b86-a308-e2494051c38f',
                'lastModifiedBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'lastModifiedOn': '2019-02-20T10:41:34+05:30',
                'name': 'Mer%quil LLC',
                'officeAddress': None,
                'parentOpunit': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
                'phoneNo': None
            },
            {
                'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'createdOn': '2019-02-19T10:23:34+05:30',
                'description': None,
                'emailId': None,
                'id': 'd54b6efc-955d-4017-ab8e-e424c7241884',
                'lastModifiedBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                'lastModifiedOn': '2019-02-19T10:45:34+05:30',
                'name': 'Mer%wik LLC',
                'officeAddress': None,
                'parentOpunit': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
                'phoneNo': None
            }
        ],
        'totalCount': 2,
        'totalPages': 1
    },
    'status': 2000
}

snapshots['test_error_when_parent_opunit_is_not_visible 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}
