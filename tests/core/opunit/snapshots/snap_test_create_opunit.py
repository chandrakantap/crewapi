# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_user_must_have_create_opunit_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'name': 'Please provide operational unit name',
        'parentOpunit': 'Please provide parent operational unit'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_unique_opunit_name 1'] = {
    'errors': {
        'name': 'An operational unit with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_opunit_name_should_not_be_same_as_org_name 1'] = {
    'errors': {
        'name': 'An operational unit with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_error_when_parent_opunit_not_exist 1'] = {
    'errors': {
        'parentOpunit': 'Unable to find parent operational unit.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_error_when_parent_opunit_is_not_part_of_org 1'] = {
    'errors': {
        'parentOpunit': 'Unable to find parent operational unit.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_opunit_name_can_be_same_as_other_org_name 1'] = {
    'message': 'Successfully created operational unit.',
    'payload': {
        'createdBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'createdOn': 'NOW()',
        'description': None,
        'emailId': None,
        'id': 'AUTO_GEN',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'NOW()',
        'name': 'Seinthon Inc',
        'officeAddress': None,
        'parentOpunit': '165fa6ab-1893-4108-b88c-a30dfbdc88de',
        'phoneNo': None
    },
    'status': 2000
}
