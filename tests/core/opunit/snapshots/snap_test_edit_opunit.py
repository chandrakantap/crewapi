# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_user_must_have_edit_opunit_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'name': 'Please provide operational unit name'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_unique_opunit_name 1'] = {
    'errors': {
        'name': 'An operational unit with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_opunit_name_should_not_be_same_as_org_name 1'] = {
    'errors': {
        'name': 'An operational unit with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_opunit_name_can_be_same_as_other_org_name 1'] = {
    'message': 'Successfully updated operational unit.',
    'payload': {
        'createdBy': 'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
        'createdOn': '2019-02-18T11:38:45+05:30',
        'description': None,
        'emailId': None,
        'id': 'aac9e91e-92e5-4049-8263-34ab96d383f8',
        'lastModifiedBy': '2e7a6f90-46ac-4d1b-b308-688b37005a97',
        'lastModifiedOn': 'AUTO GENERATED',
        'name': 'Seinthon Inc',
        'officeAddress': None,
        'parentOpunit': None,
        'phoneNo': None
    },
    'status': 2000
}

snapshots['test_error_when_opunit_not_exist 1'] = {
    'message': 'No data found with provided organisation and operation unit.',
    'status': 4000
}
