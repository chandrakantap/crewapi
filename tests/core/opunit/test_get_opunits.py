from tests.ext import DatabaseSetup
import datasetup_opunit as datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.get(resource_uri.get('opunits'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc + datasetup.simple_user_without_view_opunit_permission)
def test_user_must_have_view_opunit_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.get(resource_uri.get('opunits'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc +
               datasetup.opunits +
               datasetup.simple_user_of_Robinson_LLC)
def test_error_when_parent_opunit_is_not_visible(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.get(resource_uri.get('opunits') +
                          "?parent_opunit_id=d40beca1-e379-491c-80ad-f138cf890c2a")
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc + datasetup.simple_user_with_get_opunit_access)
def test_empty_result_when_no_data(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.get(resource_uri.get('opunits'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_get_opunit_access +
                              datasetup.opunits))
def test_success_with_proper_response(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.get(resource_uri.get('opunits')).json
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_get_opunit_access +
                              datasetup.opunits))
def test_success_when_name_starts_with_filter_applied(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.get(resource_uri.get('opunits') +
                               "?nameStartsWith=R&parentOpunitId=e79e176e-5227-4b86-a308-e2494051c38f").json
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_get_opunit_access +
                              datasetup.opunits))
def test_name_starts_with_filter_must_escape_wild_chars(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.get(resource_uri.get('opunits') +
                               "?nameStartsWith=Mer%").json
    snapshot.assert_match(response_json)
