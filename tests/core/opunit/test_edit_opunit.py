from tests.ext import DatabaseSetup
import datasetup_opunit as datasetup


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc+datasetup.soulful_mall_opunit)
def test_user_must_be_logged_in(db_connection, client, resource_uri, snapshot):
    response = client.put(
        resource_uri.get('opunit').format('aac9e91e-92e5-4049-8263-34ab96d383f8'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_without_edit_opunit_access +
                              datasetup.soulful_mall_opunit))
def test_user_must_have_edit_opunit_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('opunit').format('aac9e91e-92e5-4049-8263-34ab96d383f8'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.org_ortega_inc + datasetup.simple_user_with_edit_opunit_access)
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.put(
        resource_uri.get('opunit').format('aac9e91e-92e5-4049-8263-34ab96d383f8'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_edit_opunit_access +
                              datasetup.soulful_mall_opunit +
                              datasetup.water_owens_opunit))
def test_unique_opunit_name(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.put(
        resource_uri.get('opunit').format('aac9e91e-92e5-4049-8263-34ab96d383f8'),
        json={
            'name': 'Waters Owens'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_edit_opunit_access +
                              datasetup.soulful_mall_opunit))
def test_opunit_name_should_not_be_same_as_org_name(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.put(
        resource_uri.get('opunit').format('aac9e91e-92e5-4049-8263-34ab96d383f8'),
        json={
            'name': 'Ortega Inc'
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_edit_opunit_access +
                              datasetup.soulful_mall_opunit +
                              datasetup.org_seinthon_inc))
def test_opunit_name_can_be_same_as_other_org_name(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.put(
        resource_uri.get('opunit').format('aac9e91e-92e5-4049-8263-34ab96d383f8'),
        json={
            'name': 'Seinthon Inc'
        }).json

    response_payload = response_json.get("payload", {})

    response_payload["lastModifiedOn"] = "AUTO GENERATED"
    response_json["payload"] = response_payload

    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=(datasetup.org_ortega_inc +
                              datasetup.simple_user_with_edit_opunit_access))
def test_error_when_opunit_not_exist(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.put(
        resource_uri.get('opunit').format('352dd997-734e-4865-b4ff-f27e5d29006a'),
        json={
            'name': 'Elate Bar'
        })
    snapshot.assert_match(response.json)
