# password is 'password'
simple_user_without_manage_app_role_permission = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'Sales Managger',
            'Sales Managger role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','d6cab86e-af33-445f-b3c7-74bf98ee75d7')
    """,
    """INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """
)

simple_user_without_view_app_role_permission = simple_user_without_manage_app_role_permission

# password is 'password'
app_admin_user = ("""INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                '2e7a6f90-46ac-4d1b-b308-688b37005a97',
                '9888898888',
                '+32',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'Mary Barry',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    """, )

asset_maintainer_app_role = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
            'Asset Maintainer',
            'Asset Maintainer role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
)


app_roles = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','Sales Manager','Sales Manager role','93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2013-07-27 00:50:00+05:30',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2013-07-27 00:50:00+05:30'
        ),
        ('b5ecc917-3e73-4175-84c6-d746a58ff539','Retail manager','Retail manager role','93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2000-04-17 22:35:10+05:30',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2000-04-17 22:35:10+05:30'
        ),
        ('3309ac16-4e14-4473-a653-3cc16b6a3f83','Recycling officer','Recycling officer','93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2015-11-12 16:49:40+05:30',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2015-11-12 16:49:40+05:30'
        ),
        ('d0061f73-206f-4d9f-81d4-da5f56f733fc','Office manager','Office manager','93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2000-09-25 22:36:13+05:30',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2000-09-25 22:36:13+05:30'
        ),
        ('6dd070e4-ad66-4901-b305-f1ea9d4d733b','Insurance account manager','Insurance account manager',
        '93ee912b-d051-46cc-83bb-988c6a44459f',
        '93ee912b-d051-46cc-83bb-988c6a44459f','2008-07-09 16:00:28+05:30',
        '93ee912b-d051-46cc-83bb-988c6a44459f','2008-07-09 16:00:28+05:30'
        ),
        ('61c6e605-580b-411e-829b-77a4ce5ede24','Lighting technician, broadcasting/film/video',
        'Lighting technician, broadcasting/film/video',
        '93ee912b-d051-46cc-83bb-988c6a44459f',
        '93ee912b-d051-46cc-83bb-988c6a44459f','2005-12-20 08:32:42+05:30',
        '93ee912b-d051-46cc-83bb-988c6a44459f','2005-12-20 08:32:42+05:30'
        )
        """,
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','d6cab86e-af33-445f-b3c7-74bf98ee75d7'),
        ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','316352f3-25fc-4881-9a62-370a1e7dc84b'),
        ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','d9a5fe2c-e08a-4dca-bc25-09b2c3da4ae1'),
        ('b5ecc917-3e73-4175-84c6-d746a58ff539','fb3cd7ca-908a-4071-b62f-70d72a4f906f'),
        ('b5ecc917-3e73-4175-84c6-d746a58ff539','95105da3-4d0b-4de1-9443-9ae6caa3eb29'),
        ('3309ac16-4e14-4473-a653-3cc16b6a3f83','c2ef5f10-5a33-4c6d-88dc-0dd2b42d1729'),
        ('3309ac16-4e14-4473-a653-3cc16b6a3f83','9033f511-56b4-4f6f-baaa-2151f81ae3f7'),
        ('3309ac16-4e14-4473-a653-3cc16b6a3f83','ad6bbf0b-889d-4071-98b8-0a8453fd655c'),
        ('3309ac16-4e14-4473-a653-3cc16b6a3f83','8ec5842d-4c2e-45b9-8e1e-e2fb4aa6957a'),
        ('d0061f73-206f-4d9f-81d4-da5f56f733fc','316352f3-25fc-4881-9a62-370a1e7dc84b'),
        ('6dd070e4-ad66-4901-b305-f1ea9d4d733b','83eb58b2-60a3-4ef3-85b7-db9580da5a2f'),
        ('61c6e605-580b-411e-829b-77a4ce5ede24','fb3cd7ca-908a-4071-b62f-70d72a4f906f'),
        ('61c6e605-580b-411e-829b-77a4ce5ede24','baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc')
    """,
    """INSERT INTO role_grant_role(role_id,can_grant_role)
        VALUES('1c0dd6d5-6a11-4f91-82ef-e596730663ae','b5ecc917-3e73-4175-84c6-d746a58ff539'),
        ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','d0061f73-206f-4d9f-81d4-da5f56f733fc'),
        ('1c0dd6d5-6a11-4f91-82ef-e596730663ae','6dd070e4-ad66-4901-b305-f1ea9d4d733b'),
        ('3309ac16-4e14-4473-a653-3cc16b6a3f83','61c6e605-580b-411e-829b-77a4ce5ede24'),
        ('6dd070e4-ad66-4901-b305-f1ea9d4d733b','61c6e605-580b-411e-829b-77a4ce5ede24')
    """
)

app_role_with_special_char = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        ('b5ecc917-3e73-4175-84c6-d746a58ff539','Re%tail manager','Retail manager role','93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2011-07-11 09:45:18+05:30',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2011-07-11 09:45:18+05:30'
        ),
        ('3309ac16-4e14-4473-a653-3cc16b6a3f83','Re%wycling officer','Recycling officer','93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2015-05-13 05:33:42+05:30',
            '93ee912b-d051-46cc-83bb-988c6a44459f','2015-05-13 05:33:42+05:30'
        )
        """,
    """INSERT INTO role_has_permission(role_id,permission_id)
        VALUES ('b5ecc917-3e73-4175-84c6-d746a58ff539','fb3cd7ca-908a-4071-b62f-70d72a4f906f'),
        ('b5ecc917-3e73-4175-84c6-d746a58ff539','95105da3-4d0b-4de1-9443-9ae6caa3eb29'),
        ('3309ac16-4e14-4473-a653-3cc16b6a3f83','c2ef5f10-5a33-4c6d-88dc-0dd2b42d1729'),
        ('3309ac16-4e14-4473-a653-3cc16b6a3f83','9033f511-56b4-4f6f-baaa-2151f81ae3f7'),
        ('3309ac16-4e14-4473-a653-3cc16b6a3f83','ad6bbf0b-889d-4071-98b8-0a8453fd655c')
    """
)


store_maintainer_app_role = (
    """INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            'b5ecc917-3e73-4175-84c6-d746a58ff539',
            'Store Maintainer',
            'Store Maintainer role',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )""",
)
