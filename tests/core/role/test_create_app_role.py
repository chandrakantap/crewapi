from tests.ext import DatabaseSetup, AUDIT_PAYLOAD_AUTO_GEN_ATTR
import datasetup_role as datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.post(
        resource_uri.get('app_roles'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.simple_user_without_manage_app_role_permission)
def test_user_must_have_manage_role_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.post(
        resource_uri.get('app_roles'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_roles'),
        json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_minimum_one_permission_required(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_roles'),
        json={
            'name': 'Asset Maintainer',
            'permissions': []
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_error_when_role_name_is_admin(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_roles'),
        json={
            'name': 'Admin',
            'permissions': ['baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc']
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_error_when_role_name_is_administrator(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_roles'),
        json={
            'name': 'AdministraTor',
            'permissions': ['baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc']
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user+datasetup.asset_maintainer_app_role)
def test_unique_role_name(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.post(
        resource_uri.get('app_roles'),
        json={
            'name': 'ASSET MAINTAINER  ',
            'permissions': ['baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc']
        })
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_success_when_user_is_app_admin(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.post(
        resource_uri.get('app_roles'),
        json={
            'name': 'ASSET MAINTAINER  ',
            'permissions': ['baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc']
        }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user+datasetup.asset_maintainer_app_role)
def test_success_when_role_can_grant_roles(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.post(
        resource_uri.get('app_roles'),
        json={
            'name': 'Store Maintainer',
            'description': 'Store Maintainer role',
            'permissions': ['baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc',
                            'fb3cd7ca-908a-4071-b62f-70d72a4f906f',
                            'd9a5fe2c-e08a-4dca-bc25-09b2c3da4ae1'],
            'canGrantRoles': ['1c0dd6d5-6a11-4f91-82ef-e596730663ae']
        }).json
    response_json['payload'] = {**response_json.get('payload'), **AUDIT_PAYLOAD_AUTO_GEN_ATTR}
    snapshot.assert_match(response_json)
