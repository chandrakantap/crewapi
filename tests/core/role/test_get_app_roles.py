from tests.ext import DatabaseSetup
import datasetup_role as datasetup


def test_user_must_be_logged_in(client, resource_uri, snapshot):
    response = client.get(resource_uri.get('app_roles'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.simple_user_without_view_app_role_permission)
def test_user_must_have_view_role_permission(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")
    response = client.get(resource_uri.get('app_roles'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user)
def test_empty_result_when_no_data(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response = client.get(resource_uri.get('app_roles'))
    snapshot.assert_match(response.json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user + datasetup.app_roles)
def test_success_with_proper_response(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.get(resource_uri.get('app_roles')).json
    _sort_items(response_json)
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user + datasetup.app_roles)
def test_success_when_name_starts_with_filter_applied(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.get(resource_uri.get('app_roles')+"?nameStartsWith=Re").json
    _sort_items(response_json)
    snapshot.assert_match(response_json)


@DatabaseSetup(setup_queries=datasetup.app_admin_user + datasetup.app_role_with_special_char)
def test_name_starts_with_filter_must_escape_wild_chars(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888",
          mobileNoCountryCode="+32",
          password="password")

    response_json = client.get(resource_uri.get('app_roles')+"?nameStartsWith=Re%").json
    _sort_items(response_json)
    snapshot.assert_match(response_json)


def _sort_items(response_json):
    items = response_json.get('payload').get('items')
    for item in items:
        item['permissions'] = sorted(item['permissions'], key=lambda k: k['name'])
        item['canGrantRoles'] = sorted(item['canGrantRoles'], key=lambda k: k['name'])
