# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_empty_result_when_no_data 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 0,
        'curPageEnd': 0,
        'curPageStart': 0,
        'items': [
        ],
        'totalCount': 0,
        'totalPages': 0
    },
    'status': 2000
}

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_user_must_have_view_role_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_success_with_proper_response 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 1,
        'curPageEnd': 6,
        'curPageStart': 1,
        'items': [
            {
                'canGrantRoles': [
                    {
                        'id': '6dd070e4-ad66-4901-b305-f1ea9d4d733b',
                        'name': 'Insurance account manager'
                    },
                    {
                        'id': 'd0061f73-206f-4d9f-81d4-da5f56f733fc',
                        'name': 'Office manager'
                    },
                    {
                        'id': 'b5ecc917-3e73-4175-84c6-d746a58ff539',
                        'name': 'Retail manager'
                    }
                ],
                'description': 'Sales Manager role',
                'id': '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
                'name': 'Sales Manager',
                'permissions': [
                    {
                        'description': 'Can create a new user.',
                        'id': '316352f3-25fc-4881-9a62-370a1e7dc84b',
                        'name': 'Create',
                        'resourceName': 'User'
                    },
                    {
                        'description': 'Can modify mobile no and reset password for the user having role which current user can grant.',
                        'id': 'd9a5fe2c-e08a-4dca-bc25-09b2c3da4ae1',
                        'name': 'Reset Credentials',
                        'resourceName': 'User'
                    },
                    {
                        'description': 'Can view operational units under the current opunit',
                        'id': 'd6cab86e-af33-445f-b3c7-74bf98ee75d7',
                        'name': 'View',
                        'resourceName': 'Opunit'
                    }
                ]
            },
            {
                'canGrantRoles': [
                ],
                'description': 'Retail manager role',
                'id': 'b5ecc917-3e73-4175-84c6-d746a58ff539',
                'name': 'Retail manager',
                'permissions': [
                    {
                        'description': 'Can modify user profile,for the user having role which current user can grant,without mobileno and password.',
                        'id': 'fb3cd7ca-908a-4071-b62f-70d72a4f906f',
                        'name': 'Edit Profile',
                        'resourceName': 'User'
                    },
                    {
                        'description': 'Remove an user access from app.',
                        'id': '95105da3-4d0b-4de1-9443-9ae6caa3eb29',
                        'name': 'Remove',
                        'resourceName': 'User'
                    }
                ]
            },
            {
                'canGrantRoles': [
                    {
                        'id': '61c6e605-580b-411e-829b-77a4ce5ede24',
                        'name': 'Lighting technician, broadcasting/film/video'
                    }
                ],
                'description': 'Recycling officer',
                'id': '3309ac16-4e14-4473-a653-3cc16b6a3f83',
                'name': 'Recycling officer',
                'permissions': [
                    {
                        'description': 'Can create a new organisation.',
                        'id': '8ec5842d-4c2e-45b9-8e1e-e2fb4aa6957a',
                        'name': 'Create',
                        'resourceName': 'Organisation'
                    },
                    {
                        'description': 'Deactivate organisations',
                        'id': 'c2ef5f10-5a33-4c6d-88dc-0dd2b42d1729',
                        'name': 'Deactivate',
                        'resourceName': 'Organisation'
                    },
                    {
                        'description': 'Can modify organisation details.',
                        'id': '9033f511-56b4-4f6f-baaa-2151f81ae3f7',
                        'name': 'Edit',
                        'resourceName': 'Organisation'
                    },
                    {
                        'description': 'Can view organisations registered.',
                        'id': 'ad6bbf0b-889d-4071-98b8-0a8453fd655c',
                        'name': 'View',
                        'resourceName': 'Organisation'
                    }
                ]
            },
            {
                'canGrantRoles': [
                ],
                'description': 'Office manager',
                'id': 'd0061f73-206f-4d9f-81d4-da5f56f733fc',
                'name': 'Office manager',
                'permissions': [
                    {
                        'description': 'Can create a new user.',
                        'id': '316352f3-25fc-4881-9a62-370a1e7dc84b',
                        'name': 'Create',
                        'resourceName': 'User'
                    }
                ]
            },
            {
                'canGrantRoles': [
                    {
                        'id': '61c6e605-580b-411e-829b-77a4ce5ede24',
                        'name': 'Lighting technician, broadcasting/film/video'
                    }
                ],
                'description': 'Insurance account manager',
                'id': '6dd070e4-ad66-4901-b305-f1ea9d4d733b',
                'name': 'Insurance account manager',
                'permissions': [
                    {
                        'description': 'Can see list of users and details of the user.User having role which the current user can not grant, are excluded.',
                        'id': '83eb58b2-60a3-4ef3-85b7-db9580da5a2f',
                        'name': 'View',
                        'resourceName': 'User'
                    }
                ]
            },
            {
                'canGrantRoles': [
                ],
                'description': 'Lighting technician, broadcasting/film/video',
                'id': '61c6e605-580b-411e-829b-77a4ce5ede24',
                'name': 'Lighting technician, broadcasting/film/video',
                'permissions': [
                    {
                        'description': 'Can modify user profile,for the user having role which current user can grant,without mobileno and password.',
                        'id': 'fb3cd7ca-908a-4071-b62f-70d72a4f906f',
                        'name': 'Edit Profile',
                        'resourceName': 'User'
                    },
                    {
                        'description': 'View roles and permissions.',
                        'id': 'baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc',
                        'name': 'View',
                        'resourceName': 'Role'
                    }
                ]
            }
        ],
        'totalCount': 6,
        'totalPages': 1
    },
    'status': 2000
}

snapshots['test_success_when_name_starts_with_filter_applied 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 1,
        'curPageEnd': 2,
        'curPageStart': 1,
        'items': [
            {
                'canGrantRoles': [
                ],
                'description': 'Retail manager role',
                'id': 'b5ecc917-3e73-4175-84c6-d746a58ff539',
                'name': 'Retail manager',
                'permissions': [
                    {
                        'description': 'Can modify user profile,for the user having role which current user can grant,without mobileno and password.',
                        'id': 'fb3cd7ca-908a-4071-b62f-70d72a4f906f',
                        'name': 'Edit Profile',
                        'resourceName': 'User'
                    },
                    {
                        'description': 'Remove an user access from app.',
                        'id': '95105da3-4d0b-4de1-9443-9ae6caa3eb29',
                        'name': 'Remove',
                        'resourceName': 'User'
                    }
                ]
            },
            {
                'canGrantRoles': [
                    {
                        'id': '61c6e605-580b-411e-829b-77a4ce5ede24',
                        'name': 'Lighting technician, broadcasting/film/video'
                    }
                ],
                'description': 'Recycling officer',
                'id': '3309ac16-4e14-4473-a653-3cc16b6a3f83',
                'name': 'Recycling officer',
                'permissions': [
                    {
                        'description': 'Can create a new organisation.',
                        'id': '8ec5842d-4c2e-45b9-8e1e-e2fb4aa6957a',
                        'name': 'Create',
                        'resourceName': 'Organisation'
                    },
                    {
                        'description': 'Deactivate organisations',
                        'id': 'c2ef5f10-5a33-4c6d-88dc-0dd2b42d1729',
                        'name': 'Deactivate',
                        'resourceName': 'Organisation'
                    },
                    {
                        'description': 'Can modify organisation details.',
                        'id': '9033f511-56b4-4f6f-baaa-2151f81ae3f7',
                        'name': 'Edit',
                        'resourceName': 'Organisation'
                    },
                    {
                        'description': 'Can view organisations registered.',
                        'id': 'ad6bbf0b-889d-4071-98b8-0a8453fd655c',
                        'name': 'View',
                        'resourceName': 'Organisation'
                    }
                ]
            }
        ],
        'totalCount': 2,
        'totalPages': 1
    },
    'status': 2000
}

snapshots['test_name_starts_with_filter_must_escape_wild_chars 1'] = {
    'message': 'Success',
    'payload': {
        'curPage': 1,
        'curPageEnd': 2,
        'curPageStart': 1,
        'items': [
            {
                'canGrantRoles': [
                ],
                'description': 'Retail manager role',
                'id': 'b5ecc917-3e73-4175-84c6-d746a58ff539',
                'name': 'Re%tail manager',
                'permissions': [
                    {
                        'description': 'Can modify user profile,for the user having role which current user can grant,without mobileno and password.',
                        'id': 'fb3cd7ca-908a-4071-b62f-70d72a4f906f',
                        'name': 'Edit Profile',
                        'resourceName': 'User'
                    },
                    {
                        'description': 'Remove an user access from app.',
                        'id': '95105da3-4d0b-4de1-9443-9ae6caa3eb29',
                        'name': 'Remove',
                        'resourceName': 'User'
                    }
                ]
            },
            {
                'canGrantRoles': [
                ],
                'description': 'Recycling officer',
                'id': '3309ac16-4e14-4473-a653-3cc16b6a3f83',
                'name': 'Re%wycling officer',
                'permissions': [
                    {
                        'description': 'Deactivate organisations',
                        'id': 'c2ef5f10-5a33-4c6d-88dc-0dd2b42d1729',
                        'name': 'Deactivate',
                        'resourceName': 'Organisation'
                    },
                    {
                        'description': 'Can modify organisation details.',
                        'id': '9033f511-56b4-4f6f-baaa-2151f81ae3f7',
                        'name': 'Edit',
                        'resourceName': 'Organisation'
                    },
                    {
                        'description': 'Can view organisations registered.',
                        'id': 'ad6bbf0b-889d-4071-98b8-0a8453fd655c',
                        'name': 'View',
                        'resourceName': 'Organisation'
                    }
                ]
            }
        ],
        'totalCount': 2,
        'totalPages': 1
    },
    'status': 2000
}
