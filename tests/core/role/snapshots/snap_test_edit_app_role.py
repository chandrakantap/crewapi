# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_must_have_manage_role_permission 1'] = {
    'message': 'You are not authorized to perform this action.',
    'status': 4030
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'name': 'Please provide role name',
        'permissions': 'At least one permission is required.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_minimum_one_permission_required 1'] = {
    'errors': {
        'permissions': 'At least one permission is required.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_error_when_role_name_is_admin 1'] = {
    'errors': {
        'name': 'Please choose a different role name than Admin'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_error_when_role_name_is_administrator 1'] = {
    'errors': {
        'name': 'Please choose a different role name than AdministraTor'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_unique_role_name 1'] = {
    'errors': {
        'name': 'A role with same name already exist.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_error_when_role_not_exist 1'] = {
    'message': 'No data found with provided app role id.',
    'status': 4000
}

snapshots['test_user_must_be_logged_in 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_success_when_user_is_app_admin 1'] = {
    'message': 'Successfully updated role.',
    'payload': {
        'canGrantRoles': [
        ],
        'createdOn': 'NOW()',
        'description': None,
        'id': 'AUTO_GEN',
        'lastModifiedOn': 'NOW()',
        'name': 'ASSET MAINTAINER',
        'permissions': [
            {
                'description': 'View roles and permissions.',
                'id': 'baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc',
                'name': 'View',
                'resourceName': 'Role'
            }
        ]
    },
    'status': 2000
}

snapshots['test_success_when_role_can_grant_roles 1'] = {
    'message': 'Successfully updated role.',
    'payload': {
        'canGrantRoles': [
            {
                'id': '1c0dd6d5-6a11-4f91-82ef-e596730663ae',
                'name': 'Asset Maintainer'
            }
        ],
        'createdOn': 'NOW()',
        'description': None,
        'id': 'AUTO_GEN',
        'lastModifiedOn': 'NOW()',
        'name': 'STORE MAINTAINER',
        'permissions': [
            {
                'description': 'Can modify user profile,for the user having role which current user can grant,without mobileno and password.',
                'id': 'fb3cd7ca-908a-4071-b62f-70d72a4f906f',
                'name': 'Edit Profile',
                'resourceName': 'User'
            },
            {
                'description': 'Can modify mobile no and reset password for the user having role which current user can grant.',
                'id': 'd9a5fe2c-e08a-4dca-bc25-09b2c3da4ae1',
                'name': 'Reset Credentials',
                'resourceName': 'User'
            },
            {
                'description': 'View roles and permissions.',
                'id': 'baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc',
                'name': 'View',
                'resourceName': 'Role'
            }
        ]
    },
    'status': 2000
}
