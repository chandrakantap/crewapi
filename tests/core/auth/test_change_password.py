from tests.ext import DatabaseSetup
import datasetup_auth as datasetup


@DatabaseSetup(datasetup.simple_user_without_any_access)
def test_user_need_to_login_first(db_connection, client, resource_uri, snapshot):
    response = client.put(resource_uri.get('password'), json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(datasetup.simple_user_without_any_access)
def test_request_validation(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888", mobileNoCountryCode="+32", password="password")
    response = client.put(resource_uri.get('password'), json={})
    snapshot.assert_match(response.json)


@DatabaseSetup(datasetup.simple_user_without_any_access)
def test_error_when_confirm_password_does_not_match(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888", mobileNoCountryCode="+32", password="password")
    response = client.put(resource_uri.get('password'), json={
        "currentPassword": "password",
        "newPassword": "new_password",
        "confirmPassword": "unmatched_confirmed_password"
    })
    snapshot.assert_match(response.json)


@DatabaseSetup(datasetup.simple_user_without_any_access)
def test_error_current_password_is_worng(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888", mobileNoCountryCode="+32", password="password")
    response = client.put(resource_uri.get('password'), json={
        "currentPassword": "wrong_password",
        "newPassword": "new_password",
        "confirmPassword": "new_password"
    })
    snapshot.assert_match(response.json)


@DatabaseSetup(datasetup.simple_user_without_any_access)
def test_change_password_success(db_connection, login, client, resource_uri, snapshot):
    login(mobileNo="9888898888", mobileNoCountryCode="+32", password="password")
    response = client.put(resource_uri.get('password'), json={
        "currentPassword": "password",
        "newPassword": "new_password",
        "confirmPassword": "new_password"
    })
    snapshot.assert_match(response.json)
