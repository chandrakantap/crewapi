# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot


snapshots = Snapshot()

snapshots['test_user_need_to_login_first 1'] = {
    'message': 'You need to login before performing this operation',
    'status': 4021
}

snapshots['test_error_current_password_is_worng 1'] = {
    'errors': {
        'currentPassword': 'Incorrect password'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_change_password_success 1'] = {
    'message': 'Password changed successfully',
    'status': 2000
}

snapshots['test_request_validation 1'] = {
    'errors': {
        'confirmPassword': 'Please confirm new password',
        'currentPassword': 'Please enter your current password',
        'newPassword': 'Please enter a new password'
    },
    'message': 'Invalid input received.',
    'status': 4000
}

snapshots['test_error_when_confirm_password_does_not_match 1'] = {
    'errors': {
        'confirmPassword': 'Password do not match.'
    },
    'message': 'Invalid input received.',
    'status': 4000
}
