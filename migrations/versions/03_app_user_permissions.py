"""empty message

Revision ID: 52e3919ccf46
Revises: 835292e5a2ad
Create Date: 2019-01-13 20:18:04.832245

SERIAL - 3
"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '52e3919ccf46'
down_revision = '835292e5a2ad'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute('''
        INSERT INTO permission(id,entity_name,resource_name,name,operation_name,description)
        VALUES
        (
            '316352f3-25fc-4881-9a62-370a1e7dc84b',
            'APP',
            'User',
            'Create',
            'create_user',
            'Can create a new user.'
        ),
        (
            '83eb58b2-60a3-4ef3-85b7-db9580da5a2f',
            'APP',
            'User',
            'View',
            'view_users',
            'Can see list of users and details of the user.User having role which the current user can not grant, are excluded.'
        ),
        (
            'fb3cd7ca-908a-4071-b62f-70d72a4f906f',
            'APP',
            'User',
            'Edit Profile',
            'edit_profile',
            'Can modify user profile,for the user having role which current user can grant,without mobileno and password.'
        ),
        (
            'd9a5fe2c-e08a-4dca-bc25-09b2c3da4ae1',
            'APP',
            'User',
            'Reset Credentials',
            'reset_credentials',
            'Can modify mobile no and reset password for the user having role which current user can grant.'
        ),
        (
            '95105da3-4d0b-4de1-9443-9ae6caa3eb29',
            'APP',
            'User',
            'Remove',
            'remove_user',
            'Remove an user access from app.'
        )
    ''')


def downgrade():
    conn = op.get_bind()
    conn.execute('''
        DELETE FROM permission WHERE id IN (
            '316352f3-25fc-4881-9a62-370a1e7dc84b',
            '83eb58b2-60a3-4ef3-85b7-db9580da5a2f',
            'fb3cd7ca-908a-4071-b62f-70d72a4f906f',
            'd9a5fe2c-e08a-4dca-bc25-09b2c3da4ae1',
            '95105da3-4d0b-4de1-9443-9ae6caa3eb29'
        )
    ''')
