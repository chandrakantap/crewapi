"""empty message

Revision ID: 7caae320fc2f
Revises: d414319de0d2
Create Date: 2019-02-05 18:42:10.105923

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '7caae320fc2f'
down_revision = 'd414319de0d2'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute('''
        INSERT INTO permission(id,entity_name,resource_name,name,operation_name,description)
        VALUES
        (
            '8c943641-9eaa-4060-9230-9b31fd2ae74c',
            'APP',
            'Role',
            'Manage',
            'manage_role',
            'Manager  roles.'
        ),
        (
            'baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc',
            'APP',
            'Role',
            'View',
            'view_roles',
            'View roles and permissions.'
        ),
        (
            'd15919c7-fb24-46f0-84f9-8c5b9ec5a84c',
            'ORG',
            'Role',
            'Manage',
            'manage_role',
            'Manager organisation roles.'
        ),
        (
            '0ce45e3b-b0cc-4ce3-8872-e8f4048381a0',
            'ORG',
            'Role',
            'View',
            'view_roles',
            'View Organisational roles and permissions.'
        )
    ''')


def downgrade():
    conn = op.get_bind()
    conn.execute('''
        DELETE FROM permission WHERE id IN (
            '8c943641-9eaa-4060-9230-9b31fd2ae74c',
            'baf7c5ea-39a0-459e-bd1f-aa4d2e31acfc',
            'd15919c7-fb24-46f0-84f9-8c5b9ec5a84c',
            '0ce45e3b-b0cc-4ce3-8872-e8f4048381a0'
        )
    ''')
