"""empty message

Revision ID: 6e87a5beb664
Revises: 4f0af846e459
Create Date: 2019-01-28 18:15:51.024429

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '6e87a5beb664'
down_revision = '4f0af846e459'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute('''
        INSERT INTO permission(id,entity_name,resource_name,name,operation_name,description)
        VALUES
        (
            '8ec5842d-4c2e-45b9-8e1e-e2fb4aa6957a',
            'APP',
            'Organisation',
            'Create',
            'create_org',
            'Can create a new organisation.'
        ),
        (
            'ad6bbf0b-889d-4071-98b8-0a8453fd655c',
            'APP',
            'Organisation',
            'View',
            'view_org',
            'Can view organisations registered.'
        ),
        (
            '9033f511-56b4-4f6f-baaa-2151f81ae3f7',
            'APP',
            'Organisation',
            'Edit',
            'edit_org_details',
            'Can modify organisation details.'
        ),
        (
            'a1041671-4341-4f5c-9208-9dbd5098e445',
            'ORG',
            'Organisation',
            'Edit',
            'edit_org_details',
            'Can modify organisation details.'
        ),
        (
            'c2ef5f10-5a33-4c6d-88dc-0dd2b42d1729',
            'APP',
            'Organisation',
            'Deactivate',
            'deactivate_org',
            'Deactivate organisations'
        )
    ''')


def downgrade():
    conn = op.get_bind()
    conn.execute('''
        DELETE FROM permission WHERE id IN (
            '8ec5842d-4c2e-45b9-8e1e-e2fb4aa6957a',
            'ad6bbf0b-889d-4071-98b8-0a8453fd655c',
            '9033f511-56b4-4f6f-baaa-2151f81ae3f7',
            'c2ef5f10-5a33-4c6d-88dc-0dd2b42d1729'
        )
    ''')
