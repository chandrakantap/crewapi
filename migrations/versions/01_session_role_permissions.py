"""empty message

Revision ID: 2e39434abca0
Revises:
Create Date: 2019-01-08 21:10:35.460847

SERIAL - 1
"""
from alembic import op

revision = '2e39434abca0'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute('''
        CREATE TABLE session (
            id UUID NOT NULL PRIMARY KEY,
            user_id UUID NOT NULL,
            user_name VARCHAR(256) NOT NULL,
            is_first_login BOOLEAN NOT NULL,
            app_role JSONB,
            organisation JSONB,
            organisation_role JSONB,
            accessible_org_count INTEGER NOT NULL DEFAULT 0,
            opunit JSONB,
            opunit_role JSONB,
            accessible_opunit_count INTEGER NOT NULL DEFAULT 0,
            permissions JSONB,
            is_active BOOLEAN NOT NULL DEFAULT FALSE,
            deactivate_reason VARCHAR(4096),
            created_on TIMESTAMP WITH TIME ZONE NOT NULL,
            deactivated_on TIMESTAMP WITH TIME ZONE
        )
    ''')
    conn.execute('''
        CREATE TABLE permission (
            id UUID NOT NULL PRIMARY KEY,
            entity_name VARCHAR(6) NOT NULL,
            resource_name VARCHAR(256) NOT NULL,
            name VARCHAR(256) NOT NULL,
            operation_name VARCHAR(256) NOT NULL,
            description VARCHAR(4096)
        )
    ''')

    conn.execute('''
     CREATE TABLE role (
         id UUID NOT NULL PRIMARY KEY,
         name VARCHAR(256) NOT NULL,
         description VARCHAR(4096),
         owner_id UUID NOT NULL,
         created_by UUID NOT NULL,
         created_on TIMESTAMP WITH TIME ZONE NOT NULL,
         last_modified_by UUID NOT NULL,
         last_modified_on TIMESTAMP WITH TIME ZONE NOT NULL
     )
    ''')

    conn.execute('''
        CREATE UNIQUE INDEX UNQ_role_name ON role (LOWER(name),owner_id)
    ''')

    conn.execute('''
        INSERT INTO role(id,name,description,owner_id,created_by,created_on,last_modified_by,last_modified_on)
        VALUES
        (
            '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
            'ADMIN',
            'Administrator role. User having this role can perform all the task.',
            'cd350095-dfde-4364-be14-9c2af94b2a93',
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW(),
            '93ee912b-d051-46cc-83bb-988c6a44459f',
             NOW()
        )
    ''')

    conn.execute('''
       CREATE TABLE role_has_permission(
           role_id UUID NOT NULL,
           permission_id UUID NOT NULL
       )
    ''')

    conn.execute('''
       CREATE UNIQUE INDEX UNQ_role_has_permission ON role_has_permission(role_id, permission_id)
    ''')

    conn.execute('''
       CREATE TABLE role_grant_role(
           role_id UUID NOT NULL,
           can_grant_role UUID NOT NULL
       )
    ''')

    conn.execute('''
       CREATE UNIQUE INDEX UNQ_role_grant_role ON role_grant_role(role_id, can_grant_role)
    ''')


def downgrade():
    conn = op.get_bind()
    conn.execute('DROP TABLE role_permission')
    conn.execute('DROP TABLE role_grant_role')
    conn.execute('DROP TABLE role')
    conn.execute('DROP TABLE permission')
    conn.execute('DROP TABLE session')
