"""empty message

Revision ID: 4f0af846e459
Revises: 52e3919ccf46
Create Date: 2019-01-08 21:12:16.259638

SERIAL - 4
"""
from alembic import op

revision = '4f0af846e459'
down_revision = '52e3919ccf46'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute('''
        CREATE TABLE opunit(
            id UUID NOT NULL PRIMARY KEY,
            name VARCHAR(256) NOT NULL,
            description VARCHAR(4096),
            office_address VARCHAR(4096),
            phone_no VARCHAR(256),
            email_id VARCHAR(256),
            parent_opunit UUID,
            org_id UUID,
            created_by UUID NOT NULL,
            created_on TIMESTAMP WITH TIME ZONE NOT NULL,
            last_modified_by UUID NOT NULL,
            last_modified_on TIMESTAMP WITH TIME ZONE NOT NULL
        )
    ''')

    conn.execute(
        'CREATE UNIQUE INDEX UNQ_organisation_name ON opunit (LOWER(name)) WHERE org_id IS NULL ')
    conn.execute(
        'CREATE UNIQUE INDEX UNQ_opunit_name ON opunit (LOWER(name),org_id) WHERE org_id IS NOT NULL ')

    conn.execute('''
        CREATE TABLE visible_opunits(
            opunit_id UUID NOT NULL,
            visible_opunit UUID NOT NULL
        )
    ''')

    conn.execute(
        'CREATE UNIQUE INDEX UNQ_visible_opunit_entry ON visible_opunits(opunit_id,visible_opunit)')

    conn.execute('''
        CREATE TABLE opunit_ancestors(
            opunit_id UUID NOT NULL,
            ancestor_id UUID NOT NULL
        )
    ''')

    conn.execute(
        'CREATE UNIQUE INDEX UNQ_ancestors_opunits_entry ON opunit_ancestors(opunit_id,ancestor_id)')

    conn.execute('''
        CREATE TABLE opunit_user(
            opunit_id UUID NOT NULL,
            org_id UUID NOT NULL,
            user_id UUID NOT NULL,
            role_id UUID NOT NULL
        )
    ''')

    conn.execute(
        'CREATE UNIQUE INDEX UNQ_opunit_user ON opunit_user(opunit_id,user_id)')


def downgrade():
    conn = op.get_bind()
    conn.execute('DROP TABLE opunit_user')
    conn.execute('DROP TABLE opunit_ancestors')
    conn.execute('DROP TABLE visible_opunits')
    conn.execute('DROP TABLE opunit')
