"""empty message

Revision ID: 53eb4c9de3c3
Revises: 7caae320fc2f
Create Date: 2019-02-22 20:38:13.706864

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '53eb4c9de3c3'
down_revision = '7caae320fc2f'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute('''
        INSERT INTO permission(id,entity_name,resource_name,name,operation_name,description)
        VALUES
        (
            '2ace7705-2e37-4161-80b4-45af8a76a79c',
            'OPUNIT',
            'User',
            'Create',
            'create_user',
            'Can create a new user.'
        ),
        (
            '42b95a9b-824c-423d-ac63-a8c25a2bd5f3',
            'OPUNIT',
            'User',
            'View',
            'view_users',
            'Can see list of users and details of the user. Users of current opunit and opunits under current opunit.'
        ),
        (
            'cb568dd5-1699-492c-b5a0-6a1a85e4bc65',
            'OPUNIT',
            'User',
            'Edit Profile',
            'edit_profile',
            'Can modify user profile,for the user having role which current user can grant,without mobileno and password.'
        ),
        (
            '6ed15ef2-d881-4c53-b09a-939e529b9202',
            'OPUNIT',
            'User',
            'Reset Credentials',
            'reset_credentials',
            'Can modify mobile no and reset password for the user having role which current user can grant.'
        ),
        (
            'e391dd57-1805-485d-a4bb-32ed4b9b690c',
            'OPUNIT',
            'User',
            'Remove',
            'remove_user',
            'Remove an user access from operational unit.'
        )
    ''')


def downgrade():
    conn = op.get_bind()
    conn.execute('''
        DELETE FROM permission WHERE id IN (
            '2ace7705-2e37-4161-80b4-45af8a76a79c',
            '42b95a9b-824c-423d-ac63-a8c25a2bd5f3',
            'cb568dd5-1699-492c-b5a0-6a1a85e4bc65',
            '6ed15ef2-d881-4c53-b09a-939e529b9202',
            'e391dd57-1805-485d-a4bb-32ed4b9b690c'
        )
    ''')
