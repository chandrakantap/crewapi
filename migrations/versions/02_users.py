"""
Revision ID: 835292e5a2ad
Revises: 2e39434abca0
Create Date: 2019-01-13 12:10:31.153626

SERIAL - 2
"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '835292e5a2ad'
down_revision = '2e39434abca0'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute('''
        CREATE TABLE cru_user(
            id UUID NOT NULL PRIMARY KEY,
            mobile_no VARCHAR(20) NOT NULL,
            mobile_no_country_code VARCHAR(5) NOT NULL,
            password VARCHAR(4096) NOT NULL,
            name VARCHAR(256) NOT NULL,
            address VARCHAR(4096),
            email_id VARCHAR(256),
            owner UUID NOT NULL,
            app_role_id UUID,
            is_first_login BOOLEAN NOT NULL DEFAULT TRUE,
            password_retry_left SMALLINT NOT NULL DEFAULT 4,
            last_selected_org_id UUID,
            last_selected_opunit_id UUID,
            created_by UUID NOT NULL,
            created_on TIMESTAMP WITH TIME ZONE NOT NULL,
            last_modified_by UUID NOT NULL,
            last_modified_on TIMESTAMP WITH TIME ZONE NOT NULL
        )
    ''')

    conn.execute('''
        CREATE UNIQUE INDEX UNQ_user_mobile_no ON cru_user(mobile_no,mobile_no_country_code)
    ''')

# App admin user, password password
    conn.execute('''
        INSERT INTO cru_user(
            id,mobile_no,mobile_no_country_code,
            password,name,owner,app_role_id,
            created_by,created_on,
            last_modified_by,last_modified_on)
            VALUES(
                'b516e3aa-a3af-11e7-bbd2-63ad024569ce',
                '2345623456',
                '+91',
                'pbkdf2:sha256:50000$sSjqtSth$d5f3f2378b00c0be7547feba0544ef6d228ce40df2f2ee1534028d3d10045397',
                'App admin',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                '0ba7eee1-a0dd-4067-8626-39cd1c7138fa',
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW(),
                'd5a2cf0c-9b73-4a03-aee1-f8e3fb806c1a',
                 NOW()
            )
    ''')


def downgrade():
    conn = op.get_bind()
    conn.execute('DROP TABLE cru_user')
