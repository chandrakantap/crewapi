"""empty message

Revision ID: 4bf2c6f195e8
Revises: 53eb4c9de3c3
Create Date: 2019-03-09 00:20:24.948586

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '4bf2c6f195e8'
down_revision = '53eb4c9de3c3'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute("""CREATE TABLE catalog(
            id UUID NOT NULL PRIMARY KEY,
            name VARCHAR(256) NOT NULL,
            description VARCHAR(4096),
            org_id UUID NOT NULL,
            status VARCHAR(4) NOT NULL,
            created_by UUID NOT NULL,
            created_on TIMESTAMP WITH TIME ZONE NOT NULL,
            last_modified_by UUID NOT NULL,
            last_modified_on TIMESTAMP WITH TIME ZONE NOT NULL
        )
    """)
    conn.execute('CREATE UNIQUE INDEX UNQ_catalog_name ON catalog (LOWER(name),org_id)')
    conn.execute("""CREATE TABLE category(
            id UUID NOT NULL PRIMARY KEY,
            name VARCHAR(256) NOT NULL,
            description VARCHAR(4096),
            parent_category_id UUID,
            catalog_id UUID NOT NULL,
            status VARCHAR(4) NOT NULL,
            created_by UUID NOT NULL,
            created_on TIMESTAMP WITH TIME ZONE NOT NULL,
            last_modified_by UUID NOT NULL,
            last_modified_on TIMESTAMP WITH TIME ZONE NOT NULL
        )
    """)
    conn.execute("""CREATE UNIQUE INDEX UNQ_root_category_name ON category (LOWER(name))
        WHERE parent_category_id IS NULL """)
    conn.execute("""CREATE UNIQUE INDEX UNQ_category_name ON category (LOWER(name),parent_category_id)
        WHERE parent_category_id IS NOT NULL """)

    conn.execute("""CREATE TABLE category_draft(
            id UUID NOT NULL PRIMARY KEY,
            name VARCHAR(256) NOT NULL,
            description VARCHAR(4096),
            parent_category_id UUID,
            catalog_id UUID NOT NULL,
            created_by UUID NOT NULL,
            created_on TIMESTAMP WITH TIME ZONE NOT NULL,
            last_modified_by UUID NOT NULL,
            last_modified_on TIMESTAMP WITH TIME ZONE NOT NULL
        )
    """)
    conn.execute("""CREATE UNIQUE INDEX UNQ_root_category_draft_name ON category_draft (LOWER(name))
        WHERE parent_category_id IS NULL """)
    conn.execute("""CREATE UNIQUE INDEX UNQ_category_draft_name ON category_draft (LOWER(name),parent_category_id)
        WHERE parent_category_id IS NOT NULL """)
    conn.execute("""CREATE TABLE product_type(
            id UUID NOT NULL PRIMARY KEY,
            name VARCHAR(256) NOT NULL,
            description VARCHAR(4096),
            org_id UUID NOT NULL,
            status VARCHAR(4) NOT NULL,
            field_groups JSONB,
            fields JSONB,
            variation_fields JSONB,
            units JSONB,
            created_by UUID NOT NULL,
            created_on TIMESTAMP WITH TIME ZONE NOT NULL,
            last_modified_by UUID NOT NULL,
            last_modified_on TIMESTAMP WITH TIME ZONE NOT NULL
    )""")
    conn.execute("CREATE UNIQUE INDEX UNQ_product_type_name ON product_type (LOWER(name),org_id)")


def downgrade():
    conn = op.get_bind()
    conn.execute("DROP TABLE product_type")
    conn.execute("DROP TABLE category_draft")
    conn.execute("DROP TABLE category")
    conn.execute("DROP TABLE catalog")
