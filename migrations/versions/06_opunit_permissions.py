"""empty message

Revision ID: d414319de0d2
Revises: 6e87a5beb664
Create Date: 2019-02-01 23:40:00.393694

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = 'd414319de0d2'
down_revision = '6e87a5beb664'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute('''
        INSERT INTO permission(id,entity_name,resource_name,name,operation_name,description)
        VALUES
        (
            'ab777e31-0e5f-4131-87ef-0f32955d1c28',
            'OPUNIT',
            'Opunit',
            'Create',
            'create_opunit',
            'Can create a new operational unit.'
        ),
        (
            '02675ea6-736e-4dee-b26f-286dec7c13e9',
            'OPUNIT',
            'Opunit',
            'Edit',
            'edit_opunit_details',
            'Can update operational unit details under the current opunit'
        ),
        (
            'd6cab86e-af33-445f-b3c7-74bf98ee75d7',
            'OPUNIT',
            'Opunit',
            'View',
            'view_opunit',
            'Can view operational units under the current opunit'
        ),
        (
            '1680ed3b-ff99-47a1-8fb6-a96dec5f5e32',
            'OPUNIT',
            'Opunit',
            'Delete',
            'delete_opunit',
            'Can delete operational units under the current opunit'
        )
    ''')


def downgrade():
    conn = op.get_bind()
    conn.execute('''
        DELETE FROM permission WHERE id IN (
            '1680ed3b-ff99-47a1-8fb6-a96dec5f5e32',
            'd6cab86e-af33-445f-b3c7-74bf98ee75d7',
            '02675ea6-736e-4dee-b26f-286dec7c13e9',
            'ab777e31-0e5f-4131-87ef-0f32955d1c28',
        )
    ''')
