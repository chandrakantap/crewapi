"""empty message

Revision ID: 2487a3f4e5ba
Revises: 4bf2c6f195e8
Create Date: 2019-03-09 00:24:41.376494

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = '2487a3f4e5ba'
down_revision = '4bf2c6f195e8'
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    conn.execute('''
        INSERT INTO permission(id,entity_name,resource_name,name,operation_name,description)
        VALUES
        (
            '69b667bd-055c-462c-8326-ab459005c2d1',
            'ORG',
            'Catalog',
            'Manage',
            'manage_catalog',
            'User having manage catalog permission can created,update,remove catalog
              and catalog items(e.g. category,product).Can view all the catalogs.'
        ),
        (
            'b33c0447-c3af-4578-bfaf-f087bce9b15a',
            'ORG',
            'Catalog',
            'Review',
            'review_catalog',
            'User with this permission can not create any new catalog or catalog items. But can update existing items.'
        ),
        (
            'bf2ec736-7ceb-4272-a189-bd8905e46b58',
            'ORG',
            'Catalog',
            'View',
            'view_catalog',
            'User can view existing catalogs.
             Please note any user can view the catalog assigned to his role and operational unit.
             User who have view_catalog permission they can view all other catalogs and the draft version as well.'
        ),
        (
            'e77e4cad-a668-4dbe-a5cd-ebe54f6891d2',
            'ORG',
            'Product Type',
            'Manage Product Types',
            'manage_product_type',
            'User can create update view delete product types.'
        ),
        (
            '938a297d-1722-458a-b8f0-3477a8ee268f',
            'ORG',
            'Product Type',
            'View Product Types',
            'view_product_type',
            'User can view product types.'
        )
    ''')


def downgrade():
    conn = op.get_bind()
    conn.execute('''
        DELETE FROM permission WHERE id IN (
            '69b667bd-055c-462c-8326-ab459005c2d1',
            'b33c0447-c3af-4578-bfaf-f087bce9b15a',
            'bf2ec736-7ceb-4272-a189-bd8905e46b58',
            '938a297d-1722-458a-b8f0-3477a8ee268f',
            'e77e4cad-a668-4dbe-a5cd-ebe54f6891d2'
        )
    ''')
